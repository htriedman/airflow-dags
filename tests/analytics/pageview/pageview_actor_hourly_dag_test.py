import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'pageview', 'pageview_actor_hourly_dag.py']


def test_webrequest_actor_label_hourly_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="pageview_actor_hourly")
    assert dag is not None
    assert len(dag.tasks) == 4
