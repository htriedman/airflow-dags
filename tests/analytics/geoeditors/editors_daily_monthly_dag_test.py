import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'geoeditors', 'editors_daily_monthly_dag.py']


def test_editors_daily_monthly_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="editors_daily_monthly")
    assert dag is not None
    assert len(dag.tasks) == 3
