import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['research', 'dags', 'knowledge_gaps_dag.py']


def test_knowledge_gaps_pipeline_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="knowledge_gaps_pipeline")
    assert dag is not None
    assert len(dag.tasks) == 11
