'''

This job loads the specified datasets listed below daily
from their respective hive tables to cassandra

The datasets covered within this job include:
   mediarequest_per_file,
   mediarequest_per_referer,
   mediarequest_top_files,
   pageview_per_article,
   pageview_per_project,
   pageview_top_articles,
   pageview_top_per_country,
   unique_devices

'''

from airflow import DAG
from datetime import datetime, timedelta
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor)
from wmf_airflow_common.partitions_builder import daily_partitions
from wmf_airflow_common.operators.spark import SparkSqlOperator
from analytics.config.dag_config import default_args, hql_directory, cassandra_default_conf, artifact

dag_id = 'cassandra_daily_load'
var_props = VariableProperties(f'{dag_id}_config')
execution_conf = var_props.get(
    'conf',
    {
        **cassandra_default_conf,
        'spark.dynamicAllocation.maxExecutors': 64,
        'spark.yarn.maxAppAttempts': 1,
        'spark.executor.memoryOverhead': 2048
    }
)

jobs_properties = var_props.get('jobs_properties', {
    'mediarequest_per_file': {
        'hql_parameters': {
            'source_table': 'wmf.mediarequest',
            'destination_table': 'aqs.local_group_default_T_mediarequest_per_file.data'
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_mediarequest_per_file_daily.hql',
        'source_granularity': '@hourly'
    },
    'mediarequest_per_referer': {
        'hql_parameters': {
            'source_table': 'wmf.mediarequest',
            'destination_table': 'aqs.local_group_default_T_mediarequest_per_referer.data'
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_mediarequest_per_referer_daily.hql',
        'source_granularity': '@hourly'
    },
    'mediarequest_top_files': {
        'hql_parameters': {
            'source_table': 'wmf.mediarequest',
            'destination_table': 'aqs.local_group_default_T_mediarequest_top_files.data'
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_mediarequest_top_files_daily.hql',
        'source_granularity': '@hourly'
    },
    'pageview_per_article': {
        'hql_parameters': {
            'source_table': 'wmf.pageview_hourly',
            'destination_table': 'aqs.local_group_default_T_pageviews_per_article_flat.data'
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_pageview_per_article_daily.hql',
        'source_granularity': '@hourly'
    },
    'pageview_per_project': {
        'hql_parameters': {
            'source_table': 'wmf.projectview_hourly',
            'destination_table': 'aqs.local_group_default_T_pageviews_per_project_v2.data'
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_pageview_per_project_daily.hql',
        'source_granularity': '@hourly'
    },
    'pageview_top_articles': {
        'hql_parameters': {
            'source_table': 'wmf.pageview_hourly',
            'destination_table': 'aqs.local_group_default_T_top_pageviews.data'
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_pageview_top_articles_daily.hql',
        'source_granularity': '@hourly'
    },
    'pageview_top_per_country': {
        'hql_parameters': {
            'source_table': 'wmf.pageview_actor',
            'country_deny_list_table': 'wmf.geoeditors_blacklist_country',
            'destination_table': 'aqs.local_group_default_T_top_percountry.data',
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_pageview_top_percountry_daily.hql',
        'source_granularity': '@hourly'
    },
    'unique_devices': {
        # source_tables property accounts for the case where we have mulitple source tables
        'sensor_source_tables': ['wmf.unique_devices_per_domain_daily','wmf.unique_devices_per_project_family_daily'],
        'hql_parameters': {
            'source_table_per_domain': 'wmf.unique_devices_per_domain_daily',
            'source_table_per_project_family': 'wmf.unique_devices_per_project_family_daily',
            'destination_table': 'aqs.local_group_default_T_unique_devices.data'
        },
        'hql_path': f'{hql_directory}/cassandra/daily/load_cassandra_unique_devices_daily.hql',
        'source_granularity': '@daily'
    }
})

def create_sensor(job_name, source_table, source_granularity):
    sensor = NamedHivePartitionSensor(
        task_id=f'wait_for_{job_name}_{source_table}_partitions',
        partition_names=daily_partitions(
            table=source_table,
            granularity=source_granularity
        ),
        poke_interval=timedelta(minutes=20).total_seconds()
    )
    return sensor

    # How do I ensure this returns a binary that can be stored in the sensor variable , should i be using the return keyword?
    # Currently returning none

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 9, 20)),
    schedule_interval='@daily',
    tags=['cassandra', 'hql'],
    default_args=var_props.get_merged('default_args',
        {
            **default_args,
            'sla': timedelta(hours=10),
        }
    )
) as dag:

    for job_name, properties in jobs_properties.items():
        # Sensor to check that all hive partitions are present
        # If sensor_source_table is not present in job_properties,
        # source table definition falls back to sensor_table in the hql_properties
        source_granularity = properties['source_granularity']

        if ('sensor_source_tables' in properties):
            sensor = [create_sensor(job_name, source_table,source_granularity) for source_table in properties['sensor_source_tables']]
        else:
            sensor = create_sensor(job_name, properties['hql_parameters']['source_table'], source_granularity)


        etl = SparkSqlOperator(
            task_id=f'load_{job_name}_to_cassandra',
            sql=properties['hql_path'],
            query_parameters={
                **properties['hql_parameters'],
                'year': '{{ execution_date.year }}',
                'month': '{{ execution_date.month }}',
                'day': '{{ execution_date.day }}',
                # Setting coalesce_partitions to 6 is important
                # as it defines how many parallel loaders we use for cassandra.
                # We currently use 6 as there are 6 cassandra hosts
                'coalesce_partitions': 6,
            },
            executor_memory='8G',
            executor_cores=2,
            driver_memory='4G',
            driver_cores=1,
            jars=var_props.get('jars', artifact('spark-cassandra-connector-assembly-3.2.0-WMF-1.jar') + ',' +
                                       artifact('refinery-job-0.2.7-shaded.jar')),
            conf=execution_conf

        )

        sensor >> etl
