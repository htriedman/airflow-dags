from typing import List, Any, Optional
from datetime import datetime
import os
from airflow import DAG
from airflow.operators.python import BranchPythonOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from pyarrow.fs import FileType
from wmf_airflow_common import util
from wmf_airflow_common.operators.email import HdfsEmailOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator, SparkSubmitOperator
from wmf_airflow_common.partitions_builder import daily_partitions

def should_alert(**kwargs: Any) -> List[str]:
    """
    Determines whether the last execution of the detect_anomalies task
    has detected anomalies and whether an alert should be sent for them.
    """

    # Create HDFS file system handle.
    hdfs = util.hdfs_client(kwargs['hadoop_name_node'])

    # Check whether the detect_anomalies job did output an anomalies file.
    file_info = hdfs.get_file_info(kwargs['anomaly_path'])
    if file_info.type != FileType.NotFound:

        # Return the task_id of the next task to run.
        return ['send_email']

    # Otherwise, skip downstream tasks.
    return []


class AnomalyDetectionDAG(DAG):
    """
    Runs anomaly detection for the given metrics on a daily schedule.
    When anomalies are detected, sends an email alert to the given address.
    """

    def __init__(
        self,
        dag_id: str,
        metric_source: str,
        start_date: Optional[datetime],
        source_table: str,
        source_granularity: str,
        metrics_query: str,
        destination_table: str,
        hdfs_temp_directory: str,
        anomaly_threshold: str,
        anomaly_email: str,
        hadoop_name_node: str,
        refinery_job_shaded_jar: str,
        **kwargs: Any,
    ):
        """
        Initializes the DAG (at interpretation time).
        """

        # Add anomaly_detection tag to tag list.
        if 'tags' not in kwargs:
            kwargs['tags'] = []
        if 'anomaly_detection' not in kwargs['tags']:
            kwargs['tags'].append('anomaly_detection')

        # Initialize the parent DAG.
        super().__init__(
            dag_id,
            start_date=start_date,
            schedule_interval='@daily',  # Always daily.
            max_active_runs=1,
            **kwargs
        )

        # Determine the path of the anomaly file.
        anomaly_output_path = os.path.join(
            hdfs_temp_directory,
            'anomaly_detection',
            '{{ dag.dag_id }}__{{ ds }}',
        )

        # Sensor for waiting until the Hive partition(s) is/are present.
        sensor = NamedHivePartitionSensor(
            task_id='wait_for_source',
            partition_names=daily_partitions(
                table=source_table,
                granularity=source_granularity,
            ),
            dag=self,
        )

        # Operator for sourcing the metrics to be analyzed.
        task_1 = SparkSqlOperator(
            task_id='source_metrics',
            sql=metrics_query,
            query_parameters={
                'refinery_hive_jar': refinery_job_shaded_jar,
                'source_table': source_table,
                'destination_table': destination_table,
                'source': metric_source,
                'year': '{{ execution_date.year }}',
                'month': '{{ execution_date.month }}',
                'day': '{{ execution_date.day }}',
            },
            dag=self,
        )

        # Operator for checking the metric time series for anomalies.
        task_2 = SparkSubmitOperator(
            task_id='detect_anomalies',
            application=refinery_job_shaded_jar,
            java_class='org.wikimedia.analytics.refinery.job.dataquality.RSVDAnomalyDetection',
            # This job generates very big YARN logs. When the logs reach a
            # certain size, the gRPC library used by the skein client breaks.
            # Prevent skein collecting them to avoid issues.
            skein_app_log_collection_enabled=False,
            application_args={
                '--source-table': destination_table,
                '--metric-source': metric_source,
                '--last-data-point-dt': '{{ ds }}',
                '--output-path': anomaly_output_path,
                '--deviation-threshold': anomaly_threshold,
                '--rsvd-oversample': 1,
            },
            dag=self,
        )

        # Branch operator to determine whether to send alert or not.
        task_3 = BranchPythonOperator(
            task_id='should_alert',
            python_callable=should_alert,
            op_kwargs={
                'hadoop_name_node': hadoop_name_node,
                'anomaly_path': anomaly_output_path,
            },
            dag=self,
        )

        # Operator for sending an alert email.
        task_4 = HdfsEmailOperator(
            task_id='send_email',
            to=anomaly_email,
            subject='Anomaly report {{ dag.dag_id }} {{ ds }}',
            html_content=(
                'The following anomalies have been detected:<br/>'
                'Job: {{ dag.dag_id }}<br/>'
                'Date: {{ ds }}<br/>'
                'Affected metrics and corresponding deviations:<br/>'
            ),
            embedded_file=anomaly_output_path,
            hadoop_name_node=hadoop_name_node,
            dag=self,
        )

        # Specify task downstream dependencies.
        sensor >> task_1 >> task_2 >> task_3 >> task_4
