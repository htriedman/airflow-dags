'''
### Reports three Wikidata Daily metrics to Graphite.
* Runs daily.
* Waits for the existence of 24 webrequest partitions for a day.
* Launches the HiveToGraphite spark Job which will run 5 hql scripts:
   - wikidata_articleplaceholder_metrics.hql
   - wikidata_reliability_metrics.hql
   - wikidata_specialentity_data_metrics.hql
   - wikidata_special_entity_schema_text_metrics.hql
   - wikidata_entity_schema_namespace_metrics.hql
* Reports metrics generated to Graphite (graphite.wikimedia.org).


### Metrics include:
* ArticlePlaceholder metrics -
   Metrics can be viewed in Graphite under the daily.wikidata.articleplaceholder namespace,
   in varnish_requests.abouttopic.* folder

* Reliability metrics - metrics for the Wikidata reliability graphs
   Metrics can be viewed in Graphite under the daily.wikidata.reliability_metrics namespace

* specialentity_data metrics - metrics for the Wikidata Special:EntityData extension
   Metrics can be viewed in Graphite under the daily.wikidata.entitydata namespace

* Special:EntitySchemaText metrics - accesses to subpages of the special page which is part of the EntitySchema extension
   Metrics can be viewed in Graphite under the daily.wikidata.entity_schema.text namespace

* EntitySchema namespace metrics - number of views of pages in the EntitySchema namespace
   Metrics can be viewed in Graphite under the daily.wikidata.entity_schema.views namespace
'''

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from analytics.config import dag_config
from wmf_airflow_common.sensors.hive import NamedHivePartitionSensor
from wmf_airflow_common.partitions_builder import daily_partitions
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters

dag_id = 'wikidata_metrics_to_graphite_daily'
var_props = VariableProperties(f'{dag_id}_config')
source_table = 'wmf.webrequest'
year = '{{execution_date.year}}'
month = '{{execution_date.month}}'
day = '{{execution_date.day}}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    schedule_interval='@daily',
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 2)),
    default_args=var_props.get_merged('default_args',
        { **dag_config.default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
    tags=['wikidata', 'graphite', 'metrics']
) as dag:

    sensor1 = NamedHivePartitionSensor(
        task_id='wait_for_webrequest',
        partition_names=daily_partitions(
            table=source_table,
            granularity='@hourly',
            pre_partitions=['webrequest_source=text']
        )
    )
    #define common arguments for the 3 etl tasks
    hive_to_graphite_common_args = [
        # Graphite parameters
        '--graphite_host', 'graphite-in.eqiad.wmnet',
        '--graphite_port', 2003,
        # HQL parameters
        '-d', f'webrequest_table={source_table}',
        '-d', f'year={year}',
        '-d', f'month={month}',
        '-d', f'day={day}',
        '-d', 'coalesce_partitions=4'
    ]

    etl1 = SparkSubmitOperator(
        task_id='generate_and_send_articleplaceholder_metrics',
        application=var_props.get('refinery_job_jar',
            dag_config.artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.HiveToGraphite',
        application_args=[
            '-f', var_props.get('query_file1', f'{dag_config.hql_directory}/wikidata/wikidata_articleplaceholder_metrics.hql'),
            '--metric_prefix', var_props.get('metric_prefix1', 'daily.wikidata.articleplaceholder.varnish_requests.abouttopic')
        ]+hive_to_graphite_common_args,
        conf={'spark.dynamicAllocation.maxExecutors': 128}
    )

    etl2 = SparkSubmitOperator(
        task_id='generate_and_send_reliability_metrics',
        application=var_props.get('refinery_job_jar',
            dag_config.artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.HiveToGraphite',
        # This job generates too much YARN logs for the skein client.
        # Preventing skein to collect logs avoids issues.
        skein_app_log_collection_enabled=False,
        application_args=[
            '-f', var_props.get('query_file2', f'{dag_config.hql_directory}/wikidata/wikidata_reliability_metrics.hql'),
            '--metric_prefix', var_props.get('metric_prefix2', 'daily.wikidata.reliability_metrics'),
        ]+hive_to_graphite_common_args,
        conf={'spark.dynamicAllocation.maxExecutors': 128}
    )

    etl3 = SparkSubmitOperator(
        task_id='generate_and_send_specialentity_data_metrics',
        application=var_props.get('refinery_job_jar',
            dag_config.artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.HiveToGraphite',
        # This job generates too much YARN logs for the skein client.
        # Preventing skein to collect logs avoids issues.
        skein_app_log_collection_enabled=False,
        application_args=[
            '-f', var_props.get('query_file3', f'{dag_config.hql_directory}/wikidata/wikidata_specialentity_data_metrics.hql'),
            '--metric_prefix', var_props.get('metric_prefix3', 'daily.wikidata.entitydata'),
        ]+hive_to_graphite_common_args,
        conf={'spark.dynamicAllocation.maxExecutors': 128}
    )

    etl4 = SparkSubmitOperator(
        task_id='generate_and_send_special_entity_schema_text_metrics',
        application=var_props.get('refinery_job_jar',
            dag_config.artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.HiveToGraphite',
        application_args=[
            '-f', var_props.get('query_file4', f'{dag_config.hql_directory}/wikidata/wikidata_special_entity_schema_text_metrics.hql'),
            '--metric_prefix', var_props.get('metric_prefix4', 'daily.wikidata.entity_schema.text'),
        ]+hive_to_graphite_common_args,
        conf={'spark.dynamicAllocation.maxExecutors': 128}
    )

    etl5 = SparkSubmitOperator(
        task_id='generate_and_send_entity_schema_namespace_metrics',
        application=var_props.get('refinery_job_jar',
            dag_config.artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.HiveToGraphite',
        application_args=[
            '-f', var_props.get('query_file5', f'{dag_config.hql_directory}/wikidata/wikidata_entity_schema_namespace_metrics.hql'),
            '--metric_prefix', var_props.get('metric_prefix5', 'daily.wikidata.entity_schema.views'),
        ]+hive_to_graphite_common_args,
        conf={'spark.dynamicAllocation.maxExecutors': 128}
    )

    sensor1 >> [etl1, etl2, etl3, etl4, etl5]
