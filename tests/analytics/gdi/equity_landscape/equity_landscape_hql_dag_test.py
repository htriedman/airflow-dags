import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return [
        "analytics",
        "dags",
        "gdi",
        "equity_landscape",
        "equity_landscape_hql_dag.py",
    ]


def test_equity_landscape_hql_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="gdi_equity_landscape_hql")
    assert dag is not None