"""
A DAG to generate monthly section image recommendations.

Variables in imagerecs_monthly_config with their default values:
* article_images_output: /tmp/research/imagerec/{{execution_date | to_ds_nodash}}/article_images
* recommendations_output: /tmp/research/imagerec/{{execution_date | to_ds_nodash}}/recommendations
* section_alignments: /user/mnz/secmap_results/aligned_sections_subset/aligned_sections_subset_9.0_2022-02.parquet
* start_date: datetime(2022, 10, 1)
* wp_codes: ["ar", "en", "es", "fr", "ja", "ru"]
"""

from datetime import datetime, timedelta

from platform_eng.config.dag_config import alerts_email as default_email
from platform_eng.config.dag_config import artifact, default_args, hdfs_temp_directory

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.partitions_builder import (
    PrePartitions,
    add_post_partitions,
)
from wmf_airflow_common.templates import time_filters

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor,
)

from mergedeep import merge


dag_id = "section_image_recommendations"
var_props = VariableProperties(f"{dag_id}_config")
conda_env = var_props.get("conda_env", artifact("imagerec-0.1.0.conda.tgz"))

modified_args = merge(
    {},
    default_args,
    {
        "email": [default_email, "sd-alerts@lists.wikimedia.org"],
    }
)


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    catchup=False,
    dagrun_timeout=timedelta(days=3),
    schedule_interval="@monthly",
    start_date=var_props.get_datetime("start_date", datetime(2023, 1, 1)),
    tags=["structured-data-team"],
    default_args=modified_args,
    user_defined_filters=time_filters.filters,
) as dag:
    mediawiki_snapshot = "{{execution_date | to_ds_month}}"
    wikidata_snapshot = "{{execution_date | start_of_next_month | start_of_previous_week | to_ds}}"

    wp_codes = var_props.get_list("wp_codes", ["ar", "en", "es", "fr", "ja", "ru"])
    wiki_db_partitions = PrePartitions([[f"wiki_db={code}wiki" for code in wp_codes]])

    wait_for_mediawiki_wikitext_current_snapshot = NamedHivePartitionSensor(
        task_id="wait_for_mediawiki_wikitext_current_snapshot",
        partition_names=add_post_partitions(
            [f"wmf.mediawiki_wikitext_current/snapshot={mediawiki_snapshot}"],
            wiki_db_partitions,
        ),
    )

    wait_for_wikidata_item_page_link_snapshot = NamedHivePartitionSensor(
        task_id="wait_for_wikidata_item_page_link_snapshot",
        partition_names=add_post_partitions(
            [f"wmf.wikidata_item_page_link/snapshot={wikidata_snapshot}"],
            wiki_db_partitions,
        ),
    )

    imagerec_dir = f"{hdfs_temp_directory}/imagerec/{{{{execution_date | to_ds_nodash}}}}"

    article_images_path = var_props.get(
        "article_images_output",
        f"{imagerec_dir}/article_images",
    )
    article_images_args = [
        "--wikitext-snapshot",
        mediawiki_snapshot,
        "--item-page-link-snapshot",
        wikidata_snapshot,
        "--output",
        article_images_path,
    ]
    if wp_codes:
        article_images_args += ["--wp-codes", " ".join(wp_codes)]

    extract_article_images = SparkSubmitOperator.for_virtualenv(
        driver_memory="16G",
        executor_cores=4,
        executor_memory=var_props.get("executor_memory_for_extract_article", "8G"),
        conf={
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.sql.shuffle.partitions": 1024,
        },
        virtualenv_archive=conda_env,
        entry_point="bin/imagerec_article_images.py",
        application_args=article_images_args,
        task_id="extract_article_images",
        launcher="skein",
    )

    recommendations_args = [
        "--section-images",
        article_images_path,
        "--output",
        var_props.get("recommendations_output", f"{imagerec_dir}/recommendations"),
        "--section-alignments",
        var_props.get(
            "section_alignments",
            # TODO: Using Muniza's files here temporarily until we productionize the job that generates this file.
            "/user/mnz/secmap_results/aligned_sections_subset/aligned_sections_subset_9.0_2022-02.parquet",
        ),
        "--max-target-images",
        var_props.get(
            "max_target_images",
            0,
        ),
    ]
    if wp_codes:
        recommendations_args += ["--wp-codes", " ".join(wp_codes)]

    generate_recommendations = SparkSubmitOperator.for_virtualenv(
        driver_memory="16G",
        executor_cores=4,
        executor_memory=var_props.get("executor_memory_for_generate_recommendations", "8G"),
        conf={
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.sql.shuffle.partitions": 1024,
        },
        virtualenv_archive=conda_env,
        entry_point="bin/imagerec_recommendation.py",
        application_args=recommendations_args,
        task_id="generate_recommendations",
        launcher="skein",
    )

    # Build the actual DAG
    (
        [
            wait_for_mediawiki_wikitext_current_snapshot,
            wait_for_wikidata_item_page_link_snapshot,
        ]
        >> extract_article_images
        >> generate_recommendations
    )
