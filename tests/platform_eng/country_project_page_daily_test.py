import pytest

# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['platform_eng', 'dags', 'differential_privacy', 'country_project_page_daily.py']


def test_image_suggestions_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="country_project_page_daily_dag")
    assert dag is not None
    assert len(dag.tasks) == 2
