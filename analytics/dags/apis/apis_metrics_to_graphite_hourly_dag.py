'''
###Reports hourly metrics for api (rest and action) to graphite.

* Waits for webrequest partitions.
* Use the HiveToGraphite spark Job to run api_metrics.hql hive query.
* Output of the query is the API(rest and action) metrics which is sent to Graphite by the HiveToGraphite job.
'''

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from analytics.config.dag_config import default_args, hql_directory, artifact

dag_id = 'apis_metrics_to_graphite_hourly'
var_props = VariableProperties(f'{dag_id}_config')
source_table = 'wmf.webrequest'
year = '{{execution_date.year}}'
month = '{{execution_date.month}}'
day = '{{execution_date.day}}'
hour = '{{execution_date.hour}}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 14, 8)),
    schedule_interval='@hourly',
    default_args=var_props.get_merged('default_args',
        { **default_args,
            # We use 6 hours as webrequest data needs to be present for the job to process and webrequest-sla is 5 hours
            'sla': timedelta(hours=6),
        }
    ),
    tags=['spark', 'hql', 'graphite']
) as dag:

    s1 = NamedHivePartitionSensor(
        task_id='wait_for_webrequest',
        partition_names=[f'{source_table}/webrequest_source=text/year={year}/month={month}/day={day}/hour={hour}']
    )

    # Only add metrics-prefix parameter if its value is not empty
    # otherwise it fails being correctly passed to skein
    metric_prefix_args = []
    metric_prefix_var = var_props.get('metric_prefix', '')
    if metric_prefix_var != '':
        metric_prefix_args = ['--metric_prefix', metric_prefix_var]

    etl = SparkSubmitOperator(
        task_id='generate_and_send_apis_metrics_to_graphite',
        application=var_props.get('refinery_job_jar', artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.HiveToGraphite',
        # Passing application_args as a list to make it work with multiple -d parameters
        application_args=metric_prefix_args + [
            # Graphite parameters
            '--graphite_host', 'graphite-in.eqiad.wmnet',
            '--graphite_port', 2003,
            # HQL parameters
            '-f', var_props.get('query_file', f'{hql_directory}/apis/api_metrics.hql'),
            '-d', f'source_table={source_table}',
            '-d', f'year={year}',
            '-d', f'month={month}',
            '-d', f'day={day}',
            '-d', f'hour={hour}',
        ],
        conf={'spark.dynamicAllocation.maxExecutors': 64},
    )
    s1 >> etl
