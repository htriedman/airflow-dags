import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'anomaly_detection', 'anomaly_detection_traffic_distribution_daily_dag.py']


def test_anomaly_detection_traffic_distribution_daily_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="anomaly_detection_traffic_distribution_daily")
    assert dag is not None
    assert len(dag.tasks) == 5
