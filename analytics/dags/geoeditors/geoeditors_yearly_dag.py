"""
### Geo-editors yearly
Aggregate and archive geoeditors_edits_yearly data into edits by country by year.
Do not show any countries with fewer than 100,000 edits per year for privacy reasons.
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

from analytics.config.dag_config import default_args, hql_directory, hadoop_name_node

dag_id = 'geoeditors_yearly'
var_props = VariableProperties(f'{dag_id}_config')
current_year = '{{ execution_date.year }}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 1, 1)),
    schedule_interval='@yearly',
    tags=['archive', 'geoeditors', 'yearly'],
    user_defined_filters=filters,
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=5),
        }
    ),
) as dag:

    # generate all monthly partitions for current_year
    partitions = []
    for i in range(1, 13):
        partitions.append(f'wmf.geoeditors_edits_monthly/month={current_year}-{"{:0>2d}".format(i)}')
    partitions.reverse()

    hive_sensor_geo = NamedHivePartitionSensor(
        task_id='wait_for_geoeditors_edits',
        partition_names=partitions,  # for quality assurance, wait on a full year of data.
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    project_map_table_snapshot = var_props.get(
        'project_map_table_snapshot',
        f'{current_year}-12'
    )

    hive_sensor_namespace = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_project_namespace_map',
        partition_names=[f'wmf_raw.mediawiki_project_namespace_map/snapshot={project_map_table_snapshot}'],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    temp_directory = var_props.get(
        'temp_directory',
        f"{hadoop_name_node}/wmf/tmp/analytics/geoeditors/{{{{ts_nodash}}}}"
    )
    edit_count_threshold = var_props.get(
        'edit_count_threshold',
        '100000'
    )
    hql_path = var_props.get(
        'hql_path',
        f'{hql_directory}/geoeditors/yearly/write_geoeditors_edits_yearly_data.hql'
    )

    wikipedias_temp_directory = f'{temp_directory}-all-wikipedias'
    write_wikipedias = SparkSqlOperator(
        task_id='write_all_wikipedias_geoeditors_edits_yearly_data',
        sql=hql_path,
        query_parameters={
            'project_map_table': 'wmf_raw.mediawiki_project_namespace_map',
            'project_map_table_snapshot': project_map_table_snapshot,
            'country_map_table': 'canonical_data.countries',
            'source_table': 'wmf.geoeditors_edits_monthly',
            'destination_directory': wikipedias_temp_directory,
            'namespace_zero_edit_count_threshold': edit_count_threshold,
            'year': current_year,
            'project_family': 'wikipedia'
        },
    )

    wikidata_temp_directory = f'{temp_directory}-wikidata'
    write_wikidata = SparkSqlOperator(
        task_id='write_wikidata_geoeditors_edits_yearly_data',
        sql=hql_path,
        query_parameters={
            'project_map_table': 'wmf_raw.mediawiki_project_namespace_map',
            'project_map_table_snapshot': project_map_table_snapshot,
            'country_map_table': 'canonical_data.countries',
            'source_table': 'wmf.geoeditors_edits_monthly',
            'destination_directory': wikidata_temp_directory,
            'namespace_zero_edit_count_threshold': edit_count_threshold,
            'year': current_year,
            'project_family': 'wikidata'
        },
    )

    archive_directory = var_props.get(
        'archive_directory',
        f"{hadoop_name_node}/wmf/data/archive/geoeditors/edits/yearly"
    )

    archive_wikipedias = HDFSArchiveOperator(
        task_id='move_all_wikipedias_data_to_archive',
        source_directory=wikipedias_temp_directory,
        expected_filename_ending='.csv',
        archive_file=f'{archive_directory}/geoeditors-edits-{current_year}-all-wikipedias.csv',
        check_done=True,
        archive_parent_umask='027',  # This dataset is stored in archive, but it is not synced externally.
        archive_perms='640'          # We explicitly make it not accessible to ALL, to be changed if needed.
    )

    archive_wikidata = HDFSArchiveOperator(
        task_id='move_wikidata_data_to_archive',
        source_directory=wikidata_temp_directory,
        expected_filename_ending='.csv',
        archive_file=f'{archive_directory}/geoeditors-edits-{current_year}-wikidata.csv',
        check_done=True
    )

    hive_sensor_geo >> [write_wikipedias, write_wikidata]
    hive_sensor_namespace >> [write_wikipedias, write_wikidata]
    write_wikipedias >> archive_wikipedias
    write_wikidata >> archive_wikidata
