import pytest

# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def dagpath_fixture():
    return['analytics','dags','projectview','projectview_geo_dag.py']

def test_projectview_geo_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="projectview_geo")
    assert dag is not None
    assert len(dag.tasks) == 3
