"""
# Edit Hourly

Aggregates revision data from mediawiki_history new  month snapshot into an
easier-to-query dataset for users.

This job gathers event_entity=revision+event_type=create events from mediawiki history,
formats them according to https://docs.google.com/document/d/1jzrE3xdyEHed4Ek5ORRedOlEeH-i111hdmG3tBTF8QU
and populates the edit_hourly hive table.

At every workflow run, this job reads the entire mediawiki history for a given snapshot
and creates the corresponding snapshot partition in edit_hourly.
If that partition already exits, it overrides it.

There are no [year, month, day, hour] partitions, because the edit_hourly data is small enough.
The field ts (extracted from mediawiki_history.event_timestamp) is truncated to the hour,
and the data is aggregated hourly.

The job waits for the wmf.mediawiki_history new snapshot partition to be available
before starting computation.

"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, dataset, hql_directory

dag_id = 'edit_hourly'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2023, 2, 1)),
    schedule_interval='@monthly',
    tags=['spark', 'hql', 'hive'],
    user_defined_filters=filters,
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=5),
        }
    ),
) as dag:

    sensor = dataset("hive_wmf_mediawiki_history").get_sensor_for(dag)

    etl = SparkSqlOperator(
        task_id='process_edit_hourly',
        sql=var_props.get('hql_path', f'{hql_directory}/edit/edit_hourly.hql'),
        query_parameters={
            'source_table': 'wmf.mediawiki_history',
            'wiki_map_table': 'canonical_data.wikis',
            'destination_table': var_props.get('destination_table', 'wmf.edit_hourly'),
            'snapshot' : '{{ execution_date | to_ds_month }}',
            'coalesce_partitions': 32,
        },
        driver_memory='4G',
        executor_memory='8G',
        executor_cores=2,
        conf={
            'spark.dynamicAllocation.maxExecutors': 128,
            # Our default HDFS umask is 027, preventing read-for-all.
            # This dataset contains public-only data, so we make it
            # readable by all.
            'spark.hadoop.fs.permissions.umask-mode': '022'
        },
    )

    sensor >> etl
