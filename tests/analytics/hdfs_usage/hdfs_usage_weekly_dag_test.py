import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'hdfs_usage', 'hdfs_usage_weekly_dag.py']


def test_hdfs_usage_weekly_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="hdfs_usage_weekly")
    assert dag is not None
    assert len(dag.tasks) == 3
    sensor = dag.get_task('wait_for_raw_fsimage_on_hdfs')
    assert sensor._url == 'hdfs:///wmf/data/raw/hdfs/fsimage/fsimage_{{ds}}.gz'
