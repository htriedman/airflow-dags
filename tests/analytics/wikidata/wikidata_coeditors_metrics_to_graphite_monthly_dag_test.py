import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'wikidata',
            'wikidata_coeditors_metrics_to_graphite_monthly_dag.py']


def test_wikidata_coeditors_metrics_monthly_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="wikidata_coeditors_metrics_to_graphite_monthly")
    assert dag is not None
    assert len(dag.tasks) == 3
    etl = dag.get_task('generate_and_send_coeditormetric_to_graphite')
    application_args = etl._application_args
    assert len(application_args) == 14
