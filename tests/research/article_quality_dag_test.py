import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['research', 'dags', 'article_quality_dag.py']


def test_article_quality_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="research_article_quality")
    assert dag is not None
    assert len(dag.tasks) == 5
