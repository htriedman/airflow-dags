"""
Monitor the distribution of navigationtiming events by useragent properties,
and alert on anomalies. We use an entropy formula to express the mentioned
distribution as a monitorable metric.
"""

from datetime import datetime, timedelta
from wmf_airflow_common.config.variable_properties import VariableProperties
from analytics.dags.anomaly_detection.anomaly_detection_dag_factory import AnomalyDetectionDAG
from analytics.config import dag_config

metric_source ='useragent_distribution'
dag_id = f'anomaly_detection_{metric_source}_daily'
var_props = VariableProperties(f'{dag_id}_config')

dag = AnomalyDetectionDAG(
    dag_id=dag_id,
    metric_source=metric_source,
    doc_md=__doc__,
    default_args=var_props.get_merged('default_args',
        { **dag_config.default_args,
            'sla': timedelta(hours=10),
        }
    ),
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 13)),
    source_table='event.navigationtiming',
    source_granularity='@hourly',
    metrics_query=var_props.get('metrics_query',
        f'{dag_config.hql_directory}/anomaly_detection/useragent_distribution.hql'),
    destination_table=var_props.get('destination_table', 'wmf.anomaly_detection'),
    hdfs_temp_directory=var_props.get('hdfs_temp_directory', dag_config.hdfs_temp_directory),
    anomaly_threshold=10,
    anomaly_email=var_props.get('anomaly_email', dag_config.alerts_email),
    hadoop_name_node=dag_config.hadoop_name_node,
    refinery_job_shaded_jar=var_props.get('refinery_job_shaded_jar',
        dag_config.artifact('refinery-job-0.2.1-shaded.jar')),
)
