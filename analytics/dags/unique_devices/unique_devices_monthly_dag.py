'''
Calculates metrics about unique devices on a monthly basis.
Two sets of metrics are generated: per domain and per project family.
The metrics are stored in a Hive table and also as CSV archive files.
'''

from airflow import DAG
from analytics.config.dag_config import (
    archive_directory, hadoop_name_node, hdfs_temp_directory, hql_directory, default_args)
from datetime import datetime, timedelta
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from wmf_airflow_common.templates.time_filters import filters

dag_id = 'unique_devices_monthly'
var_props = VariableProperties(f'{dag_id}_config')

# Contains the properties of each of the datasets produced by this DAG.
datasets = var_props.get('datasets', {
    'per_domain': {
        'compute_hql_path': (
            f'{hql_directory}/unique_devices/per_domain/' +
            'unique_devices_per_domain_monthly.hql'
        ),
        'dump_hql_path': (
            f'{hql_directory}/unique_devices/per_domain/' +
            'unique_devices_per_domain_monthly_to_archive.hql'
        ),
        'intermediate_table': 'wmf.unique_devices_per_domain_monthly',
        'success_file_path': (
            f'{hadoop_name_node}/wmf/data/wmf/unique_devices/per_domain/monthly/' +
            'year={{execution_date.year}}/month={{execution_date.month}}/_SUCCESS'
        ),
        'temporary_directory': (
            f'{hdfs_temp_directory}/{dag_id}/per_domain/' +
            '{{execution_date|to_ds_month}}'
        ),
        'destination_path': (
            f'{archive_directory}/unique_devices/per_domain/' +
            '{{execution_date.year}}/{{execution_date|to_ds_month}}/' +
            'unique_devices_per_domain_monthly-{{execution_date|to_ds_month}}.gz'
        ),
    },
    'per_project_family': {
        'compute_hql_path': (
            f'{hql_directory}/unique_devices/per_project_family/' +
            'unique_devices_per_project_family_monthly.hql'
        ),
        'dump_hql_path': (
            f'{hql_directory}/unique_devices/per_project_family/' +
            'unique_devices_per_project_family_monthly_to_archive.hql'
        ),
        'intermediate_table': 'wmf.unique_devices_per_project_family_monthly',
        'success_file_path': (
            f'{hadoop_name_node}/wmf/data/wmf/unique_devices/per_project_family/monthly/' +
            'year={{execution_date.year}}/month={{execution_date.month}}/_SUCCESS'
        ),
        'temporary_directory': (
            f'{hdfs_temp_directory}/{dag_id}/per_project_family/' +
            '{{execution_date|to_ds_month}}'
        ),
        'destination_path': (
            f'{archive_directory}/unique_devices/per_project_family/' +
            '{{execution_date.year}}/{{execution_date|to_ds_month}}/' +
            'unique_devices_per_project_family_monthly-{{execution_date|to_ds_month}}.gz'
        ),
    },
})

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 9, 1)),
    schedule_interval='@monthly',
    tags=['unique_devices', 'archive'],
    user_defined_filters=filters,
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'sla': timedelta(days=1),
    }),
) as dag:

    # The pageview actor table to use as a source.
    source_table = 'wmf.pageview_actor'

    # Sensor to wait for the pageview actor partitions.
    sensor = RangeHivePartitionSensor(
        task_id='wait_for_pageview_actor',
        table_name=source_table,
        from_timestamp='{{execution_date|start_of_current_month}}',
        to_timestamp='{{execution_date|start_of_next_month}}',
        granularity='@hourly',
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    # From here on, the DAG splits in 2 analogous subdags: one for
    # uniques per domain and the other for uniques per project family.
    for dataset_name, dataset_props in datasets.items():

        # SQL operator for the computation of the metrics.
        compute = SparkSqlOperator(
            task_id=f'compute_{dataset_name}_metrics',
            sql=dataset_props['compute_hql_path'],
            query_parameters={
                'source_table': source_table,
                'destination_table': dataset_props['intermediate_table'],
                'year': '{{execution_date.year}}',
                'month': '{{execution_date.month}}',
                'coalesce_partitions': 1,
            },
            driver_cores=1,
            driver_memory='4G',
            executor_cores=4,
            executor_memory='16G',
            conf={
                'spark.dynamicAllocation.maxExecutors': 64,
                'spark.yarn.executor.memoryOverhead': 2048,
                'spark.sql.shuffle.partitions': 512,
            },
        )

        # While we have not yet migrated the unique devices druid loading jobs to Airflow,
        # we need to generate _SUCCESS files so that Oozie is able to trigger them.
        # TODO: Remove this task once Druid loading jobs are in Airflow!
        mark_done = URLTouchOperator(
            task_id=f'write_{dataset_name}_success_file',
            url=dataset_props['success_file_path'],
        )

        # SQL operator for the dump of the metrics.
        dump = SparkSqlOperator(
            task_id=f'dump_{dataset_name}_metrics',
            sql=dataset_props['dump_hql_path'],
            query_parameters={
                'source_table': dataset_props['intermediate_table'],
                'destination_directory': dataset_props['temporary_directory'],
                'year': '{{execution_date.year}}',
                'month': '{{execution_date.month}}',
            },
        )

        # Archive operator to cleanly move the dump to its final destination.
        archive = HDFSArchiveOperator(
            task_id=f'move_{dataset_name}_data_to_archive',
            source_directory=dataset_props['temporary_directory'],
            check_done=True,
            archive_file=dataset_props['destination_path'],
            archive_parent_umask='022',
            archive_perms='644',
        )

        sensor >> compute >> mark_done >> dump >> archive
