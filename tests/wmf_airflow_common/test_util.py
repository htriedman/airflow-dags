from wmf_airflow_common import util


def test_wmf_airflow_instance_name(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is unset so we get dev env variants.
    monkeypatch.delenv('AIRFLOW_INSTANCE_NAME', raising=False)
    assert util.wmf_airflow_instance_name() is None


def test_wmf_airflow_instance_name_with_env_var(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf env variants.
    monkeypatch.setenv('AIRFLOW_INSTANCE_NAME', 'analytics-test')
    assert util.wmf_airflow_instance_name() == 'analytics-test'


def test_airflow_environment_name(monkeypatch):
    monkeypatch.delenv('AIRFLOW_ENVIRONMENT_NAME', raising=False)
    assert util.airflow_environment_name() is None


def test_airflow_environment_name_with_env_var(monkeypatch):
    monkeypatch.setenv('AIRFLOW_ENVIRONMENT_NAME', 'my-env-name')
    assert util.airflow_environment_name() == 'my-env-name'


def test_airflow_environment_name_with_instance_name_env_var(monkeypatch):
    monkeypatch.delenv('AIRFLOW_ENVIRONMENT_NAME', raising=False)
    monkeypatch.setenv('AIRFLOW_INSTANCE_NAME', 'analytics-test')
    assert util.airflow_environment_name() == 'wmf'


def test_is_wmf_airflow_instance(monkeypatch):
    # Make sure AIRFLOW_{ENVIRONMENT,INSTANCE}_NAME is unset so we get dev env variants.
    monkeypatch.delenv('AIRFLOW_ENVIRONMENT_NAME', raising=False)
    monkeypatch.delenv('AIRFLOW_INSTANCE_NAME', raising=False)
    assert util.is_wmf_airflow_instance() is False


def test_is_wmf_airflow_instance_with_env_var(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf env variants.
    monkeypatch.setenv('AIRFLOW_INSTANCE_NAME', 'analytics-test')
    assert util.is_wmf_airflow_instance() is True


def test_is_relative_uri():
    assert util.is_relative_uri('rel1')
    assert not util.is_relative_uri('/rel1')
    assert not util.is_relative_uri('file:///rel1')


def test_helpers_in_dev_environment(monkeypatch):
    # Make sure AIRFLOW_INSTANCE_NAME is set so we get wmf env variants.
    monkeypatch.setenv('AIRFLOW_INSTANCE_NAME', 'airflow-development-analytics-aqu')
    monkeypatch.setenv('AIRFLOW_ENVIRONMENT_NAME', 'dev_wmf')
    assert util.is_wmf_airflow_instance() is False
    assert util.airflow_environment_name() == 'dev_wmf'


def test_resolve_kwargs_default_args(monkeypatch):
    assert util.resolve_kwargs_default_args({}, 'k') is None
    assert util.resolve_kwargs_default_args({'k': 'v'}, 'k') == 'v'
    assert util.resolve_kwargs_default_args({'default_args': {'k': 'v'}}, 'k') == 'v'
    assert util.resolve_kwargs_default_args({'k': 'v', 'default_args': {'k': 'v2'}}, 'k') == 'v'

