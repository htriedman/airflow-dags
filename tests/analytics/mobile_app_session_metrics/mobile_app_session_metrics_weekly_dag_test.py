import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'mobile_app_session_metrics',
            'mobile_app_session_metrics_weekly_dag.py']


def test_mobile_app_session_metrics_weekly_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="mobile_app_session_metrics_weekly")
    assert dag is not None
    assert len(dag.tasks) == 2
