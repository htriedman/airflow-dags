import pendulum
from wmf_airflow_common.partitions_builder import (
    partition_names_by_granularity,
    _get_partition_time_part,
    _build_partitions_options,
    _add_pre_partition_options,
    build_partition_names,
    _build_partition_timestamps,
    add_post_partitions,
    daily_intervals,
)


def test_partition_name_by_granularity():
    # without pre_partitions
    partitions = partition_names_by_granularity('wmf.aqshourly', '@hourly')
    assert partitions == ['wmf.aqshourly/year={{execution_date.year}}'
                          '/month={{execution_date.month}}'
                          '/day={{execution_date.day}}/'
                          'hour={{execution_date.hour}}']
    # with pre_partitions
    partitions = partition_names_by_granularity(
        'wmf.aqs_hourly',
        '@hourly',
        pre_partitions=['webrequest_source=test_text'])
    assert partitions == ['wmf.aqs_hourly/'
                          'webrequest_source=test_text/'
                          'year={{execution_date.year}}/'
                          'month={{execution_date.month}}/'
                          'day={{execution_date.day}}/'
                          'hour={{execution_date.hour}}']
    # with timestamp
    timestamp = pendulum.parse('2022-02-10T16:43:34.825955+01:00')
    partitions = partition_names_by_granularity(
        'wmf.aqs_hourly',
        '@hourly',
        timestamp=timestamp,
        pre_partitions=['webrequest_source=test_text'])
    assert partitions == ['wmf.aqs_hourly/'
                          'webrequest_source=test_text/'
                          'year=2022/'
                          'month=2/'
                          'day=10/'
                          'hour=16']


def test_get_partition_part():
    assert _get_partition_time_part('month', None) == 'month={{execution_date.month}}'
    timestamp = pendulum.parse('2022-02-10T16:43:34.825955+01:00')
    assert _get_partition_time_part('month', timestamp) == 'month=2'


def test_build_pre_partitions_options():
    assert _build_partitions_options(['type=1', ['dc=a', 'dc=b']]) == (
        [['type=1', 'dc=a'], ['type=1', 'dc=b']])
    assert _build_partitions_options([['dc=a', 'dc=b'], 'type=1']) == (
        [['dc=a', 'type=1'], ['dc=b', 'type=1']])
    assert _build_partitions_options([[1, 2, 3], 4, [5, 6]]) == (
        [[1, 4, 5], [1, 4, 6], [2, 4, 5], [2, 4, 6], [3, 4, 5], [3, 4, 6]])


def test_add_pre_partition_options():
    assert _add_pre_partition_options(
        ['wmf.entity', 'year=2022'], ['dc=a', 't=1']) == [
            ['wmf.entity', 'dc=a', 't=1', 'year=2022']]
    assert _add_pre_partition_options(
        ['wmf.entity', 'year=2022'], [['dc=a', 'dc=b']]) == [
            ['wmf.entity', 'dc=a', 'year=2022'],
            ['wmf.entity', 'dc=b', 'year=2022']]


def test_build_partition_names_hourly():
    assert build_partition_names(
        'event.mediawiki_page_move',
        '2022-01-29T22:00:00+00:00',
        '2022-01-30T03:00:00+00:00',
        '@hourly') == [
            'event.mediawiki_page_move/year=2022/month=1/day=30/hour=2',
            'event.mediawiki_page_move/year=2022/month=1/day=30/hour=1',
            'event.mediawiki_page_move/year=2022/month=1/day=30/hour=0',
            'event.mediawiki_page_move/year=2022/month=1/day=29/hour=23',
            'event.mediawiki_page_move/year=2022/month=1/day=29/hour=22']


def test_build_partition_names_daily():
    assert build_partition_names(
        'event.mediawiki_page_move',
        '2022-01-29T00:00:00+00:00',
        '2022-01-30T23:59:59.999999+00:00',
        '@daily',
        pre_partitions=[['datacenter=eqiad', 'datacenter=codfw']]) == [
            'event.mediawiki_page_move/datacenter=codfw/year=2022/month=1/day=29',
            'event.mediawiki_page_move/datacenter=eqiad/year=2022/month=1/day=29']


def test_build_partition_names_monthly():
    assert build_partition_names(
        'event.mediawiki_page_move',
        '2022-01-01T00:00:00+00:00',
        '2022-02-01T00:00:00+00:00',
        '@monthly',
        pre_partitions=[['datacenter=eqiad', 'datacenter=codfw']]) == [
            'event.mediawiki_page_move/datacenter=codfw/year=2022/month=1',
            'event.mediawiki_page_move/datacenter=eqiad/year=2022/month=1']


def test_build_partition_timestamps():
    from_ts = '2022-01-29T00:00:00+00:00'
    to_ts = '2022-01-30T23:59:59.999999+00:00'
    assert _build_partition_timestamps(from_ts, to_ts, '@daily') == [pendulum.parse(from_ts)]

    from_ts = '2022-01-29T00:00:00+00:00'
    assert _build_partition_timestamps(from_ts, from_ts, '@daily') == []

    from_ts = '2022-03-08T00:00:00+00:00'
    to_ts = '2022-03-09T01:59:59.999999'
    assert len(_build_partition_timestamps(from_ts, to_ts, '@hourly')) == 25


def test_add_post_partitions():
    partitions = ['year=2022/month=1']
    post_partitions = [['wiki=en', 'wiki=zh']]
    assert add_post_partitions(partitions, post_partitions) == \
        ['year=2022/month=1/wiki=en', 'year=2022/month=1/wiki=zh']


def test_daily_intervals():
    intervals = daily_intervals()
    assert len(intervals) == 24
    assert intervals[0] == (
        "{{execution_date.add(hours=0).isoformat()}}/"
        "{{execution_date.add(hours=1).isoformat()}}"
    )
    assert intervals[-1] == (
        "{{execution_date.add(hours=23).isoformat()}}/"
        "{{execution_date.add(hours=24).isoformat()}}"
    )
