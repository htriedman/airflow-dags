"""
### Geo-editors public monthly-data
This job waits on editors_daily, then aggregates it into a monthly table.
It then generates a dump of the month that is published externally as a TSV file.

This dataset does NOT contain bot actions and only considers edit actions.

The generated data has fewer dimensions than the private dataset (editors_monthly)
and limits what data is output as follows:
  * respects a blacklist of countries
  * only reports '5 to 99' and '100+' activity levels
  * only reports numbers for Wikipedia projects
  * excludes private or closed wikis
  * excludes wikis with less than 3 active editors
  * aggregates registered and anonymous editors[2]
  * buckets output to obscure exact numbers (bucket size of 10)
--
[2] The final count of editors is an aggregate of both registered and
    anonymous editors. It may happen that an editor edits as both registered
    and anonymous in the same month. If so, that editor is going to be
    counted twice.
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

from analytics.config.dag_config import default_args, hql_directory, hadoop_name_node

dag_id = 'geoeditors_public_monthly'
var_props = VariableProperties(f'{dag_id}_config')
current_month = '{{ execution_date | to_ds_month }}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 8, 1)),
    schedule_interval='@monthly',
    tags=['archive', 'geoeditors', 'monthly', 'public'],
    user_defined_filters=filters,
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=10),
        }
    ),
) as dag:

    editors_daily_sensor = NamedHivePartitionSensor(
        task_id='wait_for_editors_daily',
        partition_names=[f'wmf.editors_daily/month={current_month}'],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    geoeditors_monthly_sensor = NamedHivePartitionSensor(
        task_id='wait_for_geoeditors_monthly',
        partition_names=[f'wmf.geoeditors_monthly/month={current_month}'],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    hql_parent_path = var_props.get(
        'hql_parent_path',
        f'{hql_directory}/geoeditors/public_monthly'
    )

    public_monthly_table = var_props.get('public_monthly_table', 'wmf.geoeditors_public_monthly')
    etl = SparkSqlOperator(
        task_id='process_geoeditors_public_monthly',
        sql=f'{hql_parent_path}/load_geoeditors_public_monthly_table.hql',
        query_parameters={
            'editors_daily_table': 'wmf.editors_daily',
            'geoeditors_monthly_table': 'wmf.geoeditors_monthly',
            'country_info_table': 'canonical_data.countries',
            'country_blacklist_table': 'wmf.geoeditors_blacklist_country',
            'project_namespace_map_table': 'wmf_raw.mediawiki_project_namespace_map',
            'destination_table': public_monthly_table,
            'month': current_month,
            'coalesce': 1
        },
    )

    temp_directory = var_props.get(
        'temp_directory',
        f"{hadoop_name_node}/wmf/tmp/analytics/geoeditors/{{{{ts_nodash}}}}-public-dump"
    )
    generate_dump = SparkSqlOperator(
        task_id='generate_geoeditors_dump',
        sql=f'{hql_parent_path}/generate_geoeditors_dump.hql',
        query_parameters={
            'source_table': public_monthly_table,
            'destination_directory': temp_directory,
            'month': current_month
        },
    )

    archive_directory = var_props.get(
        'archive_directory',
        f"{hadoop_name_node}/wmf/data/archive/geoeditors/public"
    )
    move_data_to_archive = HDFSArchiveOperator(
        task_id='move_data_to_archive',
        source_directory=temp_directory,
        expected_filename_ending='.csv',  # gets written by spark with .csv extension, but it's really a .tsv
        archive_file=f'{archive_directory}/geoeditors-monthly-{current_month}.tsv',
        check_done=True
    )

    [editors_daily_sensor, geoeditors_monthly_sensor] >> etl >> generate_dump >> move_data_to_archive
