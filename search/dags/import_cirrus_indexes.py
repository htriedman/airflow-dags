from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator
from datetime import datetime
from search.config.dag_config import data_path, \
        discolytics_conda_env_tgz, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

dag_id = 'import_cirrus_indexes'
var_props = VariableProperties(f'{dag_id}_conf')

cirrus_indexes_table = var_props.get('cirrus_indexes_table', 'discovery.cirrus_index')
cirrus_indexes_location = var_props.get('cirrus_indexes_location', 'cirrus/index')

default_args = get_default_args()

with DAG(
    f'{dag_id}_init',
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': datetime(2023, 1, 1),
    }),
    schedule_interval='@once',
) as dag_init:
    HiveOperator(
        task_id='create_tables',
        hql=f"""
            CREATE TABLE { cirrus_indexes_table } (
              `page_id` bigint,
              `auxiliary_text` array<string>,
              `category` array<string>,
              `content_model` string,
              `coordinates` array<struct<
                  `coord`: map<string, double>,
                  `country`:string,
                  `dim`:bigint,
                  `globe`:string,
                  `name`:string,
                  `primary`:boolean,
                  `region`:string,
                  `type`:string>>,
              `create_timestamp` string,
              `defaultsort` string,
              `descriptions` map<string, array<string>>,
              `external_link` array<string>,
              `extra_source` string,
              `file_bits` bigint,
              `file_height` bigint,
              `file_media_type` string,
              `file_mime` string,
              `file_resolution` bigint,
              `file_size` bigint,
              `file_text` string,
              `file_width` bigint,
              `heading` array<string>,
              `incoming_links` bigint,
              `labels` map<string, array<string>>,
              `language` string,
              `local_sites_with_dupe` array<string>,
              `namespace` bigint,
              `namespace_text` string,
              `opening_text` string,
              `outgoing_link` array<string>,
              `popularity_score` double,
              `redirect` array<struct<`namespace`:bigint,`title`:string>>,
              `source_text` string,
              `template` array<string>,
              `text` string,
              `text_bytes` bigint,
              `timestamp` string,
              `title` string,
              `version` bigint,
              `wikibase_item` string,
              `weighted_tags` array<string>,
              `statement_keywords` array<string>)
            PARTITIONED BY (
              `cirrus_replica` string,
              `cirrus_group` string,
              `wiki` string,
              `snapshot` string)
            STORED AS avro
            LOCATION '{ data_path }/{ cirrus_indexes_location }'
        """
    ) >> DummyOperator(task_id='complete')


with DAG(
    'import_cirrus_indexes_weekly',
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': var_props.get_datetime('start_date', datetime(2023, 3, 5)),
    }),
    schedule_interval='0 0 * * 0',
    max_active_runs=1,
    catchup=False,
) as weekly_dag:
    SparkSubmitOperator.for_virtualenv(
        task_id='import',
        conf=dict(default_args.get('conf', {}), **{
            # This seems to help reduce the memory overhead necessary. If too
            # small a specific exception will let us know we are using more direct
            # buffers than expected, rather than a generic killed by yarn error.
            'spark.executor.extraJavaOptions': '-XX:MaxDirectMemorySize=1024M',
        }),
        # Keep down the number of connections we are making to the prod cluster
        # by limiting executors
        executor_cores=1,
        executor_memory='4g',
        # Outputing giant rows seems to need excessive memory overhead to work
        executor_memory_overhead='4g',
        max_executors=36,
        driver_memory='8g',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/import_cirrus_indexes.py',
        application_args=[
            '--elasticsearch', ",".join(var_props.get_list('elasticsearch_clusters', [
                "https://search.svc.codfw.wmnet:9243",
                "https://search.svc.codfw.wmnet:9443",
                "https://search.svc.codfw.wmnet:9643",
            ])),
            '--output-partition',  cirrus_indexes_table + '/snapshot={{ ds_nodash }}'
        ]
    ) >> DummyOperator(task_id='complete')
