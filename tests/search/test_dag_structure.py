from airflow.models import DagBag, DagModel, Pool
from airflow.sensors.external_task import ExternalTaskSensor
from airflow.providers.apache.spark.operators.spark_submit \
    import SparkSubmitOperator as AirflowSparkSubmitOperator
from airflow.secrets.metastore import MetastoreBackend
import json
import os
from pathlib import Path
import pytest
from search.shared.utils import parse_memory_to_mb
from unittest import mock
from wmf_airflow_common.operators.spark import SparkSubmitOperator


def all_dag_paths():
    test_path = Path(__file__)
    instance_name = test_path.parts[-2]
    base_dir = test_path.parents[2]
    dags_dir = Path(base_dir, instance_name, 'dags')
    return [
        list(path.relative_to(base_dir).parts) 
        for path in dags_dir.glob('**/*.py')
    ]


def collect_all_tasks():
    # WARNING: This is run prior to the airflow fixture, so environment
    # variables are not configured. We collect only information about
    # the tasks, but not the tasks themselves, as they may have incorrect
    # info
    # We have to set some instance specific environment vars. Sadly fixtures
    # are the only way to share between test files, but we can't access those
    # here and need to duplicate the env. This is still not the complete env,
    # just enough to get the same task list.
    # Also, the database is likely not configured yet. We need to mock out
    # bits that would talk to the database during dag parsing to keep them
    # quiet.
    override_env = {
        'AIRFLOW_VAR_MJOLNIR_WEEKLY_CONFIG': json.dumps({
            # Limit the number of duplicate fixtures generated
            'wikis': ['arwiki', 'enwiki'],
        })
    }
    with mock.patch.dict(os.environ, override_env, clear=False), \
            mock.patch.object(DagModel, 'get_current') as get_current, \
            mock.patch.object(MetastoreBackend, 'get_variable') as get_variable:
        get_current.return_value = False
        get_variable.return_value = None

        for dag_path in all_dag_paths():
            dag_bag = DagBag(None, include_examples=False, read_dags_from_db=False)
            dag_file = os.path.join(os.path.abspath(os.getcwd()), *dag_path)
            dag_bag.process_file(dag_file)
            for dag_id in dag_bag.dag_ids:
                dag = dag_bag.get_dag(dag_id)
                for task in dag.tasks:
                    yield (dag_path, dag_id, type(task), task.task_id)


all_tasks = list(collect_all_tasks())

def for_each_task(kind=object):
    tests = []
    for dag_path, dag_id, task_kind, task_id in all_tasks:
        if issubclass(task_kind, kind):
            param = pytest.param(dag_path, dag_id, task_id, id=f'{dag_id}-{task_id}')
            tests.append(param)
    return (('dag_path', 'dag_id', 'task_id'), tests)


@pytest.fixture
def task(dagbag, dag_id, task_id):
    return dagbag.get_dag(dag_id).get_task(task_id)


@pytest.mark.parametrize('dag_path', all_dag_paths())
def test_clean_import(dagbag):
    assert dagbag.import_errors == {}
    assert len(dagbag.dag_ids) > 0


@pytest.mark.parametrize(*for_each_task())
def test_tasks_are_renderable(task, render_task):
    # Nothing specific to test here, simply rendering the task is sufficient
    # to verify tasks are renderable. On failure jinja2.exceptions.TemplateSyntaxError will
    # be thrown from render_task.
    render_task(task)


@pytest.mark.parametrize(*for_each_task(AirflowSparkSubmitOperator))
def test_dont_use_upstream_spark_submit(task):
    assert isinstance(task, SparkSubmitOperator), \
        'Tasks must use the wmf SparkSubmitOperator implementation'


def _sort_items_recursive(maybe_dict):
    """Recursively sort dictionaries so iteration gives deterministic outputs"""
    if hasattr(maybe_dict, 'items'):
        items = ((k, _sort_items_recursive(v)) for k, v in maybe_dict.items())
        return dict(sorted(items, key=lambda x: x[0]))
    else:
        return maybe_dict


@pytest.mark.parametrize(*for_each_task(SparkSubmitOperator))
def test_spark_submit_cli_args_against_fixture(task, render_task, compare_with_fixture):
    rendered_task = render_task(task)
    # The conf dict comes out in arbitrary order, sort for stable cli command output.
    # Must come after rendering templates, as they may replace or re-order the dict
    if rendered_task._conf is not None:
        rendered_task._conf = _sort_items_recursive(rendered_task._conf)

    command = rendered_task._get_hook()._build_spark_submit_command()
    # Make sure we didn't pass numbers when strings were expected
    assert all(isinstance(x, str) for x in command), str(command)

    compare_with_fixture(
        group='spark_submit_operator',
        fixture_id=f'{task.dag_id}-{task.task_id}',
        content=command)


@pytest.mark.parametrize(*for_each_task(SparkSubmitOperator))
def test_spark_submit_uses_standard_arguments(task):
    """Test that spark operators uses same arguments

    There are parts of spark that can be configured through conf or through
    spark-submit cli args. Prefer cli args for consistency.
    """
    assert 'spark.executor.memory' not in task._conf, \
        'Executor memory must be configured through kwarg'
    assert 'spark.executor.cores' not in task._conf, \
        'Executor cores must be configured through kwarg'


@pytest.mark.parametrize(*for_each_task(SparkSubmitOperator))
def test_spark_submit_sizing(task):
    max_exec = int(task._conf['spark.dynamicAllocation.maxExecutors'])
    exec_mem_gb = parse_memory_to_mb(task._executor_memory if task._executor_memory is not None else '1G') / 1024
    max_mem_gb = max_exec * exec_mem_gb
    if max_mem_gb > 420:
        assert task.pool != Pool.DEFAULT_POOL_NAME, \
            'Large job (mem > 420g) should not be using the default pool'

    if max_mem_gb > 800:
        assert task.pool == 'sequential', \
            'Large job (mem > 800g) should be using the sequential pool'

    exec_cores = int(task._executor_cores if task._executor_cores is not None else '1')
    task_cores = int(task._conf['spark.task.cpus'] if 'spark.task.cpus' in task._conf else '1')

    assert (exec_cores % task_cores) == 0, \
        'executor_cores should be a multiple of spark.task.cpus'
    assert (exec_cores / task_cores) < exec_mem_gb, \
        'executor_memory looks suspiciously low (less than 1G per executor task)'

EMAIL_WHITELIST = {
    'discovery-alerts@lists.wikimedia.org',
    'sd-alerts@lists.wikimedia.org'
}


@pytest.mark.parametrize(*for_each_task())
def test_task_email_is_whitelisted(task):
    """Help prevent typos in alerting emails"""
    for email in task.email:
        assert email in EMAIL_WHITELIST


@pytest.mark.parametrize(*for_each_task(ExternalTaskSensor))
def test_external_task_exists(task):
    all_dag_ids = {x[1] for x in all_tasks}
    assert task.external_dag_id in all_dag_ids
    dag_task_ids = {x[3] for x in all_tasks if x[1] == task.external_dag_id}
    assert task.external_task_id in dag_task_ids
