import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'apis', 'apis_metrics_to_graphite_hourly_dag.py']


def test_apis_hourly_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="apis_metrics_to_graphite_hourly")
    assert dag is not None
    assert len(dag.tasks) == 2
    etl = dag.get_task('generate_and_send_apis_metrics_to_graphite')
    application_parameters = etl._application_args
    assert len(application_parameters) == 16
    assert '--metric_prefix' not in application_parameters
