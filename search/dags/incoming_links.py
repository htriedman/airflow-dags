from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.hive_operator import HiveOperator
from airflow.sensors.external_task import ExternalTaskSensor
from datetime import datetime, timedelta
from search.config.dag_config import data_path, get_default_args, discolytics_conda_env_tgz
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

dag_id = 'incoming_links_weekly'
var_props = VariableProperties(f'{dag_id}_config')

default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    # first execution week of 11/13 - 11/20
    'start_date': var_props.get_datetime('start_date', datetime(2022, 11, 13)),
})

incoming_links_table = var_props.get('incoming_links_table', 'discovery.incoming_links_update')
incoming_links_location = var_props.get('incoming_links_location', 'cirrus/incoming_links_update')
cirrus_indexes_table = var_props.get('cirrus_indexes_table', 'discovery.cirrus_index')

with DAG(
    f'{dag_id}_init',
    schedule_interval='@once',
    default_args=default_args,
) as dag_init:
    HiveOperator(
        task_id='create_tables',
        hql=f"""
            CREATE TABLE { incoming_links_table } (
              `wikiid` string,
              `page_id` bigint,
              `page_namespace` int,
              `dbkey` string,
              `incoming_links` bigint)
            PARTITIONED BY (
              `snapshot` string)
            STORED AS parquet
            LOCATION '{ data_path }/{ incoming_links_location }'
        """
    ) >> DummyOperator(task_id='complete')


with DAG(
    dag_id,
    default_args=default_args,
    schedule_interval='0 0 * * 0',
    max_active_runs=1,
    catchup=True,
) as weekly_dag:
    # Waiting for specific hive data is difficult, as the source
    # job writes many partitions. Instead wait for the complete task
    wait_for_data = ExternalTaskSensor(
        task_id='wait_for_data',
        sla=timedelta(hours=28),
        external_dag_id='import_cirrus_indexes_weekly',
        external_task_id='complete',
    )

    generate = SparkSubmitOperator.for_virtualenv(
        task_id='generate_incoming_link_counts',
        conf=dict(default_args.get('conf', {}), **{
            # spark spends a lot of time running only a couple tasks. It might have 400 cores
            # in active executors, but only run 4 active tasks. This seems to fix that
            'spark.locality.wait': '0',
            # The incoming links aggregation emits 11 billion rows, this seems
            # to avoid shuffle fetch failures/task retries when dealing with that.
            'spark.reducer.maxReqsInFlight': '1',
            'spark.shuffle.io.retryWait': '120s',
        }),
        max_executors=200,
        # Due to SPARK-20880 reading avro from spark2 must be done with a single executor per jvm
        executor_cores=1,
        executor_memory='2g',
        # Reading the dumps requires a little extra memory, some rows are giant.
        executor_memory_overhead='768M',
        # The total job involves ~75k tasks, a little extra memory seems to help the driver
        # avoid large pauses.
        driver_memory='4g',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/incoming_links.py',
        application_args=[
            '--cirrus-dump', cirrus_indexes_table + '/snapshot={{ ds_nodash }}',
            '--output-partition', incoming_links_table + '/snapshot={{ ds_nodash }}',
        ]
    )

    wait_for_data >> generate >> DummyOperator(task_id='complete')
