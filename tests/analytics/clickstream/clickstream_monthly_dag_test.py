import pytest
from datetime import timedelta


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'clickstream', 'clickstream_monthly_dag.py']


def test_clickstream_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="clickstream_monthly")
    assert dag is not None
    assert len(dag.tasks) == 17
    enwiki_archive_task = dag.get_task('archive_enwiki')
    assert enwiki_archive_task.sla == timedelta(days=10)
    mw_page_sensor = dag.get_task('wait_for_mediawiki_page_snapshots')
    partitions = mw_page_sensor.partition_names
    assert len(partitions) == 11
    partitions.sort()
    assert partitions[0] == \
            'wmf_raw.mediawiki_page/snapshot={{execution_date | to_ds_month}}/wiki_db=dewiki'
    assert int(mw_page_sensor.poke_interval) == 3600*2  # 2 hours
