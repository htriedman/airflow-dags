from typing import cast, List, Optional, Union, NewType

from datetime import datetime
import pendulum
from pendulum.datetime import DateTime


# Declare PrePartitions as a type for convenient reference.
PrePartitions = NewType('PrePartitions', List[Union[str, List[str]]])
"""
A PrePartitions type represents a graph of partition parts to insert between the
table name and the time-related partition parts. Its structure allows the creation
of combinations of partition parts.

Examples with `mytable` partitioned by year, for 2022:

- pre_partitions = [] ->
    ['mytable/year=2022/']

- pre_partitions = ['type=text'] ->
    ['mytable/type=text/year=2022/']

-  pre_partitions = ['type=text', 'dc=eqiad'] ->
    ['mytable/type=text/dc=eqiad/year=2022/']

- pre_partitions = ['type=text', ['dc=eqiad', 'dc=drmrs']] ->
    [
        'mytable/type=text/dc=eqiad/year=2022/',
        'mytable/type=text/dc=drmrs/year=2022/',
    ]
"""

# Translate adverb to noun.
granularity_to_word = {'@hourly': 'hour',
                       '@daily': 'day',
                       '@weekly': 'week',
                       '@monthly': 'month'}


def daily_partitions(table: str, granularity: str, pre_partitions: Optional[PrePartitions] = None) -> List[str]:
    """Gets a list of partitions for the given table and time granularity.
       The returned partitions contain jinja template placeholders,
       to be resolved by Airflow (i.e.: {{ execution_date.year }}).
       Args:
           table          Fully qualified Hive table name.
                          (i.e.: wmf.anomaly_detection)
           granularity    Table granularity: either '@hourly' or '@daily'.
    """
    full_day_partition = _partition_name_by_day(table, pre_partitions)
    if granularity == '@daily':
        partitions = [full_day_partition]
    else:  # if granularity == '@hourly':
        partitions = [
            '/'.join([
                full_day_partition,
                f'hour={ hour }',
            ])
            for hour in range(0, 24)
        ]
    return partitions


def daily_intervals() -> List[str]:
    """
    Returns a list of 24 interval strings in the format "<start>/<end>",
    one for each hour of {{execution_date}}, all ISO formatted.
    """
    return [
        f"{{{{execution_date.add(hours={i}).isoformat()}}}}/" +
        f"{{{{execution_date.add(hours={i + 1}).isoformat()}}}}"
        for i in range(24)
    ]


def _partition_name_by_day(table: str, pre_partitions: Optional[PrePartitions] = None) -> str:
    """Gets a fully qualified daily partition name for a give Hive table.
       The returned partitions contain jinja template placeholders,
       to be resolved by Airflow (i.e.: {{ execution_date.year }}).
       Args:
           table          Fully qualified Hive table name.
                          (i.e.: wmf.anomaly_detection)
           pre_partitions A list of pre_preparitions.
                          Could be strings or partition options.
    """
    if not pre_partitions:
        pre_partitions = cast(PrePartitions, [])
    return '/'.join([
        table,
        *pre_partitions,  # TODO what if pre_partitions is a List[List[str]] ???
        'year={{ execution_date.year }}',
        'month={{ execution_date.month }}',
        'day={{ execution_date.day }}'
    ])


def _build_partition_timestamps(
    from_timestamp: str,
    to_timestamp: str,
    granularity: str = '@hourly'
) -> List[datetime]:
    """Builds a list of timestamps between 2 dates matching the granularity.
       The timestamps are set at the beginning of each steps.
       Args:
           from_timestamp (str)
           to_timestamp (str)
    """
    from_timestamp_parsed = cast(DateTime, pendulum.parse(from_timestamp))
    to_timestamp_parsed = cast(DateTime, pendulum.parse(to_timestamp))
    start = from_timestamp_parsed.start_of(granularity_to_word[granularity])
    diff = to_timestamp_parsed.start_of(granularity_to_word[granularity]) - start
    if granularity == '@hourly':
        return [start.add(hours=h) for h in range(diff.in_hours())]
    if granularity == '@daily':
        return [start.add(days=d) for d in range(diff.in_days())]
    if granularity == '@monthly':
        return [start.add(months=m) for m in range(diff.in_months())]
    raise Exception(f'granularity {granularity} not implemented.')


def build_partition_names(
    table: str,
    from_timestamp: str,
    to_timestamp: str,
    granularity: str,
    pre_partitions: Optional[PrePartitions] = None
) -> List[str]:
    """Builds a list of partition names between 2 dates.
       The timestamps are set at the beginning of each steps.
       Args:
           table (str)
           from_timestamp (str)
           to_timestamp (str)
           granularity (str)
           pre_partitions        A list of pre_preparitions.
                                 Could be strings or partition options.
    """
    if not pre_partitions:
        pre_partitions = cast(PrePartitions, [])
    partition_timestamps = _build_partition_timestamps(
        from_timestamp, to_timestamp, granularity)
    names = []
    for timestamp in partition_timestamps:
        name = partition_names_by_granularity(
            table,
            granularity,
            timestamp=timestamp,
            pre_partitions=pre_partitions
        )
        if isinstance(name, list):
            names.extend(name)
        else:
            names.append(name)
    names.reverse()  # Check the last partition first
    return names


def snapshot_partition_names(
    table: str,
    granularity: str,
    pre_partitions: Optional[PrePartitions] = None,
) -> List[str]:
    """
    Build 1 or multiple snapshot partition names.
    (<database>.<table>/snapshot=<snapshot_value>)
    :param table:
        Fully qualified Hive table name of the dataset.
    :param granularity:
        Granularity of the dataset: @weekly, @weekly_starting_monday or @monthly.
    """
    if not pre_partitions:
        pre_partitions = cast(PrePartitions, [])
    if granularity in ["@weekly", "@weekly_starting_monday"]:
        snapshot_value = "{{ execution_date.format('YYYY-MM-DD') }}"
    elif granularity in ["@monthly"]:
        snapshot_value = "{{ execution_date.format('YYYY-MM') }}"
    partitions = _add_pre_partition_options(
        [table, f"snapshot={snapshot_value}"],
        pre_partitions,
    )
    return ['/'.join(p) for p in partitions]


def partition_names_by_granularity(
    table: str,
    granularity: str,
    timestamp: Optional[datetime] = None,
    pre_partitions: Optional[PrePartitions] = None
) -> List[str]:
    """ Build 1 or multiple partition names.
        It could be a partition:
            - with a specific date (ex: wmf.table/year=2022)
            - with a templated date (ex: wmf.table/year={{execution_date.year}})
        It could be a list of dates if there are multiple pre_partitions options.
        ex: [wmf.table/dc=1/year=2022, wmf.table/dc=2/year=2022]
    """
    if not pre_partitions:
        pre_partitions = cast(PrePartitions, [])
    assert granularity in ['@monthly', '@daily', '@hourly'], \
        f"Unknown granularity {granularity}"
    elements = [table, _get_partition_time_part('year', timestamp)]
    if granularity in ['@monthly', '@daily', '@hourly']:
        elements.append(_get_partition_time_part('month', timestamp))
    if granularity in ['@daily', '@hourly']:
        elements.append(_get_partition_time_part('day', timestamp))
    if granularity == '@hourly':
        elements.append(_get_partition_time_part('hour', timestamp))
    partitions = _add_pre_partition_options(elements, pre_partitions)
    return ['/'.join(partition) for partition in partitions]


def _get_partition_time_part(part: str, timestamp: Optional[datetime] = None) -> str:
    """Build a time part of a partition. ex: year=2022 or year={{execution_date.year}}"""
    if timestamp:
        return f'{part}={getattr(timestamp, part)}'
    return f'{part}={{{{execution_date.{part}}}}}'


def _add_pre_partition_options(
    partitions: List[str],
    pre_partitions: PrePartitions) -> List[List[str]]:
    """
    Add the options before the other partitions
    eg: input: True, ['wmf.table', 'type=1'], ['dc=a', 'dc=b']
        output: [['wmf.table', 'dc=a', 'type=1'], ['wmf.table', 'dc=b', 'type=1']]
    """
    return _add_options_to_partitions(
        pre=True,
        partitions=partitions,
        partition_options=pre_partitions
    )

def add_post_partitions(partitions: List[str], post_partitions: PrePartitions) -> List[str]:
    """
    Build a list of partitions by appending the post_partitions options after the provided
    partitions.

    :param partitions: eg: ['wmf.table', 'year=2022', 'month=1'] or ['wmf.table/year=2022/month=1']
    :param post_partitions: A list  describing the options to add to the partitions
      eg [['dc=1', 'dc=2']]
    :return: A list partitions with post partitions
      eg: ['wmf.table/year=2022/month=1/dc=1', 'wmf.table/year=2022/month=1/dc=2']
    """
    _partitions = _add_options_to_partitions(
        pre=False,
        partitions=partitions,
        partition_options=post_partitions
    )
    return ['/'.join(partition) for partition in _partitions]


def _add_options_to_partitions(
    pre: bool,
    partitions: List[str],
    partition_options: PrePartitions) -> List[List[str]]:
    """
    Add the options before or after the other partitions
    eg: input: True, ['wmf.table', 'type=1'], ['dc=a', 'dc=b']
        output: [['wmf.table', 'dc=a', 'type=1'], ['wmf.table', 'dc=b', 'type=1']]
    """
    if not partition_options:
        return [partitions]
    result = []
    for options in _build_partitions_options(partition_options):
        partition_copy = partitions.copy()
        options.reverse()
        for option in options:
            if pre:
                partition_copy.insert(1, option)
            else:
                partition_copy.append(option)
        result.append(partition_copy)
    return result


def _build_partitions_options(pre_partitions: PrePartitions) -> List[List[str]]:
    """Build the different options of partitions resulting from the different
    options of pre_partitions.

    ex:
        input: ['type=1', ['dc=a', 'dc=b']]
        output: [['type=1', 'dc=a'], ['type=1', 'dc=b']]
    """

    # TODO: This is hacky.  What is pre_partitions[0] supposed to be?
    # first_pre_partition: Union[str, List[str]] = pre_partitions[0]
    # if not isinstance(pre_partitions[0], list):
    #     first_pre_partition = [first_pre_partition]

    if isinstance(pre_partitions[0], list):
        first_pre_partition = pre_partitions[0]
    else:
        first_pre_partition = [pre_partitions[0]]
    result = [[part] for part in first_pre_partition]
    for part in pre_partitions[1:]:
        if isinstance(part, list):
            for line in result.copy():
                result.remove(line)
                for option in part:
                    line_copy = line.copy()
                    line_copy.append(option)
                    result.append(line_copy)
        else:
            for line in result:
                line.append(part)
    return result
