
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from datetime import datetime
from pytest import raises
from wmf_airflow_common.dataset import HiveDataset, HiveSnapshotDataset
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor


def test_invalid_table_name_hive_dataset(monkeypatch):
    with raises(ValueError):
        HiveDataset(
            table_name="invalid",
            partitioning="@hourly",
        )


def test_invalid_partitioning_hive_dataset(monkeypatch):
    with raises(ValueError):
        HiveDataset(
            table_name="database.table",
            partitioning="@invalid",
        )


def test_invalid_pre_partitions_hive_dataset(monkeypatch):
    with raises(ValueError):
        HiveDataset(
            table_name="database.table",
            partitioning="@hourly",
            pre_partitions=["invalid"]
        )


def test_hive_dataset_get_same_partitioning_sensor(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@daily",
        pre_partitions=["name=value"],
    )
    dag = DAG(
        dag_id="test_dag",
        schedule_interval="@daily",
        start_date=datetime(2023, 1, 1),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.partition_names == [
        "database.table/name=value/year={{execution_date.year}}/" +
        "month={{execution_date.month}}/day={{execution_date.day}}"
    ]
    assert sensor.poke_interval == 900


def test_hive_dataset_get_hourly_to_daily_sensor(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@hourly",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule_interval="@daily",
        start_date=datetime(2023, 1, 1),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, NamedHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.partition_names == [
        "database.table/year={{ execution_date.year }}/month={{ execution_date.month }}/" +
        "day={{ execution_date.day }}/hour=" + str(i)
        for i in range(24)
    ]
    assert sensor.poke_interval == 900


def test_hive_dataset_get_daily_to_weekly_sensor(monkeypatch):
    dataset = HiveDataset(
        table_name="database.table",
        partitioning="@daily",
    )
    dag = DAG(
        dag_id="test_dag",
        schedule_interval="0 0 * * 1",  # Weekly starting Mondays
        start_date=datetime(2023, 1, 2),
    )
    sensor = dataset.get_sensor_for(dag)
    assert isinstance(sensor, RangeHivePartitionSensor)
    assert sensor.task_id == "wait_for_database_table_partitions"
    assert sensor.table_name == "database.table"
    assert sensor.from_timestamp == "{{execution_date}}"
    assert sensor.to_timestamp == "{{execution_date.add(days=7)}}"
    assert sensor.granularity == "@daily"
    assert sensor.poke_interval == 1800


def test_hive_dataset_get_weekly_snapshot_sensor(monkeypatch):
        dataset = HiveSnapshotDataset(
            table_name="database.table",
            partitioning="@weekly",
        )
        dag = DAG(
            dag_id="test_dag",
            schedule_interval="@weekly",
            start_date=datetime(2023, 1, 2),
        )
        sensor = dataset.get_sensor_for(dag)
        assert isinstance(sensor, NamedHivePartitionSensor)
        assert sensor.task_id == "wait_for_database_table_partitions"
        assert sensor.partition_names == [
            "database.table/snapshot={{ execution_date.format('YYYY-MM-DD') }}"
        ]
        assert sensor.poke_interval == 1800


def test_hive_dataset_get_monthly_snapshot_sensor(monkeypatch):
        dataset = HiveSnapshotDataset(
            table_name="database.table",
            partitioning="@monthly",
            pre_partitions=[["dc=1", "dc=2"]],
        )
        dag = DAG(
            dag_id="test_dag",
            schedule_interval="@monthly",
            start_date=datetime(2023, 1, 1),
        )
        sensor = dataset.get_sensor_for(dag)
        assert isinstance(sensor, NamedHivePartitionSensor)
        assert sensor.task_id == "wait_for_database_table_partitions"
        assert sensor.partition_names == [
            "database.table/dc=1/snapshot={{ execution_date.format('YYYY-MM') }}",
            "database.table/dc=2/snapshot={{ execution_date.format('YYYY-MM') }}",
        ]
        assert sensor.poke_interval == 3600
