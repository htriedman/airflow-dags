"""Init DAG
    Create table and partitions for process_sparql_query_hourly dag
   Process Query
    Runs hourly. Ingests sparql queries from input hive table and
    extracts various components of the query. Saves the extracted
    queries in output hive table.

"""
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from search.config.dag_config import data_path, wdqs_spark_tools, YMDH_PARTITION, eventgate_partitions, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

var_props = VariableProperties('process_sparql_query_conf')

input_table = var_props.get('event_sparql_query_table', 'event.wdqs_external_sparql_query')
output_table = var_props.get('discovery_processed_sparql_query_table', 'discovery.processed_external_sparql_query')
wiki = var_props.get('wiki', 'wikidata')
rel_processed_sparql_query_location = var_props.get('rel_processed_sparql_query_location',
                                                    'query_service/processed_external_sparql_query')
datacenters = ["eqiad", "codfw"]

HQL = f"""
CREATE TABLE IF NOT EXISTS {input_table} (
    `id`                string  COMMENT 'Query Id',
    `query`             string  COMMENT 'The sparql query',
    `query_time`        bigint  COMMENT 'Time taken to run the query',
    `query_time_class`  string  COMMENT 'Bucketed time taken to run the query',
    `ua`                string  COMMENT 'User agent',
    `q_info`            struct<
                            queryReprinted: string,
                            opList: array<string>,
                            operators: map<string, bigint>,
                            prefixes: map<string, bigint>,
                            nodes: map<string, bigint>,
                            services: map<string, bigint>,
                            wikidataNames: map<string, bigint>,
                            expressions: map<string, bigint>,
                            paths: map<string, bigint>,
                            triples: array<
                                struct<
                                    subjectNode: struct<nodeType: string,nodeValue: string>,
                                    predicateNode: struct<nodeType: string,nodeValue: string>,
                                    objectNode: struct<nodeType: string,nodeValue: string>
                                    >
                                >
                            >
                                COMMENT 'Extracted information from the query string'
)
PARTITIONED BY (
    `year`              int     COMMENT 'Unpadded year of queries',
    `month`             int     COMMENT 'Unpadded month of queries',
    `day`               int     COMMENT 'Unpadded day of queries',
    `hour`              int     COMMENT 'Unpadded hour of queries',
    `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
)
STORED AS PARQUET
LOCATION '{data_path}/{rel_processed_sparql_query_location}'
"""

merged_default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2021, 6, 1)),
})

with DAG(
        'process_sparql_query_init',
        default_args=merged_default_args,
        schedule_interval='@once'
) as dag_init:
    complete = DummyOperator(task_id='complete')
    HiveOperator(
        task_id='create_tables',
        hql=HQL
    ) >> complete

with DAG(
        'process_sparql_query_hourly',
        default_args=merged_default_args,
        schedule_interval='@hourly',
        # The spark job uses minimal internal parallelism, increase top level
        # parallelism to allow backfills to complete in under a week.
        max_active_runs=8,
        catchup=True,
) as dag:
    # Select single hourly partition
    output_table_and_partition = f'{output_table}/{YMDH_PARTITION}/wiki={wiki}'
    input_table_and_partition = f'{input_table}/{YMDH_PARTITION}'

    wait_for_data = NamedHivePartitionSensor(
        task_id='wait_for_data',
        sla=timedelta(hours=6),
        retries=4,
        partition_names=eventgate_partitions(input_table),
    )

    # Extract the sparql queries from table and
    # process query to proper format for saving.
    extract_queries = SparkSubmitOperator(
        task_id='extract_queries',
        application=wdqs_spark_tools,
        java_class="org.wikidata.query.rdf.spark.transform.queries.sparql.QueryExtractor",
        # The job is a straight map, no shuffle occurs and the data is
        # small enough (< 200MB per run) that the job doesn't use more than
        # a couple tasks, which fit in a single executor.
        max_executors=1,
        executor_cores=8,
        executor_memory="16g",
        driver_memory="2g",
        application_args=[
            '--input-table', input_table_and_partition,
            '--output-table', output_table_and_partition,
        ],
    )

    complete = DummyOperator(task_id='complete')

    wait_for_data >> extract_queries >> complete
