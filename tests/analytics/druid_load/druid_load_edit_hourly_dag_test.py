import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'druid_load', 'druid_load_edit_hourly_dag.py']


def test_druid_load_editattemptstep_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="druid_load_edit_hourly")
    assert dag is not None
    assert len(dag.tasks) == 2
