'''

This job loads the specified datasets listed below monthly
from their respective hive tables to cassandra

The datasets covered within this job include:
   editors_by_country,
   mediarequest_per_referer,
   mediarequest_top_files,
   pageview_per_project,
   pageview_top_articles,
   pageview_top_per_country,
   unique_devices

'''

from airflow import DAG
from datetime import datetime, timedelta
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.templates.time_filters import filters
from wmf_airflow_common.operators.spark import SparkSqlOperator
from analytics.config.dag_config import default_args, hql_directory, cassandra_default_conf, artifact

dag_id = 'cassandra_monthly_load'
var_props = VariableProperties(f'{dag_id}_config')
execution_conf =  var_props.get(
    'conf',
    {
        **cassandra_default_conf,
        'spark.dynamicAllocation.maxExecutors': 128,
        'spark.yarn.maxAppAttempts': 1,
        'spark.executor.memoryOverhead': 3072
    }
)

jobs_properties = var_props.get('jobs_properties',  {
    'editors_by_country': {
        # The "wait_for_partitions" property contains the list of partitions
        # to wait for when using the NamedHivePartitionSensor.
        # If not set, the sensor uses the RangeHivePartitionSensor.
        'wait_for_partitions': ['wmf.geoeditors_public_monthly/month={{execution_date|to_ds_month}}'],
        'hql_parameters': {
            'source_table': 'wmf.geoeditors_public_monthly',
            'destination_table': 'aqs.local_group_default_T_editors_bycountry.data'
        },
        'hql_path': f'{hql_directory}/cassandra/monthly/load_cassandra_editors_bycountry_monthly.hql'
    },
    'mediarequest_per_referer': {
        'hql_parameters': {
            'source_table': 'wmf.mediarequest',
            'destination_table': 'aqs.local_group_default_T_mediarequest_per_referer.data'
        },
        'hql_path': f'{hql_directory}/cassandra/monthly/load_cassandra_mediarequest_per_referer_monthly.hql',
        'source_granularity': '@hourly'
    },
    'mediarequest_top_files': {
        'hql_parameters': {
            'source_table': 'wmf.mediarequest',
            'destination_table': 'aqs.local_group_default_T_mediarequest_top_files.data'
        },
        'hql_path': f'{hql_directory}/cassandra/monthly/load_cassandra_mediarequest_top_files_monthly.hql',
        'source_granularity': '@hourly'
    },
    'pageview_per_project': {
        'hql_parameters': {
            'source_table': 'wmf.projectview_hourly',
            'destination_table': 'aqs.local_group_default_T_pageviews_per_project_v2.data'
        },
        'hql_path': f'{hql_directory}/cassandra/monthly/load_cassandra_pageview_per_project_monthly.hql',
        'source_granularity': '@hourly'
    },
    'pageview_top_articles': {
        'hql_parameters': {
            'source_table': 'wmf.pageview_hourly',
            'destination_table': 'aqs.local_group_default_T_top_pageviews.data'
        },
        'hql_path': f'{hql_directory}/cassandra/monthly/load_cassandra_pageview_top_articles_monthly.hql',
        'source_granularity': '@hourly'
    },
    'pageview_top_by_country': {
        'hql_parameters': {
            'source_table': 'wmf.projectview_hourly',
            'country_deny_list_table': 'wmf.geoeditors_blacklist_country',
            'destination_table': 'aqs.local_group_default_T_top_bycountry.data',
        },
        'hql_path': f'{hql_directory}/cassandra/monthly/load_cassandra_pageview_top_bycountry_monthly.hql',
        'source_granularity': '@hourly'
    },
    'unique_devices': {
        # The "wait_for_partitions" property contains the list of partitions
        # to wait for when using the NamedHivePartitionSensor.
        # If not set, the sensor uses the RangeHivePartitionSensor.
        'wait_for_partitions': [
            'wmf.unique_devices_per_domain_monthly/year={{execution_date.year}}/month={{execution_date.month}}',
            'wmf.unique_devices_per_project_family_monthly/year={{execution_date.year}}/month={{execution_date.month}}'
        ],
        'hql_parameters': {
            'source_table_per_domain': 'wmf.unique_devices_per_domain_monthly',
            'source_table_per_project_family': 'wmf.unique_devices_per_project_family_monthly',
            'destination_table': 'aqs.local_group_default_T_unique_devices.data'
        },
        'hql_path': f'{hql_directory}/cassandra/monthly/load_cassandra_unique_devices_monthly.hql'
    }
})

def create_sensor(job_name, properties):
    if ('wait_for_partitions' in properties):
        return NamedHivePartitionSensor(
            task_id=f'wait_for_{job_name}_partitions',
            partition_names=properties['wait_for_partitions'],
            poke_interval=timedelta(hours=2).total_seconds()
        )
    else:
        return RangeHivePartitionSensor(
            task_id=f'wait_for_{job_name}_partitions',
            table_name=properties['hql_parameters']['source_table'],
            from_timestamp='{{execution_date|start_of_current_month}}',
            to_timestamp='{{execution_date|start_of_next_month}}',
            granularity=properties['source_granularity'],
            poke_interval=timedelta(hours=2).total_seconds()
        )

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 8, 1)),
    schedule_interval='@monthly',
    tags=['cassandra', 'hql'],
    user_defined_filters=filters,
    default_args=var_props.get_merged('default_args',
        {
            **default_args,
            'sla': timedelta(days=5),
        }
    )
) as dag:

    for job_name, properties in jobs_properties.items():
        sensor = create_sensor(job_name, properties)

        etl = SparkSqlOperator(
            task_id=f'load_{job_name}_to_cassandra',
            # Override application jar (currently set to use SparkSQLNoCLIDriver).
            # The one set in default_args contains a cassandra-connector that prevents the job
            # from using the patched one provided in jars (classpath order issue).
            # This parameter should be removed when we move to spark3-on-skein in client mode
            # instead of SparkSQLNoCLIDriver in cluster mode.
            application=var_props.get('refinery_job_jar', artifact('refinery-job-0.2.7-shaded.jar')),
            sql=properties['hql_path'],
            query_parameters={
                **properties['hql_parameters'],
                'year': '{{ execution_date.year }}',
                'month': '{{ execution_date.month }}',
                # Setting coalesce_partitions to 6 is important
                # as it defines how many parallel loaders we use for cassandra.
                # We currently use 6 as there are 6 cassandra hosts
                'coalesce_partitions': 6,
            },
            executor_memory='8G',
            executor_cores=2,
            driver_memory='4G',
            driver_cores=1,
            jars=var_props.get('jars', artifact('spark-cassandra-connector-assembly-3.2.0-WMF-1.jar') + ',' +
                                       artifact('refinery-job-0.2.7-shaded.jar')),
            conf=execution_conf
        )

        sensor >> etl
