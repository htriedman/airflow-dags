
from airflow.sensors.base import BaseSensorOperator
from datetime import datetime
from typing import Any, cast, Dict, List
import requests


def normalize_interval(interval: str) -> str:
    """
    Normalize a given Druid interval to ensure interval equality works.
    The chosen normalized format is YYYYMMDDHHMMSS. For example:
    "2023-01-01T10:30:15/2023-03-14T20:45:00" -> "20230101103015/20230314204500"
    """
    # Remove potential trailing Z, because it's not parsed as ISO format.
    interval_datetimes = [datetime.fromisoformat(ts.rstrip("Z")) for ts in interval.split("/")]
    normalized_timestamps = [datetime.strftime(dt, "%Y%m%d%H%M%S") for dt in interval_datetimes]
    return "/".join(normalized_timestamps)


class DruidAPI:
    """
    Helper class to query the Druid metadata API.
    """

    def __init__(self, druid_api_host: str, druid_api_port: str):
        """
        Initializes a DruidAPI object.
        """
        self.druid_api_host = druid_api_host
        self.druid_api_port = druid_api_port

    def get_datasource_segments(
        self,
        datasource: str,
        include_overshadowed: bool = False
    ) -> Dict[Any, Any]:
        """
        Returns a dictionary with the Druid segments information
        belonging to the given datasource. If include_overshadowed is True,
        includes information for overshadowed segments (if any).
        See: https://druid.apache.org/docs/latest/operations/api-reference.html#get-4
        Overshadowed segments are those segments whose interval is
        partially or completely covered by other newer segments.
        See: https://druid.apache.org/docs/latest/design/coordinator.html#cleaning-up-segments
        """
        uri = (
            self._get_base_uri() +
            "druid/coordinator/v1/metadata/segments?" +
            ("includeOvershadowedStatus&" if include_overshadowed else "") +
            f"datasources={datasource}"
        )
        response = requests.get(uri, timeout=10)
        response.raise_for_status()
        return cast(Dict[Any, Any], response.json())

    def _get_base_uri(self) -> str:
        """
        Returns http base URI of the Druid API.
        """
        return f"http://{self.druid_api_host}:{self.druid_api_port}/"


class DruidSegmentSensor(BaseSensorOperator):
    """
    Checks for the existence of a given list of segments in a Druid datasource.
    """

    ui_color = "#32d0dd"
    template_fields = ["intervals"]

    def __init__(
        self,
        datasource: str,
        intervals: List[str],
        druid_api_host: str,
        druid_api_port: str,
        **kwargs: Any,
    ):
        """
        Initializes a DruidSegmentSensor object.
        You can pass the following parameters or any other parameters
        from an airflow.sensors.base.BaseSensorOperator.

        :param datasource:
            Name of the datasource to check for segment existence.

        :param intervals:
            The time intervals of the segments to check for existence.
            List of strings with the format "<segment_start>/<segment_end>".
            Both timestamps must be ISO compatible.

        :param druid_api_host:
            Hostname of the Druid API service to query.
            Example: an-druid1001.eqiad.wmnet.

        :param druid_api_port:
            Port of the Druid API service to query. Example: 8081.
        """
        self.datasource = datasource
        self.intervals = intervals
        self.druid_api_host = druid_api_host
        self.druid_api_port = druid_api_port
        super().__init__(**kwargs)

    def poke(self, context: dict) -> bool:
        """
        Queries the Druid API and checks that it contains
        the specified datasource segments.
        """
        self.log.info(
            "Checking for existence of Druid segments "
            f"{self.datasource} {self.intervals}."
        )
        druid_api = DruidAPI(self.druid_api_host, self.druid_api_port)
        segments = druid_api.get_datasource_segments(self.datasource)
        # Look for target intervals among the segment intervals.
        target_intervals = [normalize_interval(i) for i in self.intervals]
        segment_intervals = [normalize_interval(s["interval"]) for s in segments]
        return all([(ti in segment_intervals) for ti in target_intervals])
