from datetime import datetime, timedelta
from airflow import DAG
from mergedeep import merge
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.partitions_builder import daily_partitions
from analytics.config.dag_config import default_args, artifact

dag_id = 'country_project_page_daily_dag'
var_props = VariableProperties(f'{dag_id}_config')
source_table = 'wmf.pageview_actor'
source_granularity = '@hourly'
year = '{{execution_date.year}}'
month = '{{execution_date.month}}'
day = '{{execution_date.day}}'
venv = 'differential-privacy-0.1.0.conda.tgz'
# venv = 'hdfs:///user/htriedman/differential-privacy-0.1.0.conda.tgz'

modified_args = merge(
    default_args,
    {
        # Custom job settings, ~20% cluster resources
        # See https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs for normal cluster reference
        # (24GB exec mem + 4GB exec mem overhead) * 24 instances + (10GB driver mem * 2 instances) = 692GB
        'driver_cores': 2,
        'driver_memory': '10G',
        'executor_cores': 12,
        'executor_memory': '24G',
        'conf': {
            'spark.dynamicAllocation.maxExecutors': '24',
            'spark.sql.shuffle.partitions': 288, # 24 instances * 12 cores / instance = 288 2GB partitions
            'spark.sql.sources.partitionOverwriteMode': 'dynamic',
            'spark.executor.memoryOverhead': 4096,
            'spark.sql.warehouse.dir': '/tmp',
        },
    }
)


with DAG(
    dag_id=dag_id,
    start_date=var_props.get_datetime('start_date', datetime(2023, 2, 14)),
    schedule_interval='@daily',
    default_args=modified_args,
    tags=['spark', 'data_release', 'differential_privacy']
) as dag:

    s = NamedHivePartitionSensor(
        task_id='wait_for_pageview_actor',
        partition_names=daily_partitions(table=source_table, granularity=source_granularity),
        poke_interval=timedelta(minutes=1).total_seconds(),
        timeout=timedelta(hours=10).total_seconds()
    )

#    conda_env = var_props.get('conda_env', artifact(venv))
    conda_env = var_props.get('conda_env', venv)
    args = [year, month, day, 'default']
    do_dp_pageview_actor = SparkSubmitOperator.for_virtualenv(
        task_id="do_dp_pageview_actor",
        virtualenv_archive=conda_env,
        use_virtualenv_spark=True,
        entry_point='lib/python3.7/site-packages/differential_privacy/country_project_page_gaussian.py',
        launcher='skein',
        application_args=args,
        env_vars={"SPARK_CONF_DIR": "/etc/spark3/conf"},
        deploy_mode='cluster'
    )

    s >> do_dp_pageview_actor
