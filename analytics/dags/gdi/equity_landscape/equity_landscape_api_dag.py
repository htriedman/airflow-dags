"""
# GDI Equity Landscape DAG - API

* This DAG is used to generate the equity landscape input metrics from APIs.
* The data is stored on Hive.

"""

from datetime import datetime
from typing import Dict

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.utils.task_group import TaskGroup

from analytics.config import dag_config

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator


dag_id = "gdi_equity_landscape_api"
var_props = VariableProperties(f"{dag_id}_config")
schema = var_props.get("schema", "gdi")


archives = (
    var_props.get(
        "conda_env",
        "https://gitlab.wikimedia.org/repos/gdi/equity-landscape/gdi-source/-/package_files/1074/download",
    )
    + ".tgz#venv"
)


def _create_spark_operator(task_id: str, application_path: str, args: Dict):
    """
    Create a SparkSubmitOperator for the given application.

    param task_id: The task_id for the operator
    param application_path: The path to the application
    param args: The arguments to pass to the application

    """
    return SparkSubmitOperator.for_virtualenv(
        task_id=task_id,
        virtualenv_archive=archives,
        use_virtualenv_spark=True,
        entry_point=application_path,
        skein_app_log_collection_enabled=False,
        launcher="skein",
        application=None,
        application_args=args,
    )


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 10, 1)),
    schedule_interval=None,
    default_args=var_props.get_merged("default_args", dag_config.default_args),
) as dag:

    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    jobs_config = var_props.get(
        "jobs_config",
        {
            "load_lua": {
                "application": "lib/python3.7/site-packages/gdi_source/equity_landscape/load_lua_data.py",
                "args": {
                    "--schema": schema,
                    "--current_year": "2021",
                    "--end_year": "2021",
                },
            },
            "load_world_bank_data": {
                "application": "lib/python3.7/site-packages/gdi_source/equity_landscape/load_wbgapi.py",
                "args": {"--schema": schema},
                "loop_args": [
                    {"--db": 2, "--series": "IT.NET.USER.ZS"},
                    {"--db": 2, "--series": "IT.CEL.SETS.P2"},
                    {"--db": 2, "--series": "SP.POP.TOTL"},
                    {"--db": 2, "--series": "NY.GDP.PCAP.PP.CD"},
                    {"--db": 2, "--series": "NY.GDP.PCAP.PP.KD"},
                    {"--db": 2, "--series": "SP.POP.GROW"},
                    {"--db": 2, "--series": "SP.POP.TOTL.FE.IN"},
                    {"--db": 2, "--series": "FP.CPI.TOTL.ZG"},
                ],
            },
        },
    )

    last_task = start
    for job_name, job_config in jobs_config.items():
        loop_args = job_config.get("loop_args", [])
        if loop_args:
            with TaskGroup(group_id=job_name) as task_group:
                for loop_arg in loop_args:
                    operator = _create_spark_operator(
                        task_id=f"{job_name}_{loop_arg['--series']}",
                        application_path=job_config["application"],
                        args={**job_config["args"], **loop_arg},
                    )
                last_task = last_task >> task_group
        else:
            operator = _create_spark_operator(
                task_id=job_name,
                application_path=job_config["application"],
                args=job_config["args"],
            )
            last_task = last_task >> operator

    last_task >> end
