from pathlib import Path
import pytest
import requests
import sys
import time
from workflow_utils.artifact import Artifact


repo_base = Path(__file__).parents[2]
config_paths = [
    str(Path(repo_base, 'wmf_airflow_common/config/artifact_config.yaml')),
    str(Path(repo_base, 'search/config/artifacts.yaml'))
]
artifacts = [
    pytest.param(artifact, id=name)
    for name, artifact in Artifact.load_artifacts_from_config(config_paths).items()
]

@pytest.mark.parametrize('artifact', artifacts)
def test_http_artifact_exists(artifact: Artifact):
    url = artifact.source.url(artifact)
    if not url.startswith('http'):
        pytest.skip(f'not an http url: {url}')
    requests.head(url).raise_for_status()


