import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'webrequest', 'officewiki', 'extract_officewiki_dag.py']


def test_extract_office_wiki_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="officewiki_pageviews_daily")
    assert dag is not None
    assert len(dag.tasks) == 2
