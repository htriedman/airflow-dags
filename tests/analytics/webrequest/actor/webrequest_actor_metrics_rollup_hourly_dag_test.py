import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'webrequest', 'actor', 'webrequest_actor_metrics_rollup_hourly_dag.py']


def test_webrequest_actor_metrics_rollup_hourly_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="webrequest_actor_metrics_rollup_hourly")
    assert dag is not None
    assert len(dag.tasks) == 2
    # Testing interval_start is 24 hours before interval_end
    query_parameters = dag.tasks[1]._query_parameters
    assert query_parameters['interval_start_year'] == '{{ (execution_date.add(hours=-23)).year }}'
    assert query_parameters['interval_start_month'] == '{{ (execution_date.add(hours=-23)).month }}'
    assert query_parameters['interval_start_day'] == '{{ (execution_date.add(hours=-23)).day }}'
    assert query_parameters['interval_start_hour'] == '{{ (execution_date.add(hours=-23)).hour }}'
