"""
### Transfering data to the AQS hourly dataset
* Extracting data from the webrequest dataset (on an hourly partition)
* Filtering on the AQS requests
    - webrequest_source = 'text'
    - uri_path like '/api/rest_v1/metrics/%'
* Selecting statistically interesting dimensions.
* Loading them to a partition in the AQS hourly dataset.

The resulting dataset is:
* partitioned by year, month, day, hour
* located in /wmf/data/wmf/aqs/hourly

#### Note

* This job is waiting for the existence of a webrequest hive partition.
* The Hive script runs in Spark.
* We are using dynamicAllocation, but best results with this configuration:
    - 128 executors, with 2 cores each
    - spark.dynamicAllocation.initialExecutors = 128
    - spark.dynamicAllocation.maxExecutors = 256
"""

from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.partitions_builder import partition_names_by_granularity
from analytics.config.dag_config import default_args, hql_directory

dag_id = 'aqs_hourly'
source_table = 'wmf.webrequest'
webrequest_source = 'text'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 14, 8)),
    schedule_interval='@hourly',
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
) as dag:

    sensor = NamedHivePartitionSensor(
        task_id='wait_for_webrequest',
        partition_names=partition_names_by_granularity(
            table=source_table,
            granularity='@hourly',
            pre_partitions=[f'webrequest_source={webrequest_source}'],
        ),
    )

    etl = SparkSqlOperator(
        task_id='process_aqs_hourly',
        sql=var_props.get('hql_path', f'{hql_directory}/aqs/hourly.hql'),
        query_parameters={
            'source_table': source_table,
            'webrequest_source': webrequest_source,
            'destination_table': var_props.get('destination_table', 'wmf.aqs_hourly'),
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
            'day': '{{ execution_date.day }}',
            'hour': '{{ execution_date.hour }}',
            'coalesce_partitions': 8,
        },
    )

    sensor >> etl
