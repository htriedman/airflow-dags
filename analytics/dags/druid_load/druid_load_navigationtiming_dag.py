"""
Loads event.navigationtiming from Hive to Druid.

The loading is done with 2 DAGs: an hourly one and a daily one.
The hourly DAG loads data to Druid as soon as it's available in Hive.
The daily DAG waits for a full day of data to be available in Hive,
and loads it all to Druid as a single daily segment (more efficient).
If there were hourly segments in Druid already, it will override them.
Use the daily DAG for back-filling and re-runs.
"""

from airflow import DAG
from analytics.config.dag_config import artifact, dataset, default_args, druid_default_conf
from datetime import datetime, timedelta
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.templates.time_filters import filters


dag_id = "druid_load_navigationtiming"
var_props = VariableProperties(f"{dag_id}_config")

# Common configuration for both the hourly and the daily DAGs.
hive_to_druid_config = {
    "database": "event",
    "table": "navigationtiming",
    "druid_datasource": var_props.get("druid_datasource", "event_navigationtiming"),
    "timestamp_column": "dt",
    "query_granularity": "minute",
    "num_shards": 1,
    "hadoop_queue": var_props.get("hadoop_queue", "production"),
    "hive_to_druid_jar": var_props.get("hive_to_druid_jar",
        artifact("refinery-job-0.2.11-shaded.jar")),
    "temp_directory": var_props.get("temp_directory", None),  # Override just for testing.
    "dimensions": [
        "event.action",
        "event.isAnon",
        "event.isOversample",
        "event.mediaWikiVersion",
        "event.mobileMode",
        "event.namespaceId",
        "event.netinfoEffectiveConnectionType",
        "event.originCountry",
        "recvFrom",
        "revision",
        "useragent.browser_family",
        "useragent.browser_major",
        "useragent.device_family",
        "useragent.is_bot",
        "useragent.os_family",
        "useragent.os_major",
        "wiki",
    ],
    "time_measures": [
        "event.connectEnd",
        "event.connectStart",
        "event.dnsLookup",
        "event.domComplete",
        "event.domInteractive",
        "event.fetchStart",
        "event.firstPaint",
        "event.loadEventEnd",
        "event.loadEventStart",
        "event.redirecting",
        "event.requestStart",
        "event.responseEnd",
        "event.responseStart",
        "event.secureConnectionStart",
        "event.unload",
        "event.gaps",
        "event.mediaWikiLoadEnd",
        "event.RSI",
    ],
}


# Hourly DAG.
with DAG(
    dag_id=f"{dag_id}_hourly",
    doc_md="Loads navigationtiming from Hive to Druid hourly.",
    start_date=var_props.get_datetime("hourly_start_date", datetime(2023, 1, 20, 0)),
    # Do not schedule the DAG for the last hour of the day.
    # Otherwise, it would run in parallel with the daily DAG,
    # and race conditions could occur, corrupting Druid segments.
    schedule_interval="0 0-22 * * *",
    tags=["druid_load"],
    user_defined_filters=filters,
    default_args={
        **var_props.get_merged("default_args", default_args),
        **druid_default_conf,
        "sla": timedelta(hours=6),
    },
) as hourly_dag:

    sensor = dataset("hive_event_navigationtiming").get_sensor_for(hourly_dag)

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        since="{{execution_date | to_ds_hour}}",
        until="{{execution_date | add_hours(1) | to_ds_hour}}",
        segment_granularity="hour",
        reduce_memory=4096,
        **hive_to_druid_config,
    )

    sensor >> loader


# Daily DAG.
with DAG(
    dag_id=f"{dag_id}_daily",
    doc_md="Loads navigationtiming from Hive to Druid daily.",
    start_date=var_props.get_datetime("daily_start_date", datetime(2023, 1, 20)),
    schedule_interval="@daily",
    tags=["druid_load"],
    user_defined_filters=filters,
    default_args={
        **var_props.get_merged("default_args", default_args),
        **druid_default_conf,
        "sla": timedelta(hours=12),
    },
) as daily_dag:

    sensor = dataset("hive_event_navigationtiming").get_sensor_for(daily_dag)

    loader = HiveToDruidOperator(
        task_id="load_to_druid",
        since="{{execution_date | to_ds_hour}}",
        until="{{execution_date | add_days(1) | to_ds_hour}}",
        segment_granularity="day",
        reduce_memory=8192,
        **hive_to_druid_config,
    )

    sensor >> loader
