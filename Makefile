setup:
	echo ">> Installing python dependencies."
	pip install -q -r ../dev_requirements.txt
	echo ">> Plz Set your dags_folder configuration in your airflow.cfg (defautls in ~/airflow/)"

load_dags:
	echo ">> Trying to load the dags in Airflow locally."
	PYTHONPATH=. find . -type f -name '*_dag.py' -not -path "./venv/*" -exec python -W ignore::DeprecationWarning "{}" \;
	
unit_test:
	echo ">> Running unit tests with pytest."
	pytest

tests: load_dags unit_test
