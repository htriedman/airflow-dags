"""
### Creates the mediarequest hourly dataset
* Wait for a webrequest Hive partition
* Load data from the webrequest
* Filter on success requests
* Parse the url, and the referer
* Classify the referer (internal/external/unknown)
* count the number of requests (and traffic size) per media request

The resulting dataset is:
* partitioned by year, month, day, hour
* located in /wmf/data/wmf/mediarequest/year=2022/month=3/day=8/hour=17/
"""

from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from wmf_airflow_common.partitions_builder import partition_names_by_granularity
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.config.variable_properties import VariableProperties
from analytics.config.dag_config import default_args, hql_directory, artifact

dag_id = 'mediarequest_hourly'
source_table = 'wmf.webrequest'
var_props = VariableProperties(f'{dag_id}_config')
destination_table = var_props.get('destination_table', 'wmf.mediarequest')
default_args = var_props.get_merged('default_args', default_args)

with DAG(dag_id=dag_id,
         doc_md=__doc__,
         start_date=var_props.get_datetime('start_date', datetime(2022, 6, 14, 8)),
         schedule_interval='@hourly',
         tags=['spark', 'hql', 'hive'],
         default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    )) as dag:
    partitions = partition_names_by_granularity(
        table=source_table,
        granularity='@hourly',
        pre_partitions=['webrequest_source=upload']
    )
    sensor = NamedHivePartitionSensor(
        task_id='wait_for_webrequest_partition',
        partition_names=partitions)

    etl = SparkSqlOperator(
        task_id='task',
        sql=var_props.get('hql_path', f'{hql_directory}/mediarequest/hourly.hql'),
        query_parameters={
            'source_table': source_table,
            'destination_table': destination_table,
            'refinery_hive_shaded': artifact('refinery-hive-0.2.1-shaded.jar'),
            'coalesce_partitions': 64,  # resulting in 64 files
            'year': '{{execution_date.year}}',
            'month': '{{execution_date.month}}',
            'day': '{{execution_date.day}}',
            'hour': '{{execution_date.hour}}'},
        conf={'spark.dynamicAllocation.maxExecutors': 64}
        )

    sensor >> etl
