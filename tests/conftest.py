import os
import pytest
from unittest import mock

from airflow.models import DagBag


"""
Set of fixtures to reused across all DAG tests
"""


@pytest.fixture(name='airflow', autouse=True, scope="session")
def airflow_fixture(tmp_path_factory):
    """
    Sets up an airflow SQLlite database and airflow home
    fixture used by the entire test session.
    """
    from airflow.utils import db

    airflow_environ = {
        'AIRFLOW__CORE__LOAD_DEFAULT_CONNECTIONS': "False",
        'AIRFLOW__CORE__LOAD_EXAMPLES': 'False',
        'AIRFLOW__CORE__UNIT_TEST_MODE': 'True',
        'AIRFLOW_HOME': os.path.join(tmp_path_factory.mktemp('airflow_home')),
        'REBUILD_FIXTURES': os.environ.get('REBUILD_FIXTURES', 'no'),
    }

    with mock.patch.dict(os.environ, airflow_environ, clear=True):
        db.resetdb()
        yield


@pytest.fixture(name='dagbag')
def dagbag_fixture(dag_path):
    """
    Loads a single DAG from its path, passed as parameter

    :param dag_path:
        The path of the DAG as a list of strings (one string per folder/ file)
    """
    dag_bag = DagBag(None, include_examples=False, read_dags_from_db=False)
    dag_file = os.path.join(os.path.abspath(os.getcwd()), *dag_path)
    dag_bag.process_file(dag_file)
    return dag_bag