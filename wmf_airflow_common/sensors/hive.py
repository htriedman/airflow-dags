from typing import Any, Optional
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.partitions_builder import build_partition_names, PrePartitions


class RangeHivePartitionSensor(NamedHivePartitionSensor):
    """
    Waits for a set of partitions to show up in Hive.

    :param
    partition_specs:
      - table_name: the hive full table name
      - from_timestamp: from which point in time to create the partitions
      - to_timestamp: from which point in time to stop the partitions
      - granularity: (@hourly, @daily, ...) check for all the partitions matching
        this parameter.
      - pre_partitions: adds some extra partitions before the time ones.

    example of partition_specs:
        { 'table_name': 'wmf.mediawiki_page_move',
          'from': '{{execution_date}}',
          'to': '2022-02-19T15:47:03.051064+01:00',
          'granularity': '@daily',
          'pre_partitions': "datacenter='codfw' }

    All other parameters are forwarded to NamedHivePartitionSensor.
    """

    ui_color = '#8d99ae'

    poke_context_fields = (
        'partition_names',
        'metastore_conn_id'
    )

    template_fields = [
        'table_name',
        'from_timestamp',
        'to_timestamp',
        'granularity',
        'pre_partitions'
    ]

    def __init__(
        self,
        table_name: str,
        from_timestamp: str,
        to_timestamp: str,
        granularity: str,
        pre_partitions: Optional[PrePartitions] = None,
        *args: Any,
        **kwargs: Any,
    ):
        self.table_name = table_name
        self.from_timestamp = from_timestamp
        self.to_timestamp = to_timestamp
        self.granularity = granularity
        self.pre_partitions = pre_partitions

        super().__init__(partition_names=[], *args, **kwargs)

    def is_smart_sensor_compatible(self) -> bool:
        return False

    def pre_execute(self, context: Any = None) -> None:
        """Fill the partition_names list before execution."""
        self.partition_names = build_partition_names(
            self.table_name,
            self.from_timestamp,
            self.to_timestamp,
            self.granularity,
            pre_partitions=self.pre_partitions,
        )
