"""
### Creates the clickstream monthly dataset for various projects.
The job runs every month and its TSV results are synchronised to the public.

The Airflow dag launches a spark action that runs the ClickstreamBuilder scala job in
analytics-refinery-source/refinery-job and saves the results in a temporary folder.

Then the files from the temporary folder are archived to an archive location, with nice names.

* Temporary result location example:
  /wmf/tmp/analytics/clickstream_monthly__create_clickstream_file__20220301/

* Archive example:
  /wmf/data/archive/clickstream/2022-03/clickstream-enwiki-2022-03.tsv.gz

#### Sources:
- project_namespace_table: wmf_raw.mediawiki_project_namespace_map
- page_table: wmf_raw.mediawiki_page
- redirect_table: wmf_raw.mediawiki_redirect
- pagelinks_table: wmf_raw.mediawiki_pagelinks
- pageview_actor_table: wmf.pageview_actor

#### Variables stored in clickstream_monthly_config with their default values
* clickstream_archive_base_path: '/wmf/data/archive/clickstream'
* default_args:
    poke_interval: 2 hours
* wiki_list: ["enwiki", "ruwiki", "dewiki", "eswiki", "jawiki", "frwiki", "zhwiki", "itwiki",
    "plwiki", "ptwiki", "fawiki"]
* start_date: datetime(2022, 3, 31)),
* refinery_job_jar: artifact('refinery-job-0.1.27-shaded')
* clickstream_minimum_links: 10
"""

from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from analytics.config.dag_config import default_args, artifact
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.partitions_builder import add_post_partitions, PrePartitions
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from wmf_airflow_common.templates.time_filters import filters


dag_id = 'clickstream_monthly'
snapshot = '{{execution_date | to_ds_month}}'

var_props = VariableProperties(f'{dag_id}_config')
tmp_base_path = var_props.get('output_base_path', f'/wmf/tmp/analytics/{dag_id}__{snapshot}')
clickstream_archive_base_path = var_props.get(
    'clickstream_archive_base_path',
    '/wmf/data/archive/clickstream'
)

default_args = var_props.get_merged(
    'default_args',
    {
        **default_args,
        'poke_interval': timedelta(hours=2).total_seconds(),
        'sla': timedelta(days=10),
    }
)

# List of mediawiki to create an archive for.
wiki_list_default = \
    'enwiki ruwiki dewiki eswiki jawiki frwiki zhwiki itwiki plwiki ptwiki fawiki'.split()
wiki_list = var_props.get_list('wiki_list', wiki_list_default)

with DAG(dag_id=dag_id,
         doc_md=__doc__,
         start_date=var_props.get_datetime('start_date', datetime(2022, 5, 1)),
         schedule_interval='@monthly',
         tags=['spark', 'archive'],
         user_defined_filters=filters,
         default_args=default_args
) as dag:

    sensors = []

    sensors.append(RangeHivePartitionSensor(
        task_id='wait_for_pageview_actor_partitions',
        table_name='wmf.pageview_actor',
        from_timestamp='{{execution_date | start_of_current_month}}',
        to_timestamp='{{execution_date | start_of_next_month}}',
        granularity='@hourly'
    ))

    sensors.append(NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_project_namespace_map_snapshot',
        partition_names=[f'wmf_raw.mediawiki_project_namespace_map/snapshot={snapshot}']
    ))

    wiki_db_partitions = PrePartitions([[f'wiki_db={db}' for db in wiki_list]])

    sensors.append(NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_page_snapshots',
        partition_names=add_post_partitions(
            [f'wmf_raw.mediawiki_page/snapshot={snapshot}'],
            wiki_db_partitions
        )
    ))

    sensors.append(NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_pagelinks_snapshots',
        partition_names=add_post_partitions(
            [f'wmf_raw.mediawiki_pagelinks/snapshot={snapshot}'],
            wiki_db_partitions
        )
    ))

    sensors.append(NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_redirect_snapshots',
        partition_names=add_post_partitions(
            [f'wmf_raw.mediawiki_redirect/snapshot={snapshot}'],
            wiki_db_partitions
        )
    ))

    etl = SparkSubmitOperator(
        task_id='clickstream_builder',

        # Spark configuration optimized for this job.
        # This job needs more memory to handle the page links dataset. Using the default of 4GB of
        # memory triggers out of memory errors and Spark tasks are recomputed. It eventually
        # finishes but it's slow.
        executor_memory='8G',
        conf={'spark.dynamicAllocation.maxExecutors': 128},
        # Too many logs are generated, it makes log collection crashed.
        skein_app_log_collection_enabled=False,

        # Spark application
        application=var_props.get('refinery_job_jar', artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.ClickstreamBuilder',
        application_args={
            '--output-base-path': tmp_base_path,
            '--wikis': ','.join(wiki_list),
            '--snapshot': snapshot,
            '--year': '{{execution_date.year}}',
            '--month': '{{execution_date.month}}',
            # The minimum count for a link to appear in the dataset. Default to 10.
            '--minimum-count': var_props.get('clickstream_minimum_links', 10),
            '--output-files-parts': 1  # Make sure only 1 file per wiki is generated.
        },
    )

    archivers = [
        HDFSArchiveOperator(
            source_directory=f"{tmp_base_path}/wiki_db={wiki}",
            archive_file=f"{clickstream_archive_base_path}"
                         f"/{snapshot}"
                         f"/clickstream-{wiki}-{snapshot}.tsv.gz",
            task_id=f'archive_{wiki}',
    ) for wiki in wiki_list
    ]

    sensors >> etl >> archivers
