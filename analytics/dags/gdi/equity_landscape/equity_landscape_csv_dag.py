"""
# GDI Equity Landscape DAG - CSV

* This DAG is used to generate the equity landscape input metrics using the csv files stored on HDFS.
* It calls load_csv.py to read the file and store it on Hive.

## Inputs
* wiki_map.csv - Maps Wiki db to the corresponding groups
* country_meta_data.csv - Country metadata

## Outputs (Hive)
* gdi.wiki_db_map_input_metrics
* gdi.country_meta_data
"""

from dataclasses import dataclass
from datetime import datetime
from pathlib import Path
from typing import Dict

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.utils.task_group import TaskGroup

from analytics.config.dag_config import default_args

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator


dag_id = "gdi_equity_landscape_csv"
var_props = VariableProperties(f"{dag_id}_config")

archives = (
    var_props.get(
        "conda_env",
        "https://gitlab.wikimedia.org/repos/gdi/equity-landscape/gdi-source/-/package_files/1074/download",
    )
    + ".tgz#venv"
)

schema = var_props.get("schema", "gdi")
base_csv_location = var_props.get(
    "base_csv_location", "hdfs://analytics-hadoop/wmf/data/raw/gdi/equity_landscape/csv"
)
partition_year = var_props.get("partition_year", 2021)


@dataclass
class Csv:
    args: Dict
    name: str


def _csv_files(table_name: str, file_location: str, args: Dict = None) -> Csv:

    full_location = f"{base_csv_location}/{file_location}"

    standard_args = {
        "--table_name": f"{schema}.{table_name}",
        "--file_location": full_location,
    }

    YEAR = "year='-'"
    PARITION_COLUMNS = "--partition_columns"

    if args is not None and PARITION_COLUMNS in args:
        if YEAR in args[PARITION_COLUMNS]:
            args[PARITION_COLUMNS] = f"year='{partition_year}'"

    # merge the standard args with the args passed in
    args = {**standard_args, **args} if args else standard_args

    return Csv(
        args=args,
        name=Path(file_location).stem,
    )


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 10, 1)),
    schedule_interval=None,
    default_args=var_props.get_merged("default_args", default_args),
) as dag:

    csv_app = var_props.get(
        "csv_app",
        "lib/python3.7/site-packages/gdi_source/equity_landscape/load_csv.py",
    )

    jobs_config = var_props.get(
        "jobs_config",
        [
            {
                "table_name": "wiki_db_map_input_metrics",
                "file_location": "wiki_db_map.csv",
                "args": {"--headers": 0},
            },
            {
                "table_name": "country_meta_data",
                "file_location": "country_meta_data.csv",
            },
        ],
    )

    lst_csvs = [_csv_files(**csv_obj) for csv_obj in jobs_config]

    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    with TaskGroup(group_id="load_csv") as load_csv:
        for csv_prop in lst_csvs:
            SparkSubmitOperator.for_virtualenv(
                task_id=f"load_{csv_prop.name.lower()}",
                virtualenv_archive=archives,
                use_virtualenv_spark=True,
                skein_app_log_collection_enabled=False,
                entry_point=csv_app,
                launcher="skein",
                application_args=csv_prop.args,
            )

    (start >> load_csv >> end)
