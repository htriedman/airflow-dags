'''
Format the commons weekly dump into a Hive table.

NOTE: Timestamps of source dumps and output data follow different conventions.
The timestamp of source dumps points at the end of the data contained in it.
For instance, if the data spans since the start of wiki time to
2022-03-20T23:59:59.999, the timestamp of the dump will be 2022-03-21.
On the other hand, for output data, the timestamp indicates the beginning of
the week that is being processed. For instance if the week starts at 2022-03-14
and ends at 2022-03-20T23:59:59.999, the timestamp of the output data will be
2022-03-14. Thus, to process the output data for week 2022-03-14, the DAG
needs to wait for the source dump with timestamp 2022-03-21 (processed week
+ 7 days). This is done so, to be consistent with all other Airflow jobs,
whose timestamp always points to the start of the interval to process.

TODO: Move this explanation to the wiki and point to it here.
'''

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import hadoop_name_node, default_args, artifact

dag_id = 'commons_structured_data_dump_to_hive_weekly'
dump_date = '{{ execution_date | add_days(7) | to_ds_nodash }}'
dump_location = f'{hadoop_name_node}/wmf/data/raw/commons/dumps/mediainfo-json/{dump_date}'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 6)),
    schedule_interval='0 0 * * 1', # Weekly, starting on Monday at 00:00.
    tags=['spark', 'dump', 'hive', 'structured_data'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=7),
        }
    ),
    user_defined_filters=filters,
) as dag:

    sensor = URLSensor(
        task_id='wait_for_input_dump',
        url=f'{dump_location}/_IMPORTED',
        poke_interval=timedelta(minutes=20).total_seconds(),
    )

    etl = SparkSubmitOperator(
        task_id='load_to_hive',
        application=var_props.get('refinery_job_jar', artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.structureddata.jsonparse.JsonDumpConverter',
        application_args={
            '--json-dump-path': f'{dump_location}/commons-{dump_date}-mediainfo.json.bz2',
            '--snapshot': '{{ ds }}',
            '--output-table': var_props.get('output_table', 'structured_data.commons_entity'),
            '--project-type': 'commons',
            '--num-partitions': 512
        },
        executor_memory='8G',
        conf={'spark.dynamicAllocation.maxExecutors': 64},
    )

    sensor >> etl
