'''
### Geo-editors monthly-data
Aggregate editors_daily data into the traditional kind of data kept by geoeditors.
Note: This dataset does NOT contain bots actions and only considers edit actions.
'''

from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.url import URLTouchOperator
from analytics.config.dag_config import default_args, hql_directory, hadoop_name_node

dag_id = 'geoeditors_monthly'
var_props = VariableProperties(f'{dag_id}_config')
current_month = '{{ execution_date.strftime("%Y-%m") }}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 5, 1)),
    schedule_interval='@monthly',
    tags=['spark', 'hql', 'hive', 'geoeditors'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=10),
        }
    ),
) as dag:

    hive_sensor = NamedHivePartitionSensor(
        task_id='wait_for_editors_daily',
        partition_names=[f'wmf.editors_daily/month={current_month}'],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    etl = SparkSqlOperator(
        task_id='process_geoeditors_monthly',
        sql=var_props.get('hql_path',
            f'{hql_directory}/geoeditors/geoeditors_monthly.hql'),
        query_parameters={
            'source_table': 'wmf.editors_daily',
            'destination_table': var_props.get('destination_table', 'wmf.geoeditors_monthly'),
            'month': current_month,
            'coalesce_partitions': 1,
        },
    )

    # this is needed to trigger the druid loading job still in Oozie
    # TODO: Remove this step when druid jobs have been migrated to airflow
    success = URLTouchOperator(
        task_id='write_success_file',
        url=(
            hadoop_name_node +
            var_props.get('destination_path', f'/wmf/data/wmf/mediawiki_private/geoeditors_monthly') +
            f'/month={current_month}/_SUCCESS'
        ),
    )

    hive_sensor >> etl >> success
