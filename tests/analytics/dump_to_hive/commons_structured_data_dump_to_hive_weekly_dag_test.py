import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags','dump_to_hive',
            'commons_structured_data_dump_to_hive_weekly_dag.py']


def test_commons_structured_data_dump_to_hive_weekly_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="commons_structured_data_dump_to_hive_weekly")
    assert dag is not None
    assert len(dag.tasks) == 2
