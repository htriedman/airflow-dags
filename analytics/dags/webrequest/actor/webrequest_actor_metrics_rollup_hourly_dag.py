"""
### Roll-up 24h of webrequest_actor_metrics_hourly every hour

The rolled-up metrics are used to compute webrequest_actor_label_hourly,
splitting actors between automated and users.

NOTE: This dag starts one day AFTER the one computing
webrequest_actor_metrics_hourly as the rolled-up metrics need 24h
of data to be computed.
"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, hql_directory

dag_id = 'webrequest_actor_metrics_rollup_hourly'
var_props = VariableProperties(f'{dag_id}_config')
source_table = var_props.get('source_table', 'wmf.webrequest_actor_metrics_hourly')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    # 24h after webrequest_actor_metrics_hourly - See note at the top of the file
    start_date=var_props.get_datetime('start_date', datetime(2023, 1, 2)),
    schedule_interval='@hourly',
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
) as dag:

    # We don't use wmf_airflow_commons.dataset here as the sensor is not
    # defined from the DAG execution frequency
    sensor = RangeHivePartitionSensor(
        task_id='wait_for_webrequest_actor_metrics_previous_24_hours',
        table_name=source_table,
        # execution_date counts for one hour but the operator excludes
        # the to_timestamp hour
        from_timestamp="{{ execution_date | add_hours(-23) }}",
        to_timestamp="{{ execution_date | add_hours(1) }}",
        granularity='@hourly',
    )

    etl = SparkSqlOperator(
        task_id='compute_webrequest_actor_metrics_rollup',
        sql=var_props.get('hql_path', f'{hql_directory}/webrequest/actor/compute_webrequest_actor_metrics_rollup_hourly.hql'),
        query_parameters={
            'source_table': source_table,
            'destination_table': var_props.get('destination_table', 'wmf.webrequest_actor_metrics_rollup_hourly'),
            'version': var_props.get('version', '0.1'), # Version of the metrics
            'coalesce_partitions': var_props.get('coalesce_partitions', 16), # Number of output files
            # interval_end_* parameters are the one of the current hour to be computed
            'interval_end_year': '{{ execution_date.year }}',
            'interval_end_month': '{{ execution_date.month }}',
            'interval_end_day': '{{ execution_date.day }}',
            'interval_end_hour': '{{ execution_date.hour }}',
            # interval_start_* parameters are the one of the hour that is 23h before
            # the one to be computed, to make a 24h interval including current hour
            'interval_start_year': '{{ (execution_date.add(hours=-23)).year }}',
            'interval_start_month': '{{ (execution_date.add(hours=-23)).month }}',
            'interval_start_day': '{{ (execution_date.add(hours=-23)).day }}',
            'interval_start_hour': '{{ (execution_date.add(hours=-23)).hour }}',
        },
        executor_memory='8G',
        executor_cores=2,
        conf={
            'spark.dynamicAllocation.maxExecutors': 128,
        }
    )

    sensor >> etl
