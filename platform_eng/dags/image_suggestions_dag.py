#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""Generate an Airflow DAG that orchestrates the image suggestions data pipeline."""

from datetime import datetime, timedelta
from mergedeep import merge

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from platform_eng.config.dag_config import artifact, cassandra_default_conf, default_args, hadoop_name_node

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.sensors.url import URLSensor
from wmf_airflow_common.operators.spark import SparkSubmitOperator, SparkSqlOperator

dag_id = 'image-suggestions'
alerts_email = 'sd-alerts@lists.wikimedia.org'
var_props = VariableProperties(f'{dag_id}_config')
hive_db = var_props.get('hive_db', 'analytics_platform_eng')
# coalesce controls the amount of files generated per Hive partition by the Spark jobs.
coalesce = var_props.get('coalesce', 2)
conda_env = var_props.get('conda_env', artifact('image-suggestions-0.6.0-v0.6.0.conda.tgz'))

modified_args = merge(
    {},
    default_args,
    {
        'email': alerts_email,
        # Regular job settings, ~15% cluster resources
        # See https://wikitech.wikimedia.org/wiki/Analytics/Systems/Cluster/Spark#Regular_jobs
        # +2 GB driver memory, needed for the Commons and Cassandra tasks
        # +5 GB driver memory, since Skein Master was OOMing with just 7G
        # +1 GB executor memory, needed for the Cassandra task
        # +7 GB executor memory, needed to reliably run the job after coalesce changes from T323614
        # The job should eat up at most 64 * 16 + 12 = 1,036 GB (plus overheads)
        'driver_cores': 2,
        'driver_memory': '12G',
        'executor_cores': 4,
        'executor_memory': var_props.get('executor_memory', '16G'),
        'conf': {
            'spark.dynamicAllocation.enabled': 'true',
            'spark.dynamicAllocation.maxExecutors': '64',
            'spark.shuffle.service.enabled': 'true',
            'spark.sql.shuffle.partitions': 256,
            'spark.sql.sources.partitionOverwriteMode': 'dynamic'
        },
        # Avoid skein.exceptions.DriverError: Received message larger than max (21089612 vs. 4194304)
        'skein_app_log_collection_enabled': False
    }
)

cassandra_conf = merge(
    {
        **cassandra_default_conf,
        'spark.yarn.maxAppAttempts': 1,
    },
    modified_args['conf']
)


# Generates a quoted, simple INSERT INTO statement to be used by the spark-sql cassandra loading tasks.
def generate_spark_to_cassandra_insert_query(cassandra_table, coalesce_num, hive_columns, hive_db, hive_table, week):
    return "\"" + (
        f"INSERT INTO aqs.image_suggestions.{cassandra_table} "
        f"SELECT /*+ COALESCE({coalesce_num}) */ {hive_columns} "
        f"FROM {hive_db}.{hive_table} "
        f"WHERE snapshot='{week}'"
    ) + "\""


with DAG(dag_id=dag_id,
         doc_md=__doc__,
         start_date=var_props.get_datetime('start_date', datetime(2022, 5, 16)),
         schedule_interval=var_props.get('schedule_interval', '0 0 * * 1'),  # Every monday @ 12AM
         catchup=var_props.get('catchup', False),
         tags=["structured-data-team", "generated-data-platform"],
         default_args=modified_args
         ) as dag:

    # Pre-condition: wait for the latest DB table snapshots (AKA partitions)
    # NOTE We pass the snapshot values through Airflow's built-in template variables and macros:
    #   `execution_date` gives a pendulum.DateTime object corresponding to the logical execution_date of the DAG, which
    #   is the BEGINNING of the scheduled time period this run is for
    #   So for example
    #   - a weekly snapshot 2022-05-16 (a Monday) covers data up until 2022-05-22T23:59:59
    #     (i.e. the end of the following Sunday)
    #   - a monthly snapshot 2022-05 covers data up until 2022-05-31T23:59:59
    # See https://airflow.apache.org/docs/apache-airflow/2.2.0/templates-ref.html (2.1.4 returns HTTP 404)
    # TODO If we upgrade to Airflow version 2.2.0, use `data_interval_start` instead of `execution_date` (deprecated)
    weekly = "{{ execution_date.format('YYYY-MM-DD') }}"
    previous_weekly = var_props.get('previous_weekly', "{{ execution_date.subtract(weeks=1).format('YYYY-MM-DD') }}")
    # Set the most recent monthly snapshot corresponding to the weekly snapshot
    # Because of the weird way snapshot dates work, the monthly snapshot corresponding to a weekly snapshot is the
    # month part of the last day of the week that corresponds to the weekly snapshot, minus 1
    # Example 1:
    #   weekly snapshot == 2022-05-16
    #   Covers data up until 2022-05-22T23:59:59
    #   At that time the most recent monthly snapshot is the one generated at the end of April, so the monthly
    #   snapshot is 2022-04
    # Example 2:
    #   weekly snapshot == 2022-05-30
    #   Covers data up until 2022-06-05T23:59:59
    #   At that time the most recent monthly snapshot is the one generated at the end of May, so the monthly
    #   snapshot is 2022-05
    monthly = "{{ execution_date.end_of('week').subtract(months=1).start_of('month').format('YYYY-MM') }}"

    # DAG task 1: wait for weekly tables
    weekly_snapshot_sensor = NamedHivePartitionSensor(
        partition_names=[
            f"wmf.wikidata_entity/snapshot={weekly}",
            f"wmf.wikidata_item_page_link/snapshot={weekly}",
            f"structured_data.commons_entity/snapshot={weekly}",
        ],
        task_id='wait_for_weekly_tables',
        poke_interval=timedelta(hours=1).total_seconds()
    )

    monthly_mediawiki_tables = [
        "category",
        "categorylinks",
        "imagelinks",
        "page",
        "page_props",
        "pagelinks",
        "revision"
    ]

    # DAG task 2: wait for monthly tables.
    # We create as many parallel Airflow tasks as the monthly tables.
    # NOTE The _PARTITIONED file tells us that:
    #      1) partitions have been added to HDFS by sqoop
    #      2) partitions have been added to Hive Metastore by analytics/dags/mediawiki/mediawiki_history_load_dag.py
    monthly_sensors = []
    for table in monthly_mediawiki_tables:
        url_sensor = URLSensor(
            url=f'{hadoop_name_node}/wmf/data/raw/mediawiki/tables/{table}/snapshot={monthly}/_PARTITIONED',
            task_id=f'wait_for_mw_{table}',
            poke_interval=timedelta(hours=1).total_seconds(),
        )
        monthly_sensors.append(url_sensor)

    # DAG task 3: Wikidata weighted tags for the Commons search index.
    # https://phabricator.wikimedia.org/T302095
    # Usage: commonswiki_file.py HIVE_DB SNAPSHOT PREVIOUS_SNAPSHOT COALESCE
    args = [hive_db, weekly, previous_weekly, coalesce]
    gather_commons_dataset = SparkSubmitOperator.for_virtualenv(
        task_id="commons_index",
        virtualenv_archive=conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/commonswiki_file.py',
        launcher='skein',
        application_args=args
    )

    # DAG task 4: all-Wikis image suggestions for Cassandra.
    # https://phabricator.wikimedia.org/T299789
    # Usage: cassandra.py HIVE_DB SNAPSHOT COALESCE
    args = [hive_db, weekly, coalesce]
    gather_cassandra_dataset = SparkSubmitOperator.for_virtualenv(
        task_id="all_suggestions",
        virtualenv_archive=conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/cassandra.py',
        launcher='skein',
        application_args=args
    )

    # DAG task 5: suggestion flags for Wikis search indices.
    # https://phabricator.wikimedia.org/T299884
    # Usage: search_indices.py SUGGESTIONS_DB SNAPSHOT PREVIOUS_SNAPSHOT COALESCE
    args = [hive_db, weekly, previous_weekly, coalesce]
    gather_search_indices_dataset = SparkSubmitOperator.for_virtualenv(
        task_id="wiki_indices",
        virtualenv_archive=conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/search_indices.py',
        launcher='skein',
        application_args=args
    )

    # Final step: feed Cassandra with all-Wikis image suggestions.
    # https://phabricator.wikimedia.org/T299885
    # Usage: spark3-sql \
    #   --driver-cores 1 \
    #   --master yarn \
    #   --conf spark.sql.catalog.aqs=com.datastax.spark.connector.datasource.CassandraCatalog \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.connection.host=aqs1010-a.eqiad.wmnet:9042,aqs1011-a.eqiad.wmnet:9042,aqs1012-a.eqiad.wmnet:9042 \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.auth.username=... \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.auth.password=... \
    #   --conf spark.sql.catalog.aqs.spark.cassandra.output.batch.size.rows=1024 \
    #   --jars /srv/deployment/analytics/refinery/artifacts/org/wikimedia/analytics/refinery/refinery-job-0.2.8-shaded.jar  \
    #   --jars spark-cassandra-connector-assembly-3.2.0-WMF-1.jar  \
    #   --conf spark.dynamicAllocation.maxExecutors=64 \
    #   --conf spark.yarn.maxAppAttempts=1 \
    #   --conf spark.executor.memoryOverhead=2048  \
    #   --executor-memory 8G \
    #   --executor-cores 2 \
    #   --driver-memory 4G \
    #   --name hive_to_cassandra_suggestions \
    #       -e "INSERT INTO aqs.suggestions SELECT ......"
    spark_cassandra_connector_uri = var_props.get(
        'spark_cassandra_connector_uri',
        artifact('spark-cassandra-connector-assembly-3.2.0-WMF-1.jar')
    )
    hive_to_cassandra_jar_uri = var_props.get('hive_to_cassandra_jar_uri', artifact('refinery-job-0.2.8-shaded.jar'))
    # Setting coalesce_partitions to 6 is important
    # as it defines how many parallel loaders we use for cassandra.
    # We currently use 6 as there are 6 cassandra hosts
    coalesce_partitions = var_props.get('coalesce_partitions', 6)

    # NOTE We create an Airflow task for every target Cassandra table
    #      See https://phabricator.wikimedia.org/T293808

    # DAG task 6: `suggestions` table
    # Comply with https://phabricator.wikimedia.org/T293808
    feed_suggestions_query = generate_spark_to_cassandra_insert_query(
        cassandra_table="suggestions",
        coalesce_num=coalesce_partitions,
        hive_columns="wiki,page_id,id,image,origin_wiki,confidence,found_on,kind,page_rev",
        hive_db=hive_db,
        hive_table="image_suggestions_suggestions",
        week=weekly
    )
    feed_suggestions = SparkSqlOperator(
        task_id=f"hive_to_cassandra_suggestions",
        sql=feed_suggestions_query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein"
    )

    # DAG task 7: `title_cache` table
    feed_title_cache_query = generate_spark_to_cassandra_insert_query(
        cassandra_table="title_cache",
        coalesce_num=coalesce_partitions,
        hive_columns="wiki,page_id,page_rev,title",
        hive_db=hive_db,
        hive_table="image_suggestions_title_cache",
        week=weekly
    )
    feed_title_cache = SparkSqlOperator(
        task_id=f"hive_to_cassandra_title_cache",
        sql=feed_title_cache_query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein"
    )

    # DAG task 8: `instanceof_cache` table
    feed_instanceof_cache_query = generate_spark_to_cassandra_insert_query(
        cassandra_table="instanceof_cache",
        coalesce_num=coalesce_partitions,
        hive_columns="wiki,page_id,page_rev,instance_of",
        hive_db=hive_db,
        hive_table="image_suggestions_instanceof_cache",
        week=weekly
    )
    feed_instanceof_cache = SparkSqlOperator(
        task_id=f"hive_to_cassandra_instanceof_cache",
        sql=feed_instanceof_cache_query,
        jars=f"{spark_cassandra_connector_uri},{hive_to_cassandra_jar_uri}",
        conf=cassandra_conf,
        launcher="skein"
    )

    # DAG task 9: push metrics to the prometheus gateway
    check_data_and_push_metrics = SparkSubmitOperator.for_virtualenv(
        task_id="check_data_and_push_metrics",
        virtualenv_archive=conda_env,
        entry_point='lib/python3.10/site-packages/image_suggestions/data_check.py',
        launcher='skein',
        application_args=args,
    )

    # Specify task order in DAG
    weekly_snapshot_sensor >> \
        monthly_sensors >> \
        gather_commons_dataset >> \
        gather_cassandra_dataset >> \
        gather_search_indices_dataset >> \
        feed_suggestions >> \
        feed_title_cache >> \
        feed_instanceof_cache >> \
        check_data_and_push_metrics
