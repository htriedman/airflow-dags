"""Coalesce query events and feed them into relforge"""
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from datetime import datetime, timedelta
from search.config.dag_config import artifact, discolytics_conda_env_tgz, \
        eventgate_partitions, get_default_args, YMDH_PARTITION
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

dag_id='export_queries_to_relforge'
var_props = VariableProperties(f'{dag_id}_config')

# Default kwargs for all Operators
default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2021, 2, 18))
})

search_satisfaction_table = var_props.get('table_search_satisfaction', 'event.searchsatisfaction')
cirrussearch_request_table = var_props.get('mediawiki_cirrussearch_request', 'event.mediawiki_cirrussearch_request')


def get_wait_sensor(table: str, sensor_name: str) -> NamedHivePartitionSensor:
    return NamedHivePartitionSensor(
        task_id=f'wait_for_data_in_{sensor_name}',
        mode='reschedule',
        # temporary sla change to accomodate the catchup, should be reverted afterwards
        sla=timedelta(days=1),
        # Select single hourly partition
        partition_names=eventgate_partitions(table))


with DAG(
        dag_id,
        default_args=default_args,
        # min hour day month dow
        schedule_interval='38 * * * *',
        max_active_runs=1,
        catchup=True
) as dag:
    export_queries_to_relforge = SparkSubmitOperator.for_virtualenv(
        task_id='export_queries_to_relforge',
        virtualenv_archive=discolytics_conda_env_tgz,
        max_executors=10,
        jars=artifact('elasticsearch-spark-30_2.12-7.17.9.jar'),
        entry_point='bin/export_queries_to_relforge.py',
        application_args=[
            '--search-satisfaction-partition', search_satisfaction_table + '/' + YMDH_PARTITION,
            '--cirrus-events-partition', cirrussearch_request_table + '/' + YMDH_PARTITION,
            '--elastic-host', var_props.get('elastic_host', 'relforge1003.eqiad.wmnet,relforge1004.eqiad.wmnet'),
            '--elastic-port', var_props.get('elastic_port', 9243),
            '--elastic-index', var_props.get('elastic_index', 'joined_queries-{dt|yyyyMM}/_doc'),
            '--elastic-template', var_props.get('elastic_template', 'joined_queries'),
        ]
    )

    wait_for_search_satisfaction_data = get_wait_sensor(search_satisfaction_table,
                                                        'search_satisfaction')
    wait_for_cirrussearch_data = get_wait_sensor(cirrussearch_request_table,
                                                 'cirrussearch_request')
    complete = DummyOperator(task_id='complete')

    [wait_for_search_satisfaction_data, wait_for_cirrussearch_data] \
        >> export_queries_to_relforge >> complete
