#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['platform_eng', 'dags', 'section_image_recommendations_dag.py']


def test_section_topics_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="section_image_recommendations")
    assert dag is not None
    assert len(dag.tasks) == 4
