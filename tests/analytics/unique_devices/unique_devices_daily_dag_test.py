import pytest

@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'unique_devices', 'unique_devices_daily_dag.py']

def test_unique_devices_daily_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="unique_devices_daily")
    assert dag is not None
    assert len(dag.tasks) == 9
