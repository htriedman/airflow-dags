""" Subgraph Mapping Init Dag
      Create table and partitions for subgraph_mapping_weekly dag
    Subgraph Mapping Dag
      Maps triples and items to top subgraphs.
      Saves list of all subgraphs along with the mappings in tables.
      Runs every monday to match with the wikidata dumps data.
    Subgraph Query Mapping Init Dag
      Create table and partitions for subgraph_query_mapping_daily dag
    Subgraph Query Mapping Dag
      Maps all WDQS sparql queries to one or more subgraphs. The queries
      are said to access the mapped subgraphs.
      Saves query-subgraph mapping table and 3 more tables that list the
      reason of the match.
      Runs daily.
"""
from datetime import datetime, timedelta

import pendulum
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.providers.apache.hive.operators.hive import HiveOperator

from search.config.dag_config import data_path, wdqs_spark_tools, YMD_PARTITION, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import NamedHivePartitionSensor, RangeHivePartitionSensor

var_props = VariableProperties('subgraph_and_query_mapping_conf')

wikidata_table = var_props.get('wikidata_dumps_table', 'discovery.wikibase_rdf')
all_subgraphs_table = var_props.get('all_subgraphs_table', 'discovery.all_subgraphs')
top_subgraph_items_table = var_props.get('top_subgraph_items_table', 'discovery.top_subgraph_items')
top_subgraph_triples_table = var_props.get('top_subgraph_triples_table', 'discovery.top_subgraph_triples')
processed_query_table = var_props.get('processed_query_table', 'discovery.processed_external_sparql_query')
subgraph_qitem_match_table = var_props.get('subgraph_qitem_match_table', 'discovery.subgraph_qitems_match')
subgraph_predicate_match_table = var_props.get('subgraph_predicate_match_table', 'discovery.subgraph_predicates_match')
subgraph_uri_match_table = var_props.get('subgraph_uri_match_table', 'discovery.subgraph_uri_match')
subgraph_query_mapping_table = var_props.get('subgraph_query_mapping_table', 'discovery.subgraph_queries')
min_items = var_props.get('min_items', 10000)
filtering_limit = var_props.get('filtering_limit', 99)
wiki = var_props.get('wiki', 'wikidata')

rel_all_subgraphs_location = var_props.get('rel_all_subgraphs_location', 'wikidata/subgraph_analysis/all_subgraphs')
rel_top_subgraph_items_location = var_props.get('rel_top_subgraph_items_location',
                                                'wikidata/subgraph_analysis/top_subgraph_items')
rel_top_subgraph_triples_location = var_props.get('rel_top_subgraph_triples_location',
                                                  'wikidata/subgraph_analysis/top_subgraph_triples')

rel_subgraph_qitem_match_location = var_props.get('rel_subgraph_qitem_match_location',
                                                  'query_service/subgraph_qitems_match')
rel_subgraph_predicate_match_location = var_props.get('rel_subgraph_predicate_match_location',
                                                      'query_service/subgraph_predicates_match')
rel_subgraph_uri_match_location = var_props.get('rel_subgraph_uri_match_location', 'query_service/subgraph_uri_match')
rel_subgraph_query_mapping_location = var_props.get('rel_subgraph_query_mapping_location',
                                                    'query_service/subgraph_queries')

# Default kwargs for all Operators
merged_default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2022, 7, 1)),
})

with DAG(
        'subgraph_mapping_init',
        default_args=merged_default_args,
        schedule_interval='@once'
) as subgraph_mapping_dag_init:
    complete = DummyOperator(task_id='complete')
    HiveOperator(
        task_id='create_tables',
        hql=f"""
            CREATE TABLE IF NOT EXISTS {all_subgraphs_table} (
                `subgraph`                    string  COMMENT 'URI of subgraphs in wikidata',
                `count`                       string  COMMENT 'Number of items in the subgraph'
            )
            PARTITIONED BY (
                `snapshot` string,
                `wiki` string
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_all_subgraphs_location}'
            ;

            CREATE TABLE IF NOT EXISTS {top_subgraph_items_table} (
                `subgraph`                    string  COMMENT 'URI of subgraphs in wikidata',
                `item`                        string  COMMENT 'Item belonging to corresponding subgraph'
            )
            PARTITIONED BY (
                `snapshot` string,
                `wiki` string
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_top_subgraph_items_location}'
            ;

            CREATE TABLE IF NOT EXISTS {top_subgraph_triples_table} (
                `subgraph`                    string  COMMENT 'URI of subgraphs in wikidata',
                `item`                        string  COMMENT 'Item belonging to corresponding subgraph',
                `subject`                     string  COMMENT 'Subject of the triple',
                `predicate`                   string  COMMENT 'Predicate of the triple',
                `object`                      string  COMMENT 'Object of the triple',
                `predicate_code`              string  COMMENT 'Last suffix of the predicate of the triple (i.e P123, rdf-schema#label etc)'
            )
            PARTITIONED BY (
                `snapshot` string,
                `wiki` string
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_top_subgraph_triples_location}'
            ;
            """  # noqa
    ) >> complete

with DAG(
        'subgraph_mapping_weekly',
        default_args=merged_default_args,
        # wikidata dumps dated every monday are available the next friday ~6 UTC
        # schedule this dag for friday and set last mondays date to keep the dates in sync
        schedule_interval='0 7 * * 5',
        # As a weekly job there should never really be more than
        # one running at a time.
        max_active_runs=1,
        catchup=True,
        user_defined_macros={
            'p': pendulum,
        }
) as subgraph_mapping_dag:
    last_monday = "{{ execution_date.previous(day_of_week=p.MONDAY).format('YYYYMMDD') }}"
    wikidata_table_and_partition: str = f'{wikidata_table}/date={last_monday}/wiki={wiki}'

    wait_for_data = NamedHivePartitionSensor(
        task_id='wait_for_data',
        mode='reschedule',
        sla=timedelta(days=2),
        retries=4,
        partition_names=[wikidata_table_and_partition],
    )

    map_subgraphs = SparkSubmitOperator(
        task_id='map_subgraphs',
        conf={
            **(merged_default_args.get('conf', {})),
            **{
                # Job generates a tb+ shuffle, give it plenty of partitions
                'spark.sql.shuffle.partitions': 4096,
            }
        },
        application=wdqs_spark_tools,
        pool='sequential',
        java_class="org.wikidata.query.rdf.spark.transform.structureddata.subgraphs.SubgraphMappingLauncher",  # noqa
        max_executors=48,
        executor_cores=4,
        # Some of the joins used here have a significant skew, we need to provide extra memory
        # to ensure to joins complete without OOM'ing.
        executor_memory="24g",
        driver_memory="8g",
        application_args=[
            '--wikidata-table', wikidata_table_and_partition,
            '--all-subgraphs-table',
            f'{all_subgraphs_table}/snapshot={last_monday}/wiki={wiki}',
            '--top-subgraph-items-table',
            f'{top_subgraph_items_table}/snapshot={last_monday}/wiki={wiki}',
            '--top-subgraph-triples-table',
            f'{top_subgraph_triples_table}/snapshot={last_monday}/wiki={wiki}',
            '--min-items', min_items
        ]
    )

    complete = DummyOperator(task_id='complete')

    wait_for_data >> map_subgraphs >> complete

with DAG(
        'subgraph_query_mapping_init',
        default_args=merged_default_args,
        schedule_interval='@once',
) as subgraph_query_mapping_dag_init:
    complete = DummyOperator(task_id='complete')
    HiveOperator(
        task_id='create_tables',
        hql=f"""
            CREATE TABLE IF NOT EXISTS {subgraph_query_mapping_table} (
                `id`                string   COMMENT 'ID of the SPARQL query',
                `subgraph`          string   COMMENT 'URI of the subgraph the query accesses',
                `qid`               boolean  COMMENT 'Whether the subgraph-query match was through the subgraphs Qid',
                `item`              boolean  COMMENT 'Whether the subgraph-query match was through an item',
                `predicate`         boolean  COMMENT 'Whether the subgraph-query match was through a predicate',
                `uri`               boolean  COMMENT 'Whether the subgraph-query match was through a URI',
                `literal`           boolean  COMMENT 'Whether the subgraph-query match was through a literal'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_subgraph_query_mapping_location}'
            ;

            CREATE TABLE IF NOT EXISTS {subgraph_qitem_match_table} (
                `id`                          string  COMMENT 'ID of the SPARQL query',
                `subgraph`                    string  COMMENT 'URI of the subgraph the query accesses',
                `item`                        string  COMMENT 'Item match that caused the query to match with the subgraph'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_subgraph_qitem_match_location}'
            ;

            CREATE TABLE IF NOT EXISTS {subgraph_predicate_match_table} (
                `id`                          string  COMMENT 'ID of the SPARQL query',
                `subgraph`                    string  COMMENT 'URI of the subgraph the query accesses',
                `predicate_code`              string  COMMENT 'Wikidata predicates present in queries that are part of the subgraph (causing the match)'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_subgraph_predicate_match_location}'
            ;

            CREATE TABLE IF NOT EXISTS {subgraph_uri_match_table} (
                `id`                          string  COMMENT 'ID of the SPARQL query',
                `subgraph`                    string  COMMENT 'URI of the subgraph the query accesses',
                `uri`                         string  COMMENT 'URIs present in queries that are part of the subgraph (causing the match)'
            )
            PARTITIONED BY (
                `year`              int     COMMENT 'Unpadded year of queries',
                `month`             int     COMMENT 'Unpadded month of queries',
                `day`               int     COMMENT 'Unpadded day of queries',
                `wiki`              string  COMMENT 'Wiki name: one of {{wikidata, commons}}'
            )
            STORED AS PARQUET
            LOCATION '{data_path}/{rel_subgraph_uri_match_location}'
            ;
            """  # noqa
    ) >> complete

with DAG(
        'subgraph_query_mapping_daily',
        default_args=merged_default_args,
        schedule_interval='@daily',
        max_active_runs=4,
        catchup=True,
        user_defined_macros={
            'p': pendulum,
        },
) as subgraph_query_mapping_dag:
    # since wikidata and subgraph mappings are generated every friday (with snapshot date of the
    # last monday), this daily job will fail to find any data for the last monday any time before
    # friday. To solve it, this dag looks for data from two mondays ago, which will definitely be
    # populated by then. For example in 2022, the dags for 28-30 June will use data from 20 June,
    # since the data of 27 June is not available yet. On 1 July, Friday, the wikidata snapshot will
    # become available but the dags continue to use 20 June's data until 4 July, from 5 July, they
    # start looking for data at 27 June (because those of last monday 4 July, aren't yet available).
    second_last_monday = "{{ execution_date.previous(day_of_week=p.MONDAY)" \
                         ".previous(day_of_week=p.MONDAY).format('YYYYMMDD') }}"
    second_last_wikidata_table_and_partition: str = f'{wikidata_table}/date={second_last_monday}/wiki={wiki}'

    top_subgraph_items_table_and_partition: str = f'{top_subgraph_items_table}/snapshot={second_last_monday}/wiki={wiki}'
    top_subgraph_triples_table_and_partition: str = f'{top_subgraph_triples_table}/snapshot={second_last_monday}/wiki={wiki}'

    wait_for_data = NamedHivePartitionSensor(
        task_id='wait_for_data',
        mode='reschedule',
        sla=timedelta(days=4),
        retries=4,
        partition_names=[second_last_wikidata_table_and_partition,
                         top_subgraph_items_table_and_partition,
                         top_subgraph_triples_table_and_partition]
    )

    wait_for_sparql_queries = RangeHivePartitionSensor(
            task_id='wait_for_sparql_queries',
            table_name=processed_query_table,
            from_timestamp='{{ execution_date }}',
            to_timestamp='{{ execution_date.add(days=1) }}',
            granularity='@hourly',
            pre_partitions=[f"wiki={wiki}"],
        )

    map_subgraphs_queries = SparkSubmitOperator(
        task_id='map_subgraphs_queries',
        conf={
            **(merged_default_args.get('conf', {})),
            **{
                # Job generates a 500GB+ shuffle, increase partition count to aim for
                # 256-512MB per partition.
                'spark.sql.shuffle.partitions': 2048,
            }
        },
        application=wdqs_spark_tools,
        pool='sequential',
        java_class="org.wikidata.query.rdf.spark.transform.queries.subgraphsqueries.SubgraphQueryMappingLauncher",
        # noqa
        max_executors=128,  # takes ~40 mins with 128 max executors and ~2 hrs with 64 max executors
        executor_cores=4,
        executor_memory="12g",
        driver_memory="8g",
        application_args=[
            '--wikidata-table', second_last_wikidata_table_and_partition,
            '--top-subgraph-items-table', top_subgraph_items_table_and_partition,
            '--top-subgraph-triples-table', top_subgraph_triples_table_and_partition,
            '--processed-query-table',
            f'{processed_query_table}/{YMD_PARTITION}/wiki={wiki}',
            '--subgraph-qitem-match-query-table',
            f'{subgraph_qitem_match_table}/{YMD_PARTITION}/wiki={wiki}',
            '--subgraph-predicate-match-query-table',
            f'{subgraph_predicate_match_table}/{YMD_PARTITION}/wiki={wiki}',
            '--subgraph-uri-match-query-table',
            f'{subgraph_uri_match_table}/{YMD_PARTITION}/wiki={wiki}',
            '--subgraph-query-mapping-table',
            f'{subgraph_query_mapping_table}/{YMD_PARTITION}/wiki={wiki}',
            '--filtering-limit', filtering_limit,
        ]
    )

    complete = DummyOperator(task_id='complete')

    [wait_for_data, wait_for_sparql_queries] >> map_subgraphs_queries >> complete
