"""
### Compute labels of webrequest actor every hour

Label can be 'automated' or 'user', it is assigned using thresholds
on metrics computed over 24h of webrequest data grouped by actor.
See webrequest_actor_metrics_hourly and webrequest_actor_metrics_rollup_houly.

"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, dataset, hql_directory

dag_id = 'webrequest_actor_label_hourly'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    # Same as webrequest_actor_metrics_rollup_hourly
    start_date=var_props.get_datetime('start_date', datetime(2023, 1, 2)),
    schedule_interval='@hourly',
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
) as dag:

    sensor = dataset("hive_wmf_webrequest_actor_metrics_rollup_hourly").get_sensor_for(dag)

    etl = SparkSqlOperator(
        task_id='compute_webrequest_actor_label',
        sql=var_props.get('hql_path', f'{hql_directory}/webrequest/actor/compute_webrequest_actor_label_hourly.hql'),
        query_parameters={
            'source_table': var_props.get('source_table', 'wmf.webrequest_actor_metrics_rollup_hourly'),
            'destination_table': var_props.get('destination_table', 'wmf.webrequest_actor_label_hourly'),
            'version': var_props.get('version', '0.1'), # Version of the metrics
            'coalesce_partitions': var_props.get('coalesce_partitions', 16), # Number of output files
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
            'day': '{{ execution_date.day }}',
            'hour': '{{ execution_date.hour }}',
        },
        executor_memory='8G',
        executor_cores=2,
        conf={
            'spark.dynamicAllocation.maxExecutors': 128,
        }
    )

    sensor >> etl
