"""A DAG to aggregate pageview_hourly into daily aggregates.

The airflow dag is configured to update an existing (backfilled) daily pageviews
dataset. This dag is not meant to a backfill job, they are sizable jobs that need
to be hand-held. A new backfilled pageviews daily dataset can be created with (note,
this job takes a couple days):

PYSPARK_DRIVER_PYTHON=python \
PYSPARK_PYTHON=./env/bin/python \
spark2-submit \
    --master yarn \
    --driver-memory 4G \
    --num-executors 96 \
    --executor-cores 4 \
    --executor-memory 16G \
    --conf spark.sql.shuffle.partitions=2000 \
    --archives $CONDA_ENV#env \
    knowledge_gaps/pageviews.py \
    --mode backfill \
    --database_name fab \
    --table_prefix test_ \
    --overwrite True
"""

from datetime import datetime, timedelta

from airflow import DAG

from wmf_airflow_common.templates.time_filters import filters
from research.config import dag_config
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

dag_id = "pageviews_daily"
var_props = VariableProperties(f"{dag_id}_config")

#TODO make this an artifact.
# knowledge_gaps_version = "0.1.4"
knowledge_gaps_version = "cleanup"
knowledge_gaps_conda_env_name = f"knowledge-gaps-{knowledge_gaps_version}.conda"
knowledge_gaps_conda_url = f"https://gitlab.wikimedia.org/api/v4/projects/212/packages/generic/knowledge-gaps/{knowledge_gaps_version}/{knowledge_gaps_conda_env_name}.tgz#venv"


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 1, 1)),
    schedule_interval='@weekly',
    dagrun_timeout=timedelta(days=2),
    catchup=False,
    user_defined_filters=filters,
    tags=['spark', 'research'],
    default_args={
        **var_props.get_merged("default_args", dag_config.default_args),
        "retries": 3,
        'email_on_failure': True,
    }
) as dag:

    database_name = var_props.get('database_name', 'knowledge_gaps')

    params = {
        'launcher': 'skein',
        'driver_memory': '4G',
        'executor_memory': '12G',
        'executor_cores': 4,
        'conf': {
            'spark.executor.memoryOverhead': "2g",
            'spark.dynamicAllocation.maxExecutors': 100,
            'spark.sql.shuffle.partitions': 500,
        },
        # As of 07/02/2022 skein memory is not configurable. Turn
        # logging off in order to avoid errors like
        # "skein.exceptions.DriverError: Received message larger than
        # max (6810095 vs. 4194304)".
        'skein_app_log_collection_enabled': False,
        # TODO these settings don't affect the log level it seems, why?
        'skein_master_log_level': 'error',
        'skein_client_log_level': 'error',
        # 'command_postamble': '|| sleep 600 }',
    }


    update_pageviews_daily = SparkSubmitOperator.for_virtualenv(
        **params,
        task_id='update_pageviews_daily',
        # virtualenv_archive = dag_config.artifact("knowledge-gaps-0.1.4.conda.tgz"),
        virtualenv_archive = knowledge_gaps_conda_url,
        entry_point = f"bin/pageviews.py",
        application_args=[
            "--mode", "update",
            "--database_name", database_name,
        ],
    )

