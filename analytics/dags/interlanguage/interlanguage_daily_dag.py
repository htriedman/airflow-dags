"""
# This job waits for the presence of all pageview partions in a day then
# aggregates cross-wiki navigation counts

This job aggregates pageview_actor records into counts of navigation by users
from one wiki project to another. Only counting desktop site browsing.

The resulting output is;
* partitioned by year, month, day
* located in /wmf/data/wmf/interlanguage/navigation/daily
"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor)
from wmf_airflow_common.partitions_builder import daily_partitions
from analytics.config.dag_config import default_args, hql_directory, artifact

# Define Constants
dag_id = 'interlanguage_daily'
source_table = 'wmf.pageview_actor'
var_props = VariableProperties(f'{dag_id}_config')

# Instantiate dag to convert common structured data dump to hive table
with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    description='write_interlanguage_data_to_hive',
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 14)),
    schedule_interval='@daily',
    tags=['spark', 'hql', 'hive']
) as dag:

    # Sensor to check that all hive partitions are present
    sensor = NamedHivePartitionSensor(
        task_id='wait_for_pageview_partitions',
        partition_names=daily_partitions(
            table=source_table, granularity='@hourly'
        ),
        poke_interval=timedelta(minutes=20).total_seconds(),
    )

    # ETL Task to execute hql and write to destination table
    etl = SparkSqlOperator(
        task_id='filter_pageview_actor',
        sql=var_props.get(
            'hql_path',
            f'{hql_directory}/interlanguage/daily/interlanguage_navigation.hql',
        ),
        query_parameters={
            'source_table': source_table,
            'destination_table': var_props.get(
                'destination_table', 'wmf.interlanguage_navigation'
            ),
            'refinery_hive_jar_path': var_props.get(
                'refinery_hive_jar_path', artifact('refinery-hive-0.2.1-shaded.jar')
            ),
            'padded_date': '{{ ds }}',
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
            'day': '{{ execution_date.day }}',
            'coalesce_partitions': 1,
        },
        conf={'spark.dynamicAllocation.maxExecutors': 32}
    )

    sensor >> etl
