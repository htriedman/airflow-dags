'''
### Aggregate projectviews geographically

* This job is responsible for creating archives of pageviews aggregated by the geographic origin of their requests, at the project level
* Output is archived into /wmf/data/archive/projectview/geo

### Note
* This job waits for the presence of a projectview hive partition for that hour.
* It Runs a hive query to aggregate projectview geographically
'''

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.operators.hdfs import HDFSArchiveOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, hadoop_name_node, hql_directory, artifact, archive_directory

dag_id = 'projectview_geo'
var_props = VariableProperties(f'{dag_id}_config')
source_table = 'wmf.projectview_hourly'
year = '{{execution_date.year}}'
month = '{{execution_date.month}}'
day = '{{execution_date.day}}'
hour = '{{execution_date.hour}}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 9, 26)),
    schedule_interval='@hourly',
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=5),
        }
    ),
    user_defined_filters=filters,
    tags=['projectview', 'geo', 'archive'],
) as dag:

    sensor = NamedHivePartitionSensor(
        task_id='wait_for_projectview',
        partition_names=[f'{source_table}/year={year}/month={month}/day={day}/hour={hour}']
    )

    temporary_directory = var_props.get('temporary_directory', f'{hadoop_name_node}/wmf/tmp/analytics/{dag_id}/{{{{execution_date|to_ds_hour_nodash}}}}')

    etl1 = SparkSqlOperator(
        task_id='aggregate_projectview_geographically',
        sql=var_props.get('hql_path', f'{hql_directory}/projectview/geo/archive_projectview_geo_hourly.hql'),
        query_parameters={
            'source_table': source_table,
            'destination_directory': temporary_directory,
            'refinery_hive_jar': var_props.get('refinery_hive_jar', artifact('refinery-hive-0.2.1-shaded.jar')),
            'year': year,
            'month': month,
            'day': day,
            'hour': hour,
            'coalesce_partitions':1
        }
    )

    etl2 = HDFSArchiveOperator(
        task_id='move_data_to_archive',
        source_directory=temporary_directory,
        archive_file=var_props.get('archive_file',
            f'{archive_directory}/projectview/geo/hourly/{year}/{{{{execution_date|to_ds_month}}}}' +
            '/projectviews-geo-{{execution_date|to_ds_nodash}}-{{execution_date|to_time_nodash}}.gz'),
        archive_parent_umask="027",
        archive_perms="640",
        check_done=True
    )

    sensor >> etl1 >> etl2
