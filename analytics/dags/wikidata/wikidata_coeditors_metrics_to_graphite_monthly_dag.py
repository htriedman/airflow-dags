'''
###Generates monthly metrics for wikidata coeditors.

* Waits for mediawiki_project_namespace_map and mediawiki_history Hive partitions.
* Use the HiveToGraphite spark Job to run coeditors_metrics.hql hive query.
* Output of the query is the CoEditors metrics which is sent to Graphite by the HiveToGraphite job.
'''

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, artifact, hql_directory

dag_id = 'wikidata_coeditors_metrics_to_graphite_monthly'
var_props = VariableProperties(f'{dag_id}_config')
mediawiki_snapshot = '{{execution_date | to_ds_month}}'
mw_project_namespace_map_table = 'wmf_raw.mediawiki_project_namespace_map'
mw_history_table = 'wmf.mediawiki_history'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 5, 1)),
    schedule_interval='@monthly',  # Monthly starting 1st of every month at 00:00.
    tags=['spark', 'hql', 'graphite', 'wikidata'],
    # We use 10 days SLA as mediawiki-denormalize is needed and can be up to 39 days late. Airflow sla starts
    # counting at the end of the interval(i.e at the end of the month). A month is 30 days on the average.
    # that means we are left with 9 days to wait for mediawiki-denormalize.
    # Hence we wait 9days (for mediawiki-denormalize) + 1 day more= 10days SLA.
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=10),
        }
    ),
    user_defined_filters=filters
) as dag:

    sensor1 = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_project_namespace_map',
        poke_interval=timedelta(hours=6).total_seconds(),
        partition_names=[f'{mw_project_namespace_map_table}/snapshot={mediawiki_snapshot}']
    )

    sensor2 = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_history',
        poke_interval=timedelta(hours=6).total_seconds(),
        partition_names=[f'{mw_history_table}/snapshot={mediawiki_snapshot}']
    )

    etl = SparkSubmitOperator(
        task_id='generate_and_send_coeditormetric_to_graphite',

        # Too many logs are generated, it makes log collection crashed.
        skein_app_log_collection_enabled=False,

        application=var_props.get('refinery_job_jar',
            artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.HiveToGraphite',
        # Passing application_args as a list to make it work with multiple -d parameters
        application_args=[
            # Graphite parameters
            '--metric_prefix', var_props.get('metric_prefix', 'daily.monthly.wikidata.coeditors'),
            '--graphite_host', 'graphite-in.eqiad.wmnet',
            '--graphite_port', 2003,
            # HQL parameters
            '-f', var_props.get('query_file', f'{hql_directory}/wikidata/coeditors_metrics.hql'),
            '-d', f'mw_project_namespace_map_table={mw_project_namespace_map_table}',
            '-d', f'mw_history_table={mw_history_table}',
            '-d', f'snapshot={mediawiki_snapshot}',
        ],
        conf={'spark.dynamicAllocation.maxExecutors': 128}
    )

    [sensor1, sensor2] >> etl