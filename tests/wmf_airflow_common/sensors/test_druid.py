
from pytest import raises
from wmf_airflow_common.sensors.druid import normalize_interval, DruidAPI
import requests


class MockResponse:

    def raise_for_status(self):
        pass

    def json(self):
        return {"mock_key": "mock_response"}


def test_normalize_interval(monkeypatch):
    normalized_interval_1 = normalize_interval("2023-01-01T10:30:15/2023-03-14T20:45:00")
    normalized_interval_2 = normalize_interval("2000-01-01T00:00:00.000Z/2001-01-01T00:00:00.000Z")
    assert normalized_interval_1 == "20230101103015/20230314204500"
    assert normalized_interval_2 == "20000101000000/20010101000000"
    with raises(ValueError):
        normalize_interval("invalid/interval")


def test_druid_api(monkeypatch):
    api = DruidAPI("some.druid.host", "8081")
    datasource = "some_datasource"
    def mock_get(uri, timeout):
        assert uri == (
            "http://some.druid.host:8081/druid/coordinator/v1/"
            f"metadata/segments?datasources={datasource}"
        )
        assert timeout == 10
        return MockResponse()
    monkeypatch.setattr(requests, "get", mock_get)
    segments = api.get_datasource_segments(datasource)
    assert segments == {"mock_key": "mock_response"}
