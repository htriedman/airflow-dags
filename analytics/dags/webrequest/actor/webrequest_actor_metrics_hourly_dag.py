"""
### Compute hourly metrics over webrequest-text grouped by actor

Metrics computed in this dataset are rolled-up over 24h into
webrequest_actor_metrics_rollup_hourly, which is then used to compute
webrequest_actor_label_hourly, splitting actors between automated and users.

"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, dataset, hql_directory, artifact

dag_id = 'webrequest_actor_metrics_hourly'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2023, 1, 1)),
    schedule_interval='@hourly',
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
) as dag:

    sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)

    etl = SparkSqlOperator(
        task_id='compute_webrequest_actor_metrics',
        sql=var_props.get('hql_path', f'{hql_directory}/webrequest/actor/compute_webrequest_actor_metrics_hourly.hql'),
        query_parameters={
            'source_table': var_props.get('source_table', 'wmf.webrequest'),
            'destination_table': var_props.get('destination_table', 'wmf.webrequest_actor_metrics_hourly'),
            'refinery_hive_jar_path': var_props.get('refinery_hive_jar_path', artifact('refinery-hive-0.2.1-shaded.jar')),
            'version': var_props.get('version', '0.1'), # Version of the metrics
            'coalesce_partitions': var_props.get('coalesce_partitions', 2), # Number of output files
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
            'day': '{{ execution_date.day }}',
            'hour': '{{ execution_date.hour }}',
        },
        executor_memory='8G',
        executor_cores=2,
        conf={
            'spark.dynamicAllocation.maxExecutors': 128,
        }
    )

    sensor >> etl
