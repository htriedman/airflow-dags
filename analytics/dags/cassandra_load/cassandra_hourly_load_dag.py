'''

This job loads the specified datasets listed below hourly
from their respective hive tables to cassandra

The datasets covered within this job include:
   pageview_per_project

'''

from airflow import DAG
from datetime import datetime, timedelta
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor)
from wmf_airflow_common.partitions_builder import partition_names_by_granularity
from wmf_airflow_common.operators.spark import SparkSqlOperator
from analytics.config.dag_config import default_args, hql_directory, cassandra_default_conf, artifact


dag_id = 'cassandra_hourly_load'
var_props = VariableProperties(f'{dag_id}_config')
execution_conf =  var_props.get(
    'conf',
    {
        **cassandra_default_conf,
        'spark.dynamicAllocation.maxExecutors': 64,
        'spark.yarn.maxAppAttempts': 1,
        'spark.executor.memoryOverhead': 2048
    }
)

jobs_properties = var_props.get('jobs_properties', {
    'pageview_per_project': {
        'hql_parameters': {
            'source_table': 'wmf.projectview_hourly',
            'destination_table': 'aqs.local_group_default_T_pageviews_per_project_v2.data'
        },
        'hql_path': f'{hql_directory}/cassandra/hourly/load_cassandra_pageview_per_project_hourly.hql',
        'source_granularity': '@hourly'
    },
})

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 9, 20, 10)),
    schedule_interval='@hourly',
    tags=['cassandra', 'hql'],
    default_args=var_props.get_merged('default_args',
        {
            **default_args,
            'sla': timedelta(hours=6),
        }
    )
) as dag:
    for job_name, properties in jobs_properties.items():
    # Sensor to check that all hive partitions are present
        sensor = NamedHivePartitionSensor(
            task_id=f'wait_for_{job_name}_partitions',
            partition_names=partition_names_by_granularity(
                table=properties['hql_parameters']['source_table'],
                granularity=properties['source_granularity']
            ),
            poke_interval=timedelta(minutes=10).total_seconds()
        )

        etl = SparkSqlOperator(
            task_id=f'load_{job_name}_hourly_to_cassandra',
            sql=properties['hql_path'],
            query_parameters={
                **properties['hql_parameters'],
                'year': '{{execution_date.year}}',
                'month': '{{execution_date.month}}',
                'day': '{{execution_date.day}}',
                'hour': '{{execution_date.hour}}',
                # Setting coalesce_partitions to 6 is important
                # as it defines how many parallel loaders we use for cassandra.
                # We currently use 6 as there are 6 cassandra hosts
                'coalesce_partitions': 6,
            },
            executor_memory='8G',
            executor_cores=2,
            driver_memory='2G',
            driver_cores=1,
            jars=var_props.get('jars', artifact('spark-cassandra-connector-assembly-3.2.0-WMF-1.jar') + ',' +
                                       artifact('refinery-job-0.2.7-shaded.jar')),
            conf=execution_conf
        )

        sensor >> etl
