import os
from typing import Union, Optional, Any

import skein
from airflow.operators.python import PythonOperator
from airflow import AirflowException
from wmf_airflow_common.hooks.skein import SkeinHook, SkeinHookBuilder


class SkeinOperator(PythonOperator):
    """
    Given a skein ApplicationSpec, runs the application in YARN.
    """

    def __init__(
        self,
        skein_application_spec: Union[skein.model.ApplicationSpec, str],
        skein_client_kwargs: Optional[dict] = None,
        **kwargs: Any
    ):
        """
        :param skein_application_spec:
            skein.model.ApplicationSpec or path to application spec file.
            If a path, ApplicationSpec.from_file() will be called on that file.

        :param skein_client_kwargs:
            kwargs to pass when instantiating skein.Client.

        """
        self._skein_hook = SkeinHook(
            application_spec=skein_application_spec,
            client_kwargs=skein_client_kwargs
        )

        super().__init__(
            python_callable=self._skein_hook.submit,
            **kwargs
        )

    def on_kill(self) -> None:
        self._skein_hook.on_kill()


class SimpleSkeinOperator(PythonOperator):
    """
    Runs a simple Skein application consisting of only
    a single YARN AppMaster.  This is kind of like the BashOperator,
    except that the script will be run in YARN instead of locally.

    Uses wmf_airflow_common.hooks.skein.SkeinHookBuilder
    to aid in building a simple SkeinHook.
    """

    # Need to ignore mypy incompatible type assignment warning here.
    # PythonOperator defines this as a 3 tuple, but we want to override it.
    template_fields = PythonOperator.template_fields + (  # type: ignore
        '_name',
        '_queue',
        '_principal',
        '_keytab',
    )

    def __init__(
        self,
        name: Optional[str] = '{{ task_instance_key_str }}',
        script: Optional[str] = None,
        queue: Optional[str] = None,
        resources: Optional[dict] = None,
        files: Optional[Union[str, list, dict]] = None,
        env: Optional[dict] = None,
        master_log_level: Optional[str] = None,
        client_log_level: Optional[str] = None,
        app_log_collection_enabled: bool = True,
        principal: Optional[str] = None,
        keytab: Optional[str] = None,
        **kwargs: Any,
    ):
        """
        Params are passed to SkeinHookBuilder methods.

        :param name:
            Will be used as Skein YARN application name.
            defaults to '{{ task_instance_key_str }}'

        :param script:
            Script to run in YARN.  If keytab and principal are passed
            a proper kinit will be prepended to the script

        :param queue:
            YARN queue. defaults to None

        :param resources:
            skein Resources to use for YARN application master.
            Provide this as a dict like:
            { memory: 2048, vcores: 4 }
            defaults to None

        :param files:
            files to upload to the YARN AppMaster.
            See docs for SkeinHookBuilder.files() for what is accepted.
            If keytab and principal are passed, the keytab will be added
            to the files.

        :param env:
            dict of env vars to set in YARN AppMaster, defaults to None

        :param master_log_level:
            Skein log level for YARN AppMaster, defaults to None

        :param client_log_level:
            Skein log level for Skein Client, defaults to None

        :param app_log_collection_enabled:
            If True (default), logs from the YARN AppMaster will be collected
            and logged after the application finishes.

        :param principal:
            Kerberos principal, defaults to None

        :param keytab:
            Path to kerberos keytab, defaults to None

        """
        if not script:
            raise AirflowException(
                f'Failed instantiating {self.__class__.__name__}: '
                'script is a required parameter.'
            )

        self._name = name
        self._queue = queue
        self._resources = resources
        self._files = files
        self._env = env
        self._master_log_level = master_log_level
        self._client_log_level = client_log_level
        self._principal = principal
        self._keytab = keytab
        self._app_log_collection_enabled = app_log_collection_enabled

        super().__init__(
            python_callable=self.hook_submit,
            op_args=[script],
            **kwargs
        )

    def hook_submit(self, script: str) -> None:

        builder = SkeinHookBuilder()

        if self._name:
            builder.name(self._name)
        if self._queue:
            builder.queue(self._queue)
        if self._resources:
            builder.resources(self._resources)
        if self._files:
            builder.files(self._files)
        if self._env:
            builder.env(self._env)
        if self._master_log_level:
            builder.master_log_level(self._master_log_level)
        if self._client_log_level:
            builder.client_log_level(self._client_log_level)
        if self._principal:
            builder.principal(self._principal)
        if self._keytab:
            builder.keytab(self._keytab)

        if self._principal and self._keytab:
            keytab_alias = os.path.basename(self._keytab)
            builder.files({keytab_alias: self._keytab}, True)
            script = f'kinit -k -t {keytab_alias} {self._principal} && {script}'

        builder.app_log_collection_enabled(self._app_log_collection_enabled)
        builder.script(script)
        self._skein_hook = builder.build()
        self._skein_hook.submit()

    def on_kill(self) -> None:
        self._skein_hook.on_kill()
