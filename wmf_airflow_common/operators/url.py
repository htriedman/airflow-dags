import fsspec
from airflow.models import BaseOperator
from typing import Any
from workflow_utils.util import fsspec_use_new_pyarrow_api
from urllib.parse import urlparse

fsspec_use_new_pyarrow_api(should_set_hadoop_env_vars=True)


def fsspec_touch(url: str) -> bool:
    """
    Touches the file at URL.
    This only works with URLs that are supported by fsspec on writeable file systems.

    :param url:
        A URL to touch.
    """
    # touch is a method on fsspec FileSystem, so
    # we 'open' the file (with fsspec, not python file IO),
    # get the fsspec FileSystem, and then
    # make FileSystem touch the url path on that FileSystem.
    url_parsed = urlparse(url)
    return bool(fsspec.open(url).fs.touch(url_parsed.path))


class URLTouchOperator(BaseOperator):
    """
    Uses fsspec client to touch output files.
    This is useful for writing _SUCCESS flags or any other file.
    It is used to mediate between jobs that are already on Oozie and those we are still migrating to airflow.
    """

    template_fields = ('_url',)


    def __init__(self,*, url: str, **kwargs: Any):
        """
        :param_url:
          url to write to
        """
        self._url = url

        super().__init__(**kwargs)

    def execute(self, context: Any) -> None:
        # Creates an empty file
        fsspec_touch(self._url)
