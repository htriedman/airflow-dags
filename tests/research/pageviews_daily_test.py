import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['research', 'dags', 'pageviews_daily.py']


def test_pageviews_daily_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="pageviews_daily")
    assert dag is not None
    assert len(dag.tasks) == 1
