import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'anomaly_detection', 'anomaly_detection_mobile_os_distribution_daily_dag.py']


def test_anomaly_detection_mobile_os_distribution_daily_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="anomaly_detection_mobile_os_distribution_daily")
    assert dag is not None
    assert len(dag.tasks) == 5
    task_2 = dag.get_task('detect_anomalies')
    application_parameters = task_2._application_args
    outpath_param_index = application_parameters.index('--output-path') + 1
    assert '/anomaly_detection/' in application_parameters[outpath_param_index]
