from wmf_airflow_common.config import deprecated_spark2_dag_default_args
from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = 'hdfs://analytics-hadoop'
refinery_directory = f'{hadoop_name_node}/wmf/refinery'
hql_directory = f'{refinery_directory}/current/hql'
artifacts_directory = f'{refinery_directory}/current/artifacts'
hdfs_temp_directory = '/tmp/research'


# DAG default_args for this Airflow Instance.
instance_default_args = {
    'owner': 'analytics-research',
    'email': 'research-alerts@wikimedia.org',
    'metastore_conn_id': 'analytics-hive',
}

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args['queue'] = 'production'

# Default arguments for all operators used by this airflow instance.
default_args = deprecated_spark2_dag_default_args.get(instance_default_args)

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# synatactic sugar to use when declaring artifacts in dags.
artifact = ArtifactRegistry.for_wmf_airflow_instance('research').artifact_url
