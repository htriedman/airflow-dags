import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'session_length', 'session_length_daily_dag.py']


def test_session_length_daily_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id='session_length_daily')
    assert dag is not None
    assert len(dag.tasks) == 2
    first_sensor = dag.get_task('wait_for_session_tick')
    assert first_sensor.table_name == 'event.mediawiki_client_session_tick'
    assert first_sensor.pre_partitions == ['datacenter=eqiad']
    assert first_sensor.from_timestamp == '{{execution_date | start_of_current_day}}'
    assert first_sensor.to_timestamp == '{{execution_date | start_of_next_day | add_hours(2)}}'
    assert first_sensor.granularity == '@hourly'
