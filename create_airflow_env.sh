#!/usr/bin/bash
rm -rf /home/htriedman/.conda/envs/htriedman_airflow_base
source /usr/lib/airflow/etc/profile.d/conda.sh
export http_proxy="http://webproxy.eqiad.wmnet:8080"
export https_proxy="http://webproxy.eqiad.wmnet:8080"
conda create --name htriedman_airflow_base python=3.7
conda activate htriedman_airflow_base
pip install "apache-airflow==2.1.4" --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-2.1.4/constraints-3.7.txt"
pip install "apache-airflow[mysql,celery,kerberos,apache.hdfs,apache.hive,apache.sqoop,apache.spark,apache.cassandra,apache.druid]==2.1.4" --constraint "https://raw.githubusercontent.com/apache/airflow/constraints-2.1.4/constraints-3.7.txt"
pip install "pyspark==3.1.2"
pip install -r "/home/htriedman/airflow-dags/pip-requirements.txt"
conda deactivate
