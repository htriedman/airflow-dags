"""A DAG to execute the full knowledge gaps pipeline.
"""

from datetime import datetime, timedelta
import os

from airflow import DAG

from wmf_airflow_common.templates.time_filters import filters
from research.config import dag_config
from research.dags.snapshot_sensor import knowledge_gaps_sensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

dag_id = "knowledge_gaps_pipeline"
var_props = VariableProperties(f"{dag_id}_config")

# snapshots that the sensors will wait for
mediawiki_snapshot = '{{execution_date | start_of_previous_month | to_ds_month}}'
wikidata_snapshot = '{{execution_date | start_of_previous_month | start_of_next_week | to_ds}}'

#TODO make this an artifact? would be nice to have a utitliy where we can just specify the version or branch
knowledge_gaps_version = "0.1.5"
knowledge_gaps_conda_env_name = f"knowledge-gaps-{knowledge_gaps_version}.conda"
knowledge_gaps_conda_url = f"https://gitlab.wikimedia.org/api/v4/projects/212/packages/generic/knowledge-gaps/{knowledge_gaps_version}/{knowledge_gaps_conda_env_name}.tgz#venv"

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 1, 1)),
    schedule_interval='@monthly',
    dagrun_timeout=timedelta(days=30),
    catchup=False,
    user_defined_filters=filters,
    tags=['spark', 'hive', 'research'],
    default_args={
        **var_props.get_merged("default_args", dag_config.default_args),
        "retries": 3
    }
) as dag:
    # for production, the airflow variable has to be set to "production"
    mode = var_props.get('mode', 'development')

    hdfs_temp_directory = var_props.get('hdfs_temp_directory', dag_config.hdfs_temp_directory)
    run_dir = "{{execution_date | to_ds_nodash}}"
    hdfs_dir = os.path.join(hdfs_temp_directory, "knowledge_gaps", run_dir)

    output_database = var_props.get('output_database', 'knowledge_gaps_dev')
    table_prefix = var_props.get('table_prefix', 'output_')

    start_time_bucket = var_props.get('start_time_bucket', '20050101')
    end_time_bucket = var_props.get('end_time_bucket', '{{execution_date | start_of_next_month | to_ds_nodash}}')

    time_bucket_freq = var_props.get('time_bucket_freq', 'monthly')

    # development mode configuration
    dev_input_config = []
    content_gaps_to_generate = []
    if mode == "development":
        dev_input_config.extend([
            "--dev_database_name", var_props.get('dev_database_name', 'knowledge_gaps_dev'),
            "--dev_table_prefix", var_props.get('dev_table_prefix', 'dev_'),
        ])

        if var_props.get('content_gaps', None):
            content_gaps_to_generate = [ "--content_gaps", var_props.get('content_gaps', None) ]

    common_params = {
        'launcher': 'skein',
        'driver_memory': '4G',
        # As of 07/2022 skein memory is not configurable. Turn
        # logging off in order to avoid errors like
        # "skein.exceptions.DriverError: Received message larger than
        # max (6810095 vs. 4194304)".
        'skein_app_log_collection_enabled': False if mode=="production" else True,
        # to keep the skein containers running for development
        # 'command_postamble': '; sleep 600',
    }

    sensor_op = knowledge_gaps_sensor(mediawiki_snapshot, wikidata_snapshot)

    compute_feature_metrics = SparkSubmitOperator.for_virtualenv(
        task_id='metric_features',
        **common_params,
        executor_memory='8G',
        executor_cores=4,
        conf={
            "spark.dynamicAllocation.maxExecutors": 100 if mode=="production" else 10,
            'spark.sql.shuffle.partitions': 4000 if mode=="production" else 20,
        },
        virtualenv_archive = knowledge_gaps_conda_url,
        entry_point = f"bin/knowledge_gaps_feature_metrics.py",
        application_args=[
            start_time_bucket,
            end_time_bucket,
            "--time_bucket_freq", time_bucket_freq,
            "--mediawiki_snapshot", mediawiki_snapshot,
            "--mode", mode,
            "--hdfs_dir", hdfs_dir
        ] + dev_input_config,
    )

    compute_content_features = SparkSubmitOperator.for_virtualenv(
        task_id='content_gap_features',
        **common_params,
        executor_memory='8G',
        executor_cores=2,
        conf={
            "spark.dynamicAllocation.maxExecutors": 120 if mode=="production" else 10,
            'spark.sql.shuffle.partitions': 2000 if mode=="production" else 20,
        },
        virtualenv_archive = knowledge_gaps_conda_url,
        entry_point = f"bin/knowledge_gaps_content_features.py",
        application_args=[
            "--mediawiki_snapshot", mediawiki_snapshot,
            "--wikidata_snapshot", wikidata_snapshot,
            "--mode", mode,
            "--hdfs_dir", hdfs_dir
        ] + dev_input_config + content_gaps_to_generate,
    )

    compute_content_gap_metrics = SparkSubmitOperator.for_virtualenv(
        task_id='content_gap_metrics',
        **common_params,
        executor_memory='8G',
        executor_cores=4,
        conf={
            "spark.dynamicAllocation.maxExecutors": 100 if mode=="production" else 10,
            'spark.sql.shuffle.partitions': 4000 if mode=="production" else 20,
        },
        virtualenv_archive = knowledge_gaps_conda_url,
        entry_point = f"bin/knowledge_gaps_aggregation.py",
        application_args=[
            "--hdfs_dir", hdfs_dir,
        ] + content_gaps_to_generate,
    )

    generate_output_datasets = SparkSubmitOperator.for_virtualenv(
        task_id='output_datasets',
        **common_params,
        executor_memory='8G',
        executor_cores=4,
        conf={
            "spark.dynamicAllocation.maxExecutors": 40 if mode=="production" else 4,
            'spark.sql.shuffle.partitions': 20 if mode=="production" else 2,
        },
        virtualenv_archive = knowledge_gaps_conda_url,
        entry_point = f"bin/knowledge_gaps_datasets.py",
        application_args=[
            "--hdfs_dir", hdfs_dir,
            "--database_name", output_database,
            "--table_prefix", table_prefix,
            "--store_content_gap_features",
            "--store_normalized_metrics",
            "--store_denormalized_metrics",
            "--store_knowledge_gap_index_metrics",
        ],
    )

    sensor_op >> \
    [compute_feature_metrics, compute_content_features ] >> \
    compute_content_gap_metrics >> \
    generate_output_datasets

