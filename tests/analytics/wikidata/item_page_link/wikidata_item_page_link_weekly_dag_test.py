import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'wikidata', 'item_page_link',
            'wikidata_item_page_link_weekly_dag.py']


def test_wikidata_item_page_link_weekly_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id='wikidata_item_page_link_weekly')
    assert dag is not None
    assert len(dag.tasks) == 5
    first_sensor = dag.get_task('wait_for_wikidata_entity')
    assert first_sensor.partition_names == [
        'wmf.wikidata_entity'
        '/snapshot={{execution_date | start_of_current_week | to_ds}}']
    second_sensor = dag.get_task('wait_for_mediawiki_page_move')
    assert second_sensor.table_name == 'event.mediawiki_page_move'
    # TODO Change this back to eqiad once the datacenter switchover test is finished.
    # assert second_sensor.pre_partitions == ['datacenter=eqiad']
    assert second_sensor.pre_partitions == ['datacenter=codfw']
    assert second_sensor.from_timestamp ==\
           '{{execution_date | start_of_next_week | start_of_current_month}}'
    assert second_sensor.to_timestamp == '{{execution_date | start_of_next_week}}'
    assert second_sensor.granularity == '@hourly'
    etl = dag.get_task('process_item_page_link_data')
    assert etl._executor_memory == '8G'
    assert etl._driver_memory == '8G'
    assert etl._conf['spark.dynamicAllocation.maxExecutors'] == 128
    assert etl._query_parameters['coalesce_partitions'] == 64
