import os
import pytest
from airflow.models import DagBag

@pytest.fixture(name='dagbag')
def fixture_dagbag():
    dag_bag = DagBag(None, include_examples=False, read_dags_from_db=False)
    dag_file=os.path.join(os.path.abspath(os.getcwd()), 'analytics', 'dags', 'mediawiki', 'mediawiki_history_load_dag.py')
    dag_bag.process_file(dag_file)
    return dag_bag


def test_mediawiki_history_load_dag(dagbag):
    assert not dagbag.import_errors
    dag=dagbag.get_dag(dag_id="mediawiki_history_load")
    assert len(dag.tasks) == 102
