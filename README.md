# Airflow DAGs

Collection of DAGs and common libraries to be executed by the WMF Airflow instances.

## Instance directories

Data Engineering maintains several airflow instances, each usually associated with a
specific engineering team or function. To collaborate together, we this single
airflow-dags repository deployed to each instance, with instance specific dags directories
and config.

Instance directories contain a dags/ folder, as well as a config/ folder.
Airflow DAGs for an instance should be placed in the `<instance>/dags` folder.

## Artifact syncing
[data-engineering/workflow_utils](https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils) is used to abstract away details of where job artifacts live from
DAG code.

Artifacts are declared in an instance's `<instance>/config/artifacts.yaml` file,
and are automatically synced from defined sources to defined caches during
scap deployment of this repository to an WMF Airflow instance.

The cached artifact URLs (usually in HDFS) are then automatically looked up
when using the `artifact()` function defined in an instance's config/dag_config.py.
Example:

```python
from my_instance_name.config.dag_config import artifact

with DAG(...) as dag:
    t1 = SparkSubmitOperator(archives=artifact('my_artifact-0.0.1.tgz'))
```

`artifact` will look up the actual URL to use for the given artifact name.



## wmf_airflow_common

Common Airflow classes and configs used for WMF Airflow instances.

Operators and hooks are generally kept non WMF specific. However,
wmf_airflow_common uses WMF conventions to automate configs for our
airflow instances.

### Spark Operatros

wmf_airflow_common comes with 2 custom Spark Operators: `SparkSubmitOperator` and
`SparkSqlOperator`.  These are compatible with Airflow's built in Spark operators,
but add additional features like:
- not having to use airflow connections
- Launching Spark via skein
- Overriding the java_class used for SparkSql.

#### SparkSubmitOperator for conda dist envs

Our `SparkSubmitOperator` has a factory method help construct `SparkSubmitOperator`s
for use with conda dist env archives:

```python
t1 = SparkSubmitOperator.for_virtualenv(
    virtualenv_archive = artifact('my-conda-dist-env-0.1.0.tgz'),
    entry_point = 'bin/my_spark_job.py',
    launcher = 'skein',
    # ... Other SparkSubmitOperator constructor keyword args here.
)
```

This is meant to work with conda dist env archives without having them
locally available to the Airflow Scheduler, so setting `launcher='skein'`
and using a `virtualenv_archive` in HDFS is probably what you want to do.

There is experimental support for using your own pyspark dependency
from the conda virtualenv.  If you have pyspark installed in your conda
virtualenv, setting `use_virtualenv_spark=True` will cause `SparkSubmitOperator`
to set `spark_binary` to the path to bin/spark-submit in your virtualenv archive.
NOTE: While this works, there are extra configurations that need to be set
to work with [Hadoop](https://spark.apache.org/docs/latest/hadoop-provided.html#apache-hadoop)
and [Hive](https://spark.apache.org/docs/latest/sql-data-sources-hive-tables.html#hive-tables).

### Skein Operators
The `SkeinOperator` and `SimpleSkeinOperator` can be used to launch generic
applications in YARN via Skein.


## Running Tests

Running test and/or linters in local dev environment:
```
pip install .[test]
flake8
mypy
pytest
```

Running test and linters in isolated environment using tox:
```
pip install tox
tox
```

You could also check that your dags are loading with `make load_dags`
