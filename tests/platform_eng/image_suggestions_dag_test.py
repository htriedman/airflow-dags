import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['platform_eng', 'dags', 'image_suggestions_dag.py']


def test_image_suggestions_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="image-suggestions")
    assert dag is not None
    assert len(dag.tasks) == 15


def test_image_suggestions_query_generation():
    from platform_eng.dags.image_suggestions_dag import generate_spark_to_cassandra_insert_query

    query = generate_spark_to_cassandra_insert_query(
        cassandra_table="ct",
        coalesce_num=6,
        hive_columns="a,b,c",
        hive_db="hdb",
        hive_table="ht",
        week="2023-01"
    )

    assert query == \
           "\"INSERT INTO aqs.image_suggestions.ct SELECT /*+ COALESCE(6) */ a,b,c FROM hdb.ht WHERE snapshot='2023-01'\""  # noqa: E501
