from typing import Any
import fsspec
from urllib.parse import urlparse
from airflow.sensors.base import BaseSensorOperator
# TODO: after https://gitlab.wikimedia.org/repos/data-engineering/workflow_utils/-/merge_requests/5 is deployed,
# use this to automate using new pyarrow HDFS API via fsspec:
from workflow_utils.util import fsspec_use_new_pyarrow_api

# This should be removed once https://github.com/fsspec/filesystem_spec/issues/874 is resolved.
fsspec_use_new_pyarrow_api(should_set_hadoop_env_vars=True)

def fsspec_exists(url: str) -> bool:
    """
    :param url:
        A URL to check if exists via fsspec.
    """
    # exists is a method on fsspecn FileSystem, so
    # we 'open' the file (with fsspec, not python file IO),
    # get the fsspec FileSystem, and then
    # ask the FileSystem if the url path exists on that FileSystem.
    url_parsed = urlparse(url)
    return bool(fsspec.open(url).fs.exists(url_parsed.path))

class URLSensor(BaseSensorOperator):
    """
    A Sensor that detects if a URL exists.
    This Sensor uses fsspec, so any URL
    supported by fsspec will work.
    """

    template_fields = (
        '_url',
    )

    def __init__(self, url: str, **kwargs: Any):
        """
        :param url:
            url to check for existence.
            Any URL protocol supported by fsspec can be used,
            e.g. file:// http://, hdfs:// etc.
        """

        self._url = url
        super().__init__(**kwargs)

    def poke(self, context: dict) -> bool:
        self.log.info('Checking for existence of %s ', self._url)
        # check if there is a file at the given path
        return fsspec_exists(self._url)
        