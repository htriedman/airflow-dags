"""A DAG to generate article quality scores.
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from wmf_airflow_common.templates.time_filters import filters
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.partitions_builder import add_post_partitions, PrePartitions
from research.config import dag_config

dag_id = "research_article_quality"
var_props = VariableProperties(f"{dag_id}_config")

# As of 07/01/2022, latest snapshot for wmf.mediawiki_page_history is
# 2022-05. That's why we need to go back 2 months to be safe.
mediawiki_snapshot = '{{execution_date | start_of_previous_month | start_of_previous_month | to_ds_month}}'
wikidata_snapshot = '{{execution_date | start_of_previous_month | start_of_current_week | to_ds}}'
wiki_list = var_props.get_list('wiki_list', [])

article_quality_version = "0.0.2"
article_quality_conda_env_name = f"article-quality-{article_quality_version}.conda"
article_quality_conda_url = f"https://gitlab.wikimedia.org/api/v4/projects/211/packages/generic/article-quality/{article_quality_version}/{article_quality_conda_env_name}.tgz#{article_quality_conda_env_name}"


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 1, 1)),
    schedule_interval='@monthly',
    dagrun_timeout=timedelta(days=8),
    catchup=False,
    user_defined_filters=filters,
    tags=['spark', 'hql', 'hive', 'research', 'article-quality'],
    default_args={
        **var_props.get_merged("default_args", dag_config.default_args),
        "retries": 0
    }
) as dag:
    wait_for_mediawiki_project_namespace_map_snapshot = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_project_namespace_map_snapshot',
        partition_names=[f'wmf_raw.mediawiki_project_namespace_map/snapshot={mediawiki_snapshot}']
    )

    wiki_db_partitions = PrePartitions([[f'wiki_db={db}' for db in wiki_list]])

    wait_for_mediawiki_page_snapshot = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_page_snapshot',
        partition_names=add_post_partitions(
            [f'wmf_raw.mediawiki_page/snapshot={mediawiki_snapshot}'],
            wiki_db_partitions
        )
    )

    wait_for_mediawiki_wikitext_history_snapshot = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_wikitext_history_snapshot',
        partition_names=add_post_partitions(
            [f'wmf.mediawiki_wikitext_history/snapshot={mediawiki_snapshot}'],
            wiki_db_partitions
        )
    )

    wait_for_wikidata_item_page_link_snapshot = NamedHivePartitionSensor(
        task_id='wait_for_wikidata_item_page_link_snapshot',
        partition_names=[f'wmf.wikidata_item_page_link/snapshot={wikidata_snapshot}']
    )

    params = {
        'launcher': 'skein',
        'driver_memory': '4G',
        'executor_memory': '16G',
        'executor_cores': 8,
        'conf': {
            'spark.sql.shuffle.partitions': 1024,
            'spark.shuffle.service.enabled': True,
            'spark.dynamicAllocation.enabled': True,
            'spark.dynamicAllocation.maxExecutors': 96,
            'spark.dynamicAllocation.minExecutors': 8,
            'spark.dynamicAllocation.initialExecutors': 32,
            # Enable everyone to read/write the output Parquet files.
            'spark.hadoop.fs.permissions.umask-mode': '000'
        },
        # As of 07/02/2022 skein memory is not configurable. Turn
        # logging off in order to avoid errors like
        # "skein.exceptions.DriverError: Received message larger than
        # max (6810095 vs. 4194304)".
        'skein_app_log_collection_enabled': False
    }

    application_args = [
        "--mediawiki_snapshot", mediawiki_snapshot,
        "--wikidata_snapshot", wikidata_snapshot,
        "--start_date", var_props.get('article_quality_start_date', "20200101"),
        "--end_date", var_props.get('article_quality_end_date', "20200131"),
        "--mode", "production",
        "--save_directory", var_props.get('hdfs_temp_directory', dag_config.hdfs_temp_directory)
    ]
    if wiki_list:
        application_args.extend(["--projects", ','.join(wiki_list)])

    compute_article_quality_scores = SparkSubmitOperator.for_virtualenv(
        **params,
        task_id='compute_article_quality_scores',
        virtualenv_archive = article_quality_conda_url,
        entry_point = "bin/article_quality_app.py",
        application_args=application_args
    )

    [
        wait_for_mediawiki_project_namespace_map_snapshot,
        wait_for_mediawiki_page_snapshot,
        wait_for_mediawiki_wikitext_history_snapshot,
        wait_for_wikidata_item_page_link_snapshot
    ] >> \
    compute_article_quality_scores


