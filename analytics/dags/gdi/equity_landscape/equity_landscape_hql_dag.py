"""
# GDI Equity Landscape DAG - HQL

* This DAG is used to generate the equity landscape input metrics using the hql files stored on HDFS.
* The input is various input metrics from the APIs and the CSV files stored on HDFS.
## Inputs

* editorship
* readership

## Outputs (Hive)
* gdi.geoeditor_input_metrics
* gdi.geoeditor_input_metrics_pivot
* gdi.geoeditor_online_input_metrics
* gdi.georeadership_input_metrics
"""

from typing import List, Tuple
from datetime import datetime
from dataclasses import dataclass
from pathlib import Path

from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.utils.task_group import TaskGroup

from analytics.config import dag_config

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator


dag_id = "gdi_equity_landscape_hql"
var_props = VariableProperties(f"{dag_id}_config")

a_year_ago = var_props.get("a_year_ago", 2021)
two_years_ago = var_props.get("two_years_ago", 2020)


@dataclass
class HqlTaskProps:
    hql_path: str
    pos: int
    query_parameters: dict
    years: list = None

    def __post_init__(self):
        self.task_name = Path(self.hql_path).stem + "_" + str(self.pos)

        if (
            "year" in self.query_parameters
            and self.query_parameters["year"] is not None
        ):
            if (
                self.query_parameters["year"] == "-"
                or self.query_parameters["year"] == "-1"
            ):
                self.years = [a_year_ago]
            elif (
                "-" in self.query_parameters["year"]
                and str(self.query_parameters["year"]).lstrip("-").isdigit()
            ):
                self.years = [a_year_ago + int(self.query_parameters["year"])]

        if self.years == []:
            self.years = [two_years_ago, a_year_ago]


def _group_sparksql_operators(lst_hql: List[HqlTaskProps]) -> None:
    """
    Processes the HQL files and passes the relevant arguments and macros to _create_sparksql_operators to create  SparkSQL operators

    :param lst_hql:

    :return: None
    """
    lst_tasks = []
    index_tasks = 0
    for hql_props in lst_hql:
        if hql_props.years is not None:
            lst_tasks, index_tasks = _create_sparksql_operators(
                lst_tasks, index_tasks, hql_props
            )


def _create_sparksql_operators(lst_tasks, index_tasks, hql_props) -> Tuple[List, int]:
    """
    Helper method to create spark sql operators for the given hql properties.

    Handles the case where the hql properties have multiple years and creates a spark sql operator for each year.

    :param lst_tasks:
    :param index_tasks:
    :param_hql_props:

    :return: Tuple[List, int]
    """
    for index, year in enumerate(hql_props.years):
        new_task_name = f"{hql_props.task_name}_{index}"
        hql_props.query_parameters["year"] = year
        lst_tasks.append(
            SparkSqlOperator(
                task_id=new_task_name,
                sql=hql_props.hql_path,
                query_parameters=hql_props.query_parameters,
            )
        )

        if index_tasks > 0:
            lst_tasks[index_tasks - 1] >> lst_tasks[index_tasks]

        index_tasks += 1

    return lst_tasks, index_tasks


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 10, 1)),
    schedule_interval=None,
    default_args=var_props.get_merged("default_args", dag_config.default_args),
) as dag:

    editorship_config = var_props.get("editorship_config", [])

    editorship_tasks = [HqlTaskProps(**c) for c in editorship_config]

    editorship_tasks.sort(key=lambda x: x.pos)

    readership_config = var_props.get("readership_config", [])

    readership_tasks = [HqlTaskProps(**c) for c in readership_config]

    readership_tasks.sort(key=lambda x: x.pos)

    grants_leadership_config = var_props.get("grants_leadership_config", [])

    grants_leadership_tasks = [HqlTaskProps(**c) for c in grants_leadership_config]

    grants_leadership_tasks.sort(key=lambda x: x.pos)

    population_config = var_props.get("population_config", [])

    population_tasks = [HqlTaskProps(**c) for c in population_config]

    population_tasks.sort(key=lambda x: x.pos)

    access_config = var_props.get("access_config", [])

    access_tasks = [HqlTaskProps(**c) for c in access_config]

    access_tasks.sort(key=lambda x: x.pos)

    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    with TaskGroup(group_id="geometrics") as geometrics:

        with TaskGroup(group_id="editorship") as editorship:
            _group_sparksql_operators(editorship_tasks)

        with TaskGroup(group_id="readership") as readership:
            _group_sparksql_operators(readership_tasks)

    with TaskGroup(group_id="grants_leadership") as grants_leadership:
        _group_sparksql_operators(grants_leadership_tasks)

    with TaskGroup(group_id="population") as population:
        _group_sparksql_operators(population_tasks)

    with TaskGroup(group_id="access") as access:
        _group_sparksql_operators(access_tasks)


    (start >> geometrics >> grants_leadership >> population >> access >> end)
