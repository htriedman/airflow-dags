"""
Process to collect discrepancies identified by the rdf-streaming-updater producer (flink app)
and schedule reconciliation events that are fed back to this same flink application.
This process assumes that an independent flink pipeline runs per DC and thus might
have encountered different discrepancies.
Since reconciliation events are shipped back via event-gate we do not entirely control
in which DC they might produced to. This means that reconciliation events for the flink
pipeline running in eqiad might be collected in a topic prefixed with "codfw".
In order for the flink pipeline to filter the reconciliation events that are related
to the discrepancies it found a source_tag is used (via --reconciliation-source in this spark
job).
This is also the reason unlike other jobs consuming MEP topics why this spark job is scheduled
on a per DC basis.
"""
from collections import namedtuple
from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy import DummyOperator

from search.config.dag_config import wdqs_spark_tools, YMDH_PARTITION, eventgate_datacenters, get_default_args
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import NamedHivePartitionSensor

dag_conf = VariableProperties('rdf_streaming_updater_reconcile_conf')

lapsed_actions_table = dag_conf.get('lapsed_actions_table', 'event.rdf_streaming_updater_lapsed_action')
fetch_failures_table = dag_conf.get('fetch_failures_table', 'event.rdf_streaming_updater_fetch_failure')
state_inconsistencies_table = dag_conf.get('state_inconsistencies_table',
                                           'event.rdf_streaming_updater_state_inconsistency')

wdqs_source_tag_prefix = dag_conf.get('wdqs_source_tag_prefix', 'wdqs_sideoutputs_reconcile')
wcqs_source_tag_prefix = dag_conf.get('wcqs_source_tag_prefix', 'wcqs_sideoutputs_reconcile')

wdqs_events_domain = dag_conf.get('wdqs_events_domain', 'www.wikidata.org')
wcqs_events_domain = dag_conf.get('wcqs_events_domain', 'commons.wikimedia.org')

wdqs_entity_namespaces = dag_conf.get('wdqs_entity_namespaces', '0,120,146')
wcqs_entity_namespaces = dag_conf.get('wcqs_entity_namespaces', '6')

wdqs_initial_to_namespace_map = dag_conf.get('wdqs_initial_to_namespace_map', 'Q=,P=Property,L=Lexeme')
wcqs_initial_to_namespace_map = dag_conf.get('wcqs_initial_to_namespace_map', 'M=File')

eventgate_endpoint = dag_conf.get('eventgate_endpoint', 'https://eventgate-main.discovery.wmnet:4492/v1/events')
http_routes_conf = dag_conf.get('http_routes',
                                'www.wikidata.org=https://api-ro.discovery.wmnet,'
                                'commons.wikimedia.org=https://api-ro.discovery.wmnet')

reconciliation_stream = dag_conf.get('reconciliation_stream', 'rdf-streaming-updater.reconcile')

SideOutputs = namedtuple('SideOutputs',
                         ['dc', 'lapsed_actions', 'fetch_failures', 'state_inconsistencies'])

sensors_sla = timedelta(hours=6)


def wait_for_side_output(name: str, partition: str) -> NamedHivePartitionSensor:
    return NamedHivePartitionSensor(
        task_id='wait_for_' + name,
        mode='reschedule',
        sla=sensors_sla,
        partition_names=[partition])


def wait_for_side_output_data(parts: SideOutputs):
    return [
        wait_for_side_output(name=f"lapsed_actions_{parts.dc}", partition=parts.lapsed_actions),
        wait_for_side_output(name=f"fetch_failures_{parts.dc}", partition=parts.fetch_failures),
        wait_for_side_output(name=f"state_inconsistencies_{parts.dc}", partition=parts.state_inconsistencies),
    ]


def submit_reconciliation(name: str,
                          domain: str,
                          source_tag: str,
                          entity_namespaces: str,
                          initials_to_namespace: str,
                          http_routes: str,
                          sideoutputs: SideOutputs) -> SparkSubmitOperator:
    return SparkSubmitOperator(
        task_id=name,
        application=wdqs_spark_tools,
        java_class="org.wikidata.query.rdf.updater.reconcile.UpdaterReconcile",
        max_executors=25,
        executor_cores=8,
        executor_memory="16g",
        driver_memory="4g",
        application_args=[
            "--domain", domain,
            "--reconciliation-source", source_tag,
            "--event-gate", eventgate_endpoint,
            "--late-events-partition", sideoutputs.lapsed_actions,
            "--failures-partition", sideoutputs.fetch_failures,
            "--inconsistencies-partition", sideoutputs.state_inconsistencies,
            "--stream", reconciliation_stream,
            "--entity-namespaces", entity_namespaces,
            "--initial-to-namespace", initials_to_namespace,
            "--http-routes", http_routes
        ],
    )


def get_sideoutputs_partitions(dc: str) -> SideOutputs:
    return SideOutputs(
        dc=dc,
        lapsed_actions=f"{lapsed_actions_table}/datacenter={dc}/{YMDH_PARTITION}",
        fetch_failures=f"{fetch_failures_table}/datacenter={dc}/{YMDH_PARTITION}",
        state_inconsistencies=f"{state_inconsistencies_table}/datacenter={dc}/{YMDH_PARTITION}")


def build_dag(dag_id: str,
              start_date: datetime,
              source_tag: str,
              domain: str,
              entity_namespaces: str,
              initial_to_namespace_map: str) -> DAG:
    merged_default_args = dag_conf.get_merged('default_args', {
        **get_default_args(),
        'start_date': start_date,
        # This job is not technically speaking dependent on past, but we increase our
        # chances of making the reconciliations happier by processing them in order
        'depends_on_past': True,
    })

    with DAG(
            dag_id=dag_id,
            default_args=merged_default_args,
            schedule_interval='@hourly',
            # We want hourly runs to be scheduled one ofter the other
            max_active_runs=1,
            catchup=True
    ) as streaming_updater_reconcile:
        complete = DummyOperator(task_id='complete')
        for dc in eventgate_datacenters:
            # work on a per DC basis because the side-output topics are populated by the flink app
            # running in this DC, e.g. eqiad.rdf-streaming-updater.fetch-failures is populated by
            # flink@eqiad # We could think about adding a field to discriminate a particular flink
            # app but here it's identified using the datacenter (topic prefix) and meta.domain.
            sideoutputs_parts = get_sideoutputs_partitions(dc)
            wait_for_data = wait_for_side_output_data(sideoutputs_parts)
            job = submit_reconciliation(
                name=f'reconcile_{dc}',
                domain=domain,
                source_tag=f"{source_tag}@{dc}",
                entity_namespaces=entity_namespaces,
                initials_to_namespace=initial_to_namespace_map,
                http_routes=http_routes_conf,
                sideoutputs=sideoutputs_parts
            )
            wait_for_data >> job >> complete

    return streaming_updater_reconcile


wdqs_reconcile_dag = build_dag(dag_id='wdqs_streaming_updater_reconcile_hourly',
                               start_date=datetime(2022, 3, 22, 12, 00, 00),
                               source_tag=wdqs_source_tag_prefix,
                               domain=wdqs_events_domain,
                               entity_namespaces=wdqs_entity_namespaces,
                               initial_to_namespace_map=wdqs_initial_to_namespace_map)

wcqs_reconcile_dag = build_dag(dag_id='wcqs_streaming_updater_reconcile_hourly',
                               start_date=datetime(2022, 3, 22, 12, 00, 00),
                               source_tag=wcqs_source_tag_prefix,
                               domain=wcqs_events_domain,
                               entity_namespaces=wcqs_entity_namespaces,
                               initial_to_namespace_map=wcqs_initial_to_namespace_map)
