from typing import Any
from airflow.operators.email import EmailOperator
from airflow.utils.helpers import parse_template_string
from wmf_airflow_common.util import hdfs_client


class HdfsEmailOperator(EmailOperator):
    """
    Sends an email.
    Allows to embed a file from HDFS at the end of the email body.
    """

    def __init__(
        self,
        to: str,
        subject: str,
        html_content: str,
        embedded_file: str,
        hadoop_name_node: str,
        **kwargs: Any,
    ):
        """
        Initializes an HdfsEmailOperator (at DAG interpretation time).
        """

        # Initialize the parent EmailOperator.
        super().__init__(
            to=to,
            subject=subject,
            html_content=html_content,
            **kwargs
        )

        # File will be embedded in execute method (DAG execution time).
        self.embedded_file = embedded_file
        self.hadoop_name_node = hadoop_name_node

    def execute(self, context: Any) -> None:
        """
        Embeds the HDFS file to the end of the file.
        Then sends the email.
        """

        # Render the potentially templated file path.
        _, jinja = parse_template_string(self.embedded_file)
        rendered_path = jinja.render(**context)

        # Create an HDFS file system handle.
        hdfs = hdfs_client(self.hadoop_name_node)

        # Read the file contents and add them to the email body.
        with hdfs.open_input_file(rendered_path) as file:
            embedded_content = file.read().decode('ascii')
        self.html_content += embedded_content.replace('\n', '<br/>')

        # Let the parent EmailOperator send the email.
        super().execute(context)
