"""
Loads wmf.edit_hourly from Hive to Druid at a monthly schedule.

At every DAG run, this job reads the entire edit_hourly table for the given
snapshot and loads it to Druid overriding any data that could be there.

The segment granularity is monthly, to allow for better compaction, given that
we load the full history every time. And the query granularity is hourly.
Depending on performance when adding new fields, we might switch to daily.
"""

from airflow import DAG
from analytics.config.dag_config import artifact, dataset, default_args, druid_default_conf
from datetime import datetime, timedelta
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.druid import HiveToDruidOperator
from wmf_airflow_common.templates.time_filters import filters


dag_id = "druid_load_edit_hourly"
var_props = VariableProperties(f"{dag_id}_config")

hive_to_druid_config = {
    "database": "wmf",
    "table": "edit_hourly",
    "druid_datasource": var_props.get("druid_datasource", "edits_hourly"),
    "timestamp_column": "ts",
    # The following parameter is enclosed in single quotes because Skein
    # uses the command line to call spark_submit and passes the parameter
    # as --timestamp_format <parameter value>. If not quoted, the command
    # line interprets it only partially, because it has a space.
    # TODO: Handle this problem from a more appropriate place, i.e. Skein.
    "timestamp_format": var_props.get("timestamp_format", "'yyyy-MM-dd HH:mm:ss.S'"),
    "query_granularity": "hour",
    "segment_granularity": "month",
    "num_shards": 1,
    "map_memory": "4096",
    "reduce_memory": "8192",
    "hadoop_queue": var_props.get("hadoop_queue", "production"),
    "hive_to_druid_jar": var_props.get("hive_to_druid_jar",
        artifact("refinery-job-0.2.11-shaded.jar")),
    "temp_directory": var_props.get("temp_directory", None),  # Override just for testing.
    "dimensions": [
        "project",
        "project_family",
        "language",
        "user_is_anonymous",
        "user_is_bot",
        "user_is_administrator",
        "user_groups",
        "user_tenure_bucket",
        "namespace_is_content",
        "namespace_is_talk",
        "namespace_name",
        "creates_new_page",
        "is_deleted",
        "is_reverted",
        "is_redirect_currently",
        "user_edit_count_bucket",
        "platform",
        "interface",
        "revision_tags",
    ],
    "metrics": [
        "edit_count",
        "text_bytes_diff",
    ],
}


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2023, 1, 1)),
    schedule_interval="@monthly",
    tags=["druid_load"],
    user_defined_filters=filters,
    default_args={
        **var_props.get_merged("default_args", default_args),
        **druid_default_conf,
        "sla": timedelta(days=10),
    },
) as dag:

    sensor = dataset("hive_wmf_edit_hourly").get_sensor_for(dag)

    loader = HiveToDruidOperator(
        task_id="load_wmf_edit_hourly_to_druid",
        since="{{execution_date | to_ds_hour}}",
        until="{{execution_date | add_months(1) | to_ds_hour}}",
        executor_cores=4,
        executor_memory="8G",
        **hive_to_druid_config,
    )

    sensor >> loader
