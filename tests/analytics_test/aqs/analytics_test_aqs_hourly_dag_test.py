import pytest
import unittest.mock as mock


@pytest.fixture(autouse=True)
def run_around_tests():
    json_var = '{"start_date": "2022-02-22"}'
    with mock.patch.dict('os.environ', AIRFLOW_VAR_AQS_HOURLY_CONFIG=json_var):
        yield

# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics_test', 'dags', 'aqs', 'aqs_hourly_dag.py']

def test_aqs_hourly_dag_loaded_in_analytics_test(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="aqs_hourly")
    assert dag is not None
    assert dag.start_date.strftime("%Y-%m-%d") == "2022-02-22"
    assert len(dag.tasks) == 2
