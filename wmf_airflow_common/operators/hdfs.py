from typing import Any
import textwrap
from wmf_airflow_common.operators.skein import SimpleSkeinOperator
from analytics.config.dag_config import artifact


class HDFSArchiveOperator(SimpleSkeinOperator):
    """An archiver Operator which, on HDFS:
    - checks for unicity of the file in source,
    - checks for the presence of a done file,
    - creates an archive directory with specific umask
    - moves the source file to the archive file
    - deletes the source directory
    """

    # Need to ignore mypy incompatible type assignment warning here.
    # PythonOperator defines this as a 3 tuple, but we want to override it.
    template_fields = SimpleSkeinOperator.template_fields + (  # type: ignore
        'source_directory',
        'archive_file',
        'hdfsarchiver_job_shaded_jar_path',)

    def __init__(
        self,
        source_directory: str,
        archive_file: str,
        hdfsarchiver_job_shaded_jar_path: str = artifact('hdfs-tools-0.0.6-shaded.jar'),
        archive_parent_umask: str = "022",
        archive_perms: str = "644",
        expected_filename_ending: str = ".gz",
        check_done: bool = False,
        done_file: str = "_SUCCESS",
        **kwargs: Any
    ):    
        """
        Creates an HDFS Archive Operator.

        :param hdfsarchiver_job_shaded_jar_path: The path to the hdfs archiver job-shaded jar. The
        default job jar is located as a artifact in the artifacts.yaml file.
        :param source_directory: Full hdfs path.
        :param archive_file: Full hdfs path.
        :param archive_parent_umask: The umask to create the archive (octal).
        :param archive_perms: The permission to create the archive (octal).
        :param expected_filename_ending: The end of the file name in the source directory.
        :param check_done: Is there an empty file to flag the source file as done.
        :param done_file: The name of the flag file.
        :param kwargs:
        """
        self.source_directory = source_directory
        self.archive_file = archive_file
        self.hdfsarchiver_job_shaded_jar_path = hdfsarchiver_job_shaded_jar_path

        java_classpath = 'hdfsarchiver_job_shaded_jar:' +\
                         '$(hadoop classpath)'
        
        jar_files = {
            'hdfsarchiver_job_shaded_jar': self.hdfsarchiver_job_shaded_jar_path
        }

        skein_script = f"""
                java -cp {java_classpath}
                org.wikimedia.analytics.hdfstools.HDFSArchiver
                --source_directory "{self.source_directory}"
                --archive_file "{self.archive_file}"
                --archive_parent_umask {archive_parent_umask}
                --archive_perms {archive_perms}
                --expected_filename_ending "{expected_filename_ending}"
                --check_done {str(check_done).lower()}
                --done_file "{done_file}"
            """

        skein_script = textwrap.dedent(skein_script).strip().replace('\n', ' ')
        super().__init__(script=skein_script, files=jar_files, **kwargs)
