'''
# This job waits for the presence of all partitions in the last 7 days
# from the webrequest table then computes session related metrics for app pageviews

#The job computes the following sessions metrics;
- Number of sessions per user
- Number of pageviews per session
- Session length (gap between first and last pageview, in milliseconds)

# The resulting output metrics are stored;
# in output_directory/session_metrics.tsv
# and also exposed through a hive external table at wmf.mobile_apps_session_metrics

'''

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, artifact, hadoop_name_node

dag_id = 'mobile_app_session_metrics_weekly'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 5)),
    schedule_interval='@weekly',
    tags=['spark', 'mobile_apps'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=7),
        }
    ),
    user_defined_filters=filters
) as dag:

    sensor = RangeHivePartitionSensor(
        task_id='wait_for_webrequest_partitions',
        table_name='wmf.webrequest',
        from_timestamp='{{execution_date}}',
        to_timestamp='{{execution_date | start_of_next_week}}',
        granularity='@hourly',
        pre_partitions=['webrequest_source=text']
    )

    etl = SparkSubmitOperator(
        task_id='compute_app_session_metrics_from_webrequest',
        application=var_props.get('refinery_job_jar',
            artifact('refinery-job-0.2.1-shaded.jar')),
        java_class='org.wikimedia.analytics.refinery.job.AppSessionMetrics',
        # This job generates very big YARN logs. When the logs reach a
        # certain size, the gRPC library used by the skein client breaks.
        # Prevent skein collecting them to avoid issues.
        skein_app_log_collection_enabled=False,
        application_args={
            '--output-dir': var_props.get(
                'output-dir', f'{hadoop_name_node}/wmf/data/wmf/mobile_apps/session_metrics_by_os'
            ),
            '--year': '{{ execution_date.year }}',
            '--month':'{{ execution_date.month }}',
            '--day': '{{ execution_date.day }}',
            '--period-days': 7,
            '--split-by-os':'true',
            '--webrequest-base-path': f'{hadoop_name_node}/wmf/data/wmf/webrequest',
            '--num-partitions': 64
        },
        conf={'spark.dynamicAllocation.maxExecutors': 32}
    )

    sensor >> etl
