import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'mediawiki', 'mediawiki_history_dumps_dag.py']


def test_mediawiki_history_dumps_dag_loaded(airflow, dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="mediawiki_history_dumps")
    assert dag is not None
    assert len(dag.tasks) == 2