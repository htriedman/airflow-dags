from typing import List, Optional
from os import path
from workflow_utils.artifact import Artifact
from wmf_airflow_common import config as wmf_airflow_common_config


class ArtifactRegistry:
    """
    An instance of ArtifactRegistry loads artifact config files
    that wraps the workflow_utils Artifacts in a simple API
    that allows lookup of cached URLs only.  This is usd
    for syntactic sugar when referencing artifacts from DAGs,
    so that users don't have to think to hard about the actual
    URLs where their artifacts have been synced.
    """

    def __init__(self, artifact_config_files: List[str]):
        """
        :param artifact_config_files:
            List of artifact .yaml config files to load.
        """
        self._artifacts = Artifact.load_artifacts_from_config(artifact_config_files)

    def artifact(self, artifact_name: str) -> Artifact:
        """
        Gets the registered Artifact by artifact_name.
        Throws KeyError is artifact_name is not registered in config files.
        """
        if artifact_name not in self._artifacts:
            raise KeyError(f'Artifact name {artifact_name} not declared in configuration.')
        return self._artifacts[artifact_name]

    def artifact_url(self, artifact_name: str) -> str:
        """
        Gets the first expected cached url for the artifact name.
        The artifact is not checked for existence at this url.

        :param artifact_name:
        """
        return str(self.artifact(artifact_name).cached_url())

    @classmethod
    def for_wmf_airflow_instance(
        cls,
        airflow_instance_name: str,
        other_files: Optional[List[str]] = None
    ) -> 'ArtifactRegistry':
        """
        Uses WMF airflow instance conventions in the
        data-enginering/airflow-dags repository to
        automate loading of artifact config files for a
        airflow instance.

        These are:
        - wmf_airflow_common/config/artifact_config.yaml
        - <instance_name>/config/artifacts.yaml

        NOTE: This function uses relative paths to this python file,
        so if you move this file, make sure you adjust the paths accordingly.

        :param airflow_instance_name:
            Name of airflow instance.   This should
            match a top level directory in this airflow-dags repository.

        :param other_files:
            Other artifact config files to load aside from the
            airflow-dags instance convention defaults.
        """
        if other_files is None:
            other_files = []

        airflow_dags_repo_root = path.dirname(path.dirname(__file__))
        instance_dir = path.join(airflow_dags_repo_root, airflow_instance_name)

        possible_artifact_config_files = [
            path.join(path.dirname(wmf_airflow_common_config.__file__), 'artifact_config.yaml'),
            path.join(instance_dir, 'config', 'artifacts.yaml'),
        ] + other_files

        artifact_config_files = [
            config_file for config_file in possible_artifact_config_files
            if path.exists(config_file)
        ]

        return cls(artifact_config_files)
