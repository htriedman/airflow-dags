from airflow.utils.decorators import apply_defaults
from airflow.utils.log.logging_mixin import LoggingMixin
from dataclasses import dataclass
import json
import os
from pyarrow.fs import FileSystem
from search.shared.utils import parse_memory_to_mb
from typing import Any, Mapping, Optional, Tuple
from wmf_airflow_common.operators.spark import SparkSubmitOperator


def get_via_dotted_path(data: Mapping, path: str) -> Any:
    for piece in path.split('.'):
        data = data[piece]
    return data


@dataclass
class AutoSizeForMatrix:
    template_fields = ('metadata_uri',)

    metadata_uri: str
    bytes_per_value: int
    num_obs_path: str
    features_path: str

    def read_metadata(self) -> Mapping:
        stream = os.popen('hdfs classpath --glob')
        os.environ['CLASSPATH'] = stream.read().strip()

        fs, path = FileSystem.from_uri(self.metadata_uri)
        with fs.open_input_stream(path) as stream:
            metadata = json.loads(stream.readall().decode('utf8'))
        if isinstance(metadata, dict):
            return metadata
        raise ValueError(
            f'Expected to read json dict from [{self.metadata_uri}]'
            ' but found [{type(metadata)}]')

    def extract_dimensions(self, metadata: Mapping) -> Tuple[(int, int)]:
        return (
            get_via_dotted_path(metadata, self.num_obs_path),
            len(get_via_dotted_path(metadata, self.features_path))
        )

    def calc_overhead(self, dims: Tuple[int, int]) -> int:
        num_obs, num_features = dims
        # +1 is for the label
        bytes_estimate = num_obs * (num_features + 1) * self.bytes_per_value
        return int(bytes_estimate / 2**20)


class AutoSizeSpark(LoggingMixin):
    def __init__(
        self,
        agg_memory_limit: str,
        agg_cores_limit: int,
        matrix: Optional[AutoSizeForMatrix],
    ):
        self._agg_memory_limit_mb = parse_memory_to_mb(agg_memory_limit)
        self._agg_cores_limit = agg_cores_limit
        self._matrix = matrix

    def _calc_matrix_overhead_mb(self) -> int:
        """Heuristic to decide the amount of memory overhead necessary"""
        if not self._matrix:
            return 0
        dims = self._matrix.extract_dimensions(self._matrix.read_metadata())
        overhead = self._matrix.calc_overhead(dims)
        self.log.info(f'Decided on memory overhead of [{overhead}] mb for dims [{dims}]')
        return overhead

    def _calc_max_executors(self, op: SparkSubmitOperator, memory_overhead_mb: int) -> int:
        """Determine the the maximum number of executors within provided limits"""
        total_executor_memory_mb = parse_memory_to_mb(op._executor_memory) + \
                memory_overhead_mb

        by_memory = int(self._agg_memory_limit_mb // total_executor_memory_mb)
        by_cores = int(self._agg_cores_limit // int(op._executor_cores))
        max_executors = min(by_memory, by_cores)
        self.log.info(f'Decided max executors of [{max_executors}]')
        return max_executors


    def apply(self, op: SparkSubmitOperator) -> None:
        # replace dict to ensure we don't change a dict that might be used externally
        op._conf = dict(op._conf)

        if 'spark.executor.memoryOverhead' in op._conf:
            overhead = parse_memory_to_mb(op._conf['spark.executor.memoryOverhead'])
        else:
            # this should be sparks default overhead, but lets be explicit so that
            # future calculations have a value to work with.
            executor_memory = parse_memory_to_mb(op._executor_memory)
            overhead = int(max(executor_memory * 0.1, 384))
        overhead += self._calc_matrix_overhead_mb()

        op._conf['spark.executor.memoryOverhead'] = f'{overhead}m'
        op._conf['spark.dynamicAllocation.maxExecutors'] = self._calc_max_executors(op, overhead)


class AutoSizeSparkSubmitOperator(SparkSubmitOperator):
    """Run spark/mjolnir scripts

    Not the cleanest implementation, but tries to keep repetative
    declarations out of the definitions.
    """
    template_fields = SparkSubmitOperator.template_fields + ('_autosize_for_matrix',)

    @apply_defaults
    def __init__(
        self,
        agg_memory_limit: str,
        agg_cores_limit: int,
        autosize_for_matrix: Optional[AutoSizeForMatrix] = None,
        **kwargs: Any
    ):
        super().__init__(**kwargs)
        self._agg_memory_limit = agg_memory_limit
        self._agg_cores_limit = agg_cores_limit
        self._autosize_for_matrix = autosize_for_matrix

    def _get_auto_sizer(self) -> AutoSizeSpark:
        return AutoSizeSpark(
            self._agg_memory_limit,
            self._agg_cores_limit,
            self._autosize_for_matrix)

    def execute(self, context: Mapping) -> None:
        self._get_auto_sizer().apply(self)
        super().execute(context)
