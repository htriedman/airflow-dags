# pylint: disable=protected-access

import pytest

from skein import (
    File as SkeinFile,
    FileType as SkeinFileType
)
from skein.model import ApplicationSpec as SkeinApplicationSpec

from wmf_airflow_common.hooks import skein


@pytest.mark.parametrize(['source', 'as_dict', 'file_properties','expected'], [
    pytest.param(
        '/local/path/to/file.txt', False, {},
        ('file.txt', SkeinFile(source='/local/path/to/file.txt')),
        id='alias should be basename'
    ),
    pytest.param(
        '/local/path/to/file.txt#alias.txt', False, {},
        ('alias.txt', SkeinFile(source='/local/path/to/file.txt')),
        id='alias should be after #'
    ),
    pytest.param(
        '/local/path/to/file.tgz#alias.tgz', False, {},
        ('alias.tgz', SkeinFile(source='/local/path/to/file.tgz')),
        id='alias after #, type should be inferred as archive'
    ),
    pytest.param(
        '/local/path/to/file.tgz#alias.tgz', False, {'type': SkeinFileType.FILE},
        ('alias.tgz', SkeinFile(source='/local/path/to/file.tgz', type=SkeinFileType.FILE)),
        id='alias after #, type should be explicitly set to file'
    ),
    pytest.param(
        '/local/path/to/file.tgz#alias.tgz', True, {'type': 'file'},
        (
            'alias.tgz',
            SkeinFile(source='/local/path/to/file.tgz', type=SkeinFileType.FILE).to_dict()
        ),
        id='alias after #, as_dict=True'
    ),
])
def test_parse_file_source(source, as_dict, file_properties, expected):
    assert skein.parse_file_source(
        source,
        as_dict=as_dict,
        **file_properties
    ) == expected


@pytest.mark.parametrize(['app_ids', 'app_owner', 'sudo', 'expected'], [
    (
        ['ABC'], None, False,
        ['yarn logs -applicationId ABC'],
    ),
    (
        ['ABC', '123'], None, False,
        [
            'yarn logs -applicationId ABC',
            'yarn logs -applicationId 123'
        ],
    ),
    (
        ['ABC', '123'], 'app_user', False,
        [
            'yarn logs -appOwner app_user -applicationId ABC',
            'yarn logs -appOwner app_user -applicationId 123'
        ],
    ),
    (
        ['ABC', '123'], 'app_user', True,
        [
            'sudo -u app_user yarn logs -appOwner app_user -applicationId ABC',
            'sudo -u app_user yarn logs -appOwner app_user -applicationId 123'
        ],
    ),
])
def test_yarn_logs_commands(app_ids, app_owner, sudo, expected):
    assert skein.yarn_logs_commands(app_ids, app_owner, sudo) == expected


def test_find_yarn_app_ids():
    logs = """
Container: container_e33_1637058075222_419446_01_000001 on an-worker1109.eqiad.wmnet_8041_1643316031119
LogAggregationType: AGGREGATED
=======================================================================================================
22/01/27 20:40:17 INFO Client: Application report for application_1637058075222_419447 (state: ACCEPTED)
22/01/27 20:40:17 INFO Client:
	 client token: Token { kind: YARN_CLIENT_TOKEN, service:  }
	 diagnostics: AM container is launched, waiting for AM container to Register with RM
	 ApplicationMaster host: N/A
	 ApplicationMaster RPC port: -1
	 queue: default
	 start time: 1643316016172
	 final status: UNDEFINED
	 tracking URL: http://an-master1001.eqiad.wmnet:8088/proxy/application_1637058075222_419447/
	 user: otto
22/01/27 20:40:14 INFO Client: Source and destination file systems are the same. Not copying hdfs:/user/spark/share/lib/spark-2.4.4-assembly.zip
22/01/27 20:40:15 INFO Client: Uploading resource file:/var/lib/hadoop/data/f/yarn/local/usercache/otto/appcache/application_1637058075222_419446/spark-2ccec297-e3ff-4865-830c-4bf76e941f4d/__spark_conf__3185890315398158687.zip -> hdfs://analytics-hadoop/user/otto/.sparkStaging/application_1637058075222_419447/__spark_conf__.zip
22/01/27 20:40:18 INFO Client: Application report for application_1637058075222_419447 (state: ACCEPTED)
22/01/27 20:40:19 INFO Client: Application report for application_1637058075222_419447 (state: ACCEPTED)
22/01/27 20:40:20 INFO Client: Application report for application_1637058075222_419447 (state: RUNNING)
22/01/27 20:40:20 INFO Client:
	 client token: Token { kind: YARN_CLIENT_TOKEN, service:  }
	 diagnostics: N/A
	 ApplicationMaster host: 10.64.36.9
	 ApplicationMaster RPC port: -1
	 queue: default
	 start time: 1643316016172
	 final status: UNDEFINED
	 tracking URL: http://an-master1001.eqiad.wmnet:8088/proxy/application_1637058075222_419447/
	 user: otto
22/01/27 20:40:20 INFO YarnClientSchedulerBackend: Application application_1637058075222_419447 has started running.

Container: container_e33_1637058075222_419446_01_000001 on an-worker1109.eqiad.wmnet_8041_1643316031119
    """  # noqa

    expected = ['application_1637058075222_419446', 'application_1637058075222_419447']
    assert skein.find_yarn_app_ids(logs) == expected


@pytest.mark.parametrize(['builder_spec', 'expected_app_spec', 'expected_client_kwargs'], [
    pytest.param(
        {
            'script': 'echo hi',
        },
        SkeinApplicationSpec.from_dict({
            'master': {
                'script': 'echo hi',
                'resources': {
                    'memory': skein.SkeinHookBuilder._default_memory,
                    'vcores': skein.SkeinHookBuilder._default_vcores,
                }
            }
        }),
        {},
        id='only set script'
    ),

    pytest.param(
        {
            'name': 'say hi',
            'script': 'echo hi',
            'queue': 'myqueue',
            'env': {
                'env1': 'val1',
            },
            'client_log_level': 'DEBUG',
            'master_log_level': 'DEBUG',
            'principal': 'user@domain',
            'keytab': '/pat/to/keytab',
            'resources': {
                'memory': 2048,
                'vcores': 2,
            }
        },
        SkeinApplicationSpec.from_dict({
            'name': 'say hi',
            'queue': 'myqueue',
            'master': {
                'script': 'echo hi',
                'env': {
                    'env1': 'val1',
                },
                'log_level': 'DEBUG',
                'resources': {
                    'memory': 2048,
                    'vcores': 2,
                }
            },
        }),
        {
            'log_level': 'DEBUG',
            'principal': 'user@domain',
            'keytab': '/pat/to/keytab',
        },
        id='set all available properties execpt files'
    ),

    pytest.param(
        {
            'script': 'echo hi',
            # test with files as string
            'files': '/path/to/file1.txt,/path/to/file2.tgz',
        },
        SkeinApplicationSpec.from_dict({
            'master': {
                'script': 'echo hi',
                'resources': {
                    'memory': skein.SkeinHookBuilder._default_memory,
                    'vcores': skein.SkeinHookBuilder._default_vcores,
                },
                'files': {'file1.txt': '/path/to/file1.txt', 'file2.tgz': '/path/to/file2.tgz'}
            }
        }),
        {},
        id='set files as string'
    ),

    pytest.param(
        {
            'script': 'echo hi',
            # test with files as list
            'files': ['/path/to/file1.txt', '/path/to/file2.tgz'],
        },
        SkeinApplicationSpec.from_dict({
            'master': {
                'script': 'echo hi',
                'resources': {
                    'memory': skein.SkeinHookBuilder._default_memory,
                    'vcores': skein.SkeinHookBuilder._default_vcores,
                },
                'files': {'file1.txt': '/path/to/file1.txt', 'file2.tgz': '/path/to/file2.tgz'}
            }
        }),
        {},
        id='set files as list'
    ),

    pytest.param(
        {
            'script': 'echo hi',
            # test with files as dict File spec
            'files': {
                'file1.txt': {
                    'source': '/path/to/file1.txt',
                },
                'file2.tgz': {
                    'source': '/path/to/file2.tgz'
                },
            }
        },
        SkeinApplicationSpec.from_dict({
            'master': {
                'script': 'echo hi',
                'resources': {
                    'memory': skein.SkeinHookBuilder._default_memory,
                    'vcores': skein.SkeinHookBuilder._default_vcores,
                },
                'files': {'file1.txt': '/path/to/file1.txt', 'file2.tgz': '/path/to/file2.tgz'}
            }
        }),
        {},
        id='set files as dict'
    ),
])
def test_skein_hook_builder_methods(builder_spec, expected_app_spec, expected_client_kwargs):
    builder = skein.SkeinHookBuilder()

    # builder_spec is a mapping from builder method name to value to pass.
    # We're just doing this to make it easy to parameterize this test.
    for method_name, val in builder_spec.items():
        method = getattr(builder, method_name)
        method(val)

    hook = builder.build()
    assert hook._application_spec == expected_app_spec
    assert hook._client_kwargs == expected_client_kwargs


@pytest.mark.parametrize(['files', 'append', 'file_properties', 'expected_files'], [
    pytest.param(
        '/path/to/file2.tgz#my_archive', False, {},
        {
            'my_archive': SkeinFile(source='/path/to/file2.tgz')
        },
        id='append=False, no extra file properties'
    ),

    pytest.param(
        '/path/to/file2.tgz#my_archive', True, {},
        {
            'first_file.txt': SkeinFile(source='first_file.txt'),
            'my_archive': SkeinFile(source='/path/to/file2.tgz')
        },
        id='append=True, no extra file properties'
    ),

    pytest.param(
        '/path/to/file2.tgz#my_archive.tgz', True, {'type': 'file'},
        {
            'first_file.txt': SkeinFile(source='first_file.txt'),
            'my_archive.tgz': SkeinFile(source='/path/to/file2.tgz', type='file')
        },
        id='append=True with extra file properties'
    ),
])
def test_skein_hook_builder_with_complex_files(
    files,
    append,
    file_properties,
    expected_files
):
    """
    Tests SkeinHookBuilder with complex files
    """
    builder = skein.SkeinHookBuilder()
    builder.script('echo hi')
    builder.files('first_file.txt')

    builder.files(files, append=append, **file_properties)
    hook = builder.build()
    assert hook._application_spec.master.files == expected_files
