'''
### Unique editors by country
Aggregate editors_daily data to compute unique editors per country, regardless of wiki project.
Note: This dataset does NOT contain bot actions and only considers edit actions.
'''

from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters

from analytics.config.dag_config import default_args, hql_directory

dag_id = 'unique_editors_by_country_monthly'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 7, 1)),
    schedule_interval='@monthly',
    catchup=var_props.get('catchup', False),
    tags=['geoeditors'],
    user_defined_filters=filters,
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=10),
        }
    )
) as dag:

    source_table = var_props.get('source_table', 'wmf.editors_daily')
    current_month = '{{ execution_date | to_ds_month }}'

    hive_sensor = NamedHivePartitionSensor(
        task_id='wait_for_editors_daily',
        partition_names=[f'{source_table}/month={current_month}'],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    etl = SparkSqlOperator(
        task_id='process_unique_editors_by_country_monthly',
        sql=var_props.get('hql_path', f'{hql_directory}/geoeditors/unique_editors_by_country_monthly.hql'),
        query_parameters={
            'source_table': source_table,
            'destination_table': var_props.get('destination_table', 'wmf.unique_editors_by_country_monthly'),
            'month': current_month,
            'coalesce_partitions': 1,
        },
    )

    hive_sensor >> etl
