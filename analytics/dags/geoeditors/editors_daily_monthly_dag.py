'''
### Geo-editors daily
Geolocate and group edits by wiki, country, user, user_is_bot_by and action_type.
This data will be used to generate monthly geoeditors reports,
and deleted after 90 days for privacy reasons.
'''

from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.url import URLSensor
from analytics.config.dag_config import default_args, hadoop_name_node, hql_directory, artifact

dag_id = 'editors_daily_monthly'
var_props = VariableProperties(f'{dag_id}_config')
current_month = '{{ execution_date.strftime("%Y-%m") }}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 1)),
    schedule_interval='@monthly',
    tags=['spark', 'hql', 'hive', 'geoeditors'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(days=10),
        }
    ),
) as dag:

    hive_sensor = NamedHivePartitionSensor(
        task_id='wait_for_user_history',
        partition_names=[f'wmf.mediawiki_user_history/snapshot={current_month}'],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    # The _PARTITIONED file tells us that projects partitions
    # have been added to hive for this month
    hdfs_sensor = URLSensor(
        task_id='wait_for_cu_changes',
        url=(f'{hadoop_name_node}/wmf/data/raw/mediawiki_private'
             f'/tables/cu_changes/month={current_month}/_PARTITIONED'),
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    etl = SparkSqlOperator(
        task_id='process_editors_daily',
        sql=var_props.get('hql_path', f'{hql_directory}/geoeditors/editors_daily_monthly.hql'),
        query_parameters={
            'refinery_hive_jar': artifact('refinery-hive-0.2.1-shaded.jar'),
            'source_table': 'wmf_raw.mediawiki_private_cu_changes',
            'user_history_table': 'wmf.mediawiki_user_history',
            'destination_table': var_props.get('destination_table', 'wmf.editors_daily'),
            'month': current_month,
            'coalesce_partitions': 1,
        },
        conf={'spark.dynamicAllocation.maxExecutors': 64},
    )

    [hive_sensor, hdfs_sensor] >> etl
