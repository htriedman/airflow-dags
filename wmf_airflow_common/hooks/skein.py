from typing import cast, Any, Tuple, List, Union, Optional

import os
import re
import getpass
import time
import skein
from airflow import AirflowException
from airflow.hooks.base import BaseHook
from airflow.compat.functools import cached_property


class SkeinHook(BaseHook):
    """
    Runs an application to YARN via Skein.
    """
    def __init__(
        self,
        application_spec: Union[skein.model.ApplicationSpec, str],
        client_kwargs: Optional[dict] = None,
        app_log_collection_enabled: bool = True,
        **kwargs: Any
    ):
        """
        application_spec describes the application to run in YARN.

        driver_* parameters are passed to skein.Client(). Depending
        on parameters passed, a skein Driver will either be connected to,
        or started locally within this process.

        We avoid instantiaing the skein.Client until it is needed
        during the call to submit()

        :param application_spec:
            skein.model.ApplicationSpec.
            If a string, then this should be a path to an application spec file, and
            ApplicationSpec.from_file() will be called on it.

            See:
                - https://jcristharif.com/skein/specification.html
                - https://jcristharif.com/skein/api.html#application-specification

        :param client_kwargs:
            skein.Client pararmeters, used to connect to or start a skein Driver.
            See: https://jcristharif.com/skein/api.html#client for
            documentation.

        :param app_log_collection_enabled:
            If True (default), logs from the YARN AppMaster will be collected
            and logged here after the application finishes.
            Disable this if you expect your AppMaster to be too verbose for
            local logging.

        :param **kwargs:
            kwargs to pass to BaseHook parent constructor.
        """

        if isinstance(application_spec, str):
            app_spec = \
                skein.model.ApplicationSpec.from_file(application_spec)
        else:
            app_spec = cast(skein.model.ApplicationSpec, application_spec)

        self._application_spec: skein.model.ApplicationSpec = app_spec
        self._client_kwargs: dict = client_kwargs or {}
        self._app_log_collection_enabled: bool = app_log_collection_enabled

        self._application_id: Optional[str] = None
        self._application_client: Optional[skein.ApplicationClient] = None
        self._yarn_logs: Optional[str] = None

        # finished will be set after submit() has been called and done.
        # This is used to prevent submit() from being called more than once.
        self._finished: bool = False
        super().__init__(**kwargs)

    def get_conn(self) -> None:
        """
        Overridden for compatibility with Airflow BaseHook.
        We may want to implement this if we want to store Hadoop
        connection info in Airflow.
        """

    @cached_property
    def _client(self) -> skein.Client:
        """
        A skein.Client used by this SkeinSubmitHook.
        This is meant to be single use. Once submit() is called,
        this cannot be used again.
        """
        self.log.info('Constructing skein Client with kwargs: %s', self._client_kwargs)
        return skein.Client(**self._client_kwargs)

    @cached_property
    def _master(self) -> dict:
        """
        The skein Master part of the ApplicationSpec as a dict.
        """
        return dict(self._application_spec.to_dict()['master'])

    @cached_property
    def _script(self) -> str:
        """
        The script that will be run in the YARN AppMaster.
        """
        return str(self._master['script'])

    @cached_property
    def _app_owner(self) -> str:
        """
        Skein ApplicationSpec user, or user running the current process
        if not set in ApplicationSpec.
        """
        return self._application_spec.user or getpass.getuser()

    def report(self) -> skein.model.ApplicationReport:
        """
        The skein ApplicationReport, or None if the YARN application is not running.
        """
        if self._application_id is None or self._client is None:
            return None
        return self._client.application_report(self._application_id)

    def final_status(self) -> skein.model.FinalStatus:
        """
        skein FinalStatus of the YARN application.
        """
        final_status = skein.model.FinalStatus.UNDEFINED
        report = self.report()
        if report:
            final_status = report.final_status
        return final_status

    def status(self) -> Union[skein.model.ApplicationState, skein.model.FinalStatus]:
        """
        The current ApplicationState if the application is running,
        else FinalStatus, else UNDEFINED.
        """
        status = skein.model.FinalStatus.UNDEFINED
        report = self.report()

        if report:
            if report.final_status == skein.model.FinalStatus.UNDEFINED:
                status = report.state
            else:
                status = report.final_status

        return status

    def report_string(self) -> str:
        """
        Returns a nicely formatted report string for the Skein app.
        If the app as not been started (AKA there is no report yet),
        this will return an empty string.
        """
        report = self.report()

        if report is None:
            return ''

        report_string = f'status: {self.status()}'
        if report.diagnostics:
            report_string += f'. {report.diagnostics}'
        return report_string

    def get_yarn_logs(self) -> skein.model.ApplicationLogs:
        """
        YARN logs of the application. Can only be retrieved
        after the application has finished, and requires that
        YARN's log aggregation is enabled. If called before
        the application has finished, this returns None.
        """
        if self.final_status() == skein.model.FinalStatus.UNDEFINED:
            return None

        if self._yarn_logs:
            return self._yarn_logs

        self._yarn_logs = self._client.application_logs(self._application_id).dumps()

        return self._yarn_logs

    def find_yarn_app_ids(self) -> Optional[List[str]]:
        """
        Searches Skein YARN application master logs for other YARN app ids and returns
        a list of mentioned YARN app ids.
        """
        yarn_logs = self.get_yarn_logs()
        if yarn_logs is None:
            return None

        return find_yarn_app_ids(yarn_logs)

    def submit(self) -> str:
        """
        Submits the Skein application and waits for it to finish.

        :return: Skein application FinalStatus as a string.
        """
        if self._finished:
            raise AirflowException(
                f'{self} cannot call submit, the YARN application has already finished.'
            )

        self.log.info(
            '%s - Submitting with Skein ApplicationSpec:\n%s',
            self,
            self._application_spec.to_yaml()
        )
        self._application_id = self._client.submit(self._application_spec)

        # (First try here is just to wrap everything for the finally block.)
        try:
            try:
                # connect to the running application and wait for it to finish.
                self._application_client = self._client.connect(self._application_id, wait=True)
            except skein.exceptions.ApplicationNotRunningError as err:
                # This might happen if the application finishes before we can connect to it.
                # If the app succeeded, just continue on, else re-raise the error.
                if self.final_status() != 'SUCCEEDED':
                    raise err

                self.log.debug(
                    '%s finished before Skein ApplicationClient could connect to get '
                    'status. If the YARN application was meant to be very quick, this '
                    'is probably okay.',
                    self
                )

            # Wait until the YARN application master finishes.
            # TO DO: perhaps use Airlfow Deferrable trigger here instead?
            # https://airflow.apache.org/docs/apache-airflow/stable/concepts/deferring.html
            while self.final_status() == 'UNDEFINED':
                self.log.info('%s %s - Waiting until finished.', self, self.report_string())
                time.sleep(15)

            if self._app_log_collection_enabled:
                # Sleep a bit to give yarn log aggregation a chance to finish.
                # TO DO: should this be configurable?
                time.sleep(5)

                self.log.info(
                    '%s - Showing logs for Skein application master:\n%s',
                    self,
                    self.get_yarn_logs()
                )

                yarn_app_ids = self.find_yarn_app_ids()
                if yarn_app_ids is not None:
                    self.log.info(
                        '%s - YARN logs mentioned the following YARN application ids: %s',
                        self,
                        ','.join(yarn_app_ids)
                    )
                    self.log.info(
                        '%s - To view all mentioned YARN application logs, '
                        'run the following commands:\n\t%s',
                        self,
                        '\n\t'.join(yarn_logs_commands(yarn_app_ids, self._app_owner, sudo=True))
                    )
                else:
                    self.log.info(
                        '%s - YARN logs did not mention any of YARN application ids.',
                        self
                    )

            else:
                self.log.info(
                    '%s - YARN application log collection is disabled. To view logs for the '
                    'YARN App Master, run the following command:\n\t%s'
                    '\nIf your App Master launched other YARN applications (e.g. a Spark app), '
                    'you will need to look at these logs and run a simliar command but with '
                    'the appropriate YARN application_id.',
                    self,
                    '\n\t'.join(yarn_logs_commands([str(self._application_id)], self._app_owner, sudo=True))
                )

            if self.final_status() != 'SUCCEEDED':
                raise AirflowException(str(self))

        finally:
            self.log.debug('%s %s - Stopping Skein.', self, self.report_string())
            self.stop(self.final_status())

        return str(self.final_status())

    def stop(self, final_status: str, diagnostics: Optional[str] = None) -> None:
        """
        Stops the YARN application (if running), closes the Skein ApplicationClientk
        and closes the Skein Client (and Driver, if this Client started it).

        :param final_status:
            Skein FinalStatus to use when shutting down the YARN app.
            This is ignored if the actual YARN app final status is anything
            but UNDEFINED and/or if the application_client is not connected.s

        :param diagnostics:
            String to pass to Skein ApplicationClient shutdown, if
            it needs to be called.
        """
        if self._application_client:
            if self.final_status() == 'UNDEFINED':
                self._application_client.shutdown(
                    status=final_status,
                    diagnostics=diagnostics
                )
            self._application_client.close()
            self._application_client = None

        if self._client:
            self._client.close()
            self._client = None

        # set finished to True so further calls to
        # submit will no longer do anything.
        self._finished = True

    def on_kill(self) -> None:
        return self.stop('KILLED', 'Killed by Airflow executor')

    def __str__(self) -> str:
        parts = [self.__class__.__name__, self._application_spec.name]
        if self._application_id:
            parts += [self._application_id]
        return ' '.join(parts).strip()


class SkeinHookBuilder():

    _default_memory = 1024
    _default_vcores = 1

    """
    Aides in building a simple SkeinHook
    with just a single YARN ApplicationMaster container.
    """
    def __init__(
        self,
        app_spec_dict: Optional[dict] = None,
        client_kwargs: Optional[dict] = None,
    ):
        """
        :param app_spec_dict:
            If given, will be used as the starting ApplicationSpec dict.
            You can use this to initialize the app spec before
            calling any builder methods.

        :param client_kwargs:
            Starting kwargs to pass to skein.Client.
            These can be modifeid by builder methods too.
            NOTE: This hook being used within operators, the default_args
                  in this kwargs should have been expanded.
        """
        self.app_spec_dict = app_spec_dict or {}
        if 'master' not in self.app_spec_dict:
            self.app_spec_dict['master'] = {}

        self.client_kwargs = client_kwargs or {}

        # Set any defaults
        if 'resources' not in self.app_spec_dict['master']:
            self.app_spec_dict['master']['resources'] = {
                'memory': self._default_memory,
                'vcores': self._default_vcores
            }

        self._app_log_collection_enabled = True

    def name(self, name: str) -> 'SkeinHookBuilder':
        self.app_spec_dict['name'] = name
        return self

    def queue(self, queue: str) -> 'SkeinHookBuilder':
        self.app_spec_dict['queue'] = queue
        return self

    def script(self, script: str) -> 'SkeinHookBuilder':
        self.app_spec_dict['master']['script'] = script
        return self

    def resources(self, resources: dict) -> 'SkeinHookBuilder':
        self.app_spec_dict['master']['resources'] = resources
        return self

    def files(
        self,
        files: Union[str, list, dict],
        append: bool = False,
        **file_properties: dict
    ) -> 'SkeinHookBuilder':
        """
        Sets skein.Master files kwargs.

        :param files:
            If a string, this is assumed to be a comma separated string
            of files to add.  You may use spark style '#' to name the alias,
            otherwise basename of the file path will be used.
            Example: path/to/file1#myfile,path/to/file2

            If a list, assumed to be a list of file strings as described above.

            If a dict, assumed to be something can be understood by
            skein.Master spec for use with Master.from_dict.

        :param append:
            If true, files given here will be appened to ones previously added.
            Defaults to False

        :param **file_properties:
            Only used when files is a str or a list. These are
            skein.model.File spec properties. Use this if you want
            to set e.g. the type or security of the files.
            This will be set for all provided files.  If you want to set
            different properties for different files, call this
            method multiple times with append=True.
            See: https://jcristharif.com/skein/specification.html#files

        """
        if isinstance(files, str):
            files = files.split(',')

        if isinstance(files, list):
            files = dict([
                parse_file_source(
                    source=file,
                    **file_properties,
                    as_dict=True
                ) for file in files
            ])

        if not append or 'files' not in self.app_spec_dict['master']:
            self.app_spec_dict['master']['files'] = files
        else:
            self.app_spec_dict['master']['files'].update(files)

        return self

    def env(self, env_vars: dict, append: bool = False) -> 'SkeinHookBuilder':
        """
        Sets env vars for the YARN AppMaster.

        :param env_vars:
            dict of env vars to set

        :param append:
            If true, env_vars will be added to any already
            added by previous calls to this method.
            Defaults to False.
        """
        if not append or 'env' not in self.app_spec_dict['master']:
            self.app_spec_dict['master']['env'] = env_vars
        else:
            self.app_spec_dict['master']['env'].update(env_vars)
        return self

    def master_log_level(self, log_level: str) -> 'SkeinHookBuilder':
        self.app_spec_dict['master']['log_level'] = log_level
        return self

    def client_log_level(self, log_level: str) -> 'SkeinHookBuilder':
        self.client_kwargs['log_level'] = log_level
        return self

    def app_log_collection_enabled(self, enabled: bool) -> 'SkeinHookBuilder':
        self._app_log_collection_enabled = enabled
        return self

    def principal(self, principal: str) -> 'SkeinHookBuilder':
        self.client_kwargs['principal'] = principal
        return self

    def keytab(self, keytab: str) -> 'SkeinHookBuilder':
        self.client_kwargs['keytab'] = keytab
        return self

    def build(self) -> SkeinHook:
        return SkeinHook(
            skein.ApplicationSpec.from_dict(self.app_spec_dict),
            self.client_kwargs,
            app_log_collection_enabled=self._app_log_collection_enabled,
        )


YARN_APP_ID_REGEX: re.Pattern = re.compile('application_\d{13}_\d{4,}')
"""
Regex used to match YARN applicaition IDs out of logs.
"""


def parse_file_source(
    source: str,
    as_dict: bool = True,
    **file_properties: Any
) -> Tuple[str, Union[dict, skein.File]]:
    """
    Parses a file source string similar to the way spark-submit does.
    and returns a tuple of (alias, File|file spec).

    :param source:
        A URI string pointing at a file source. If source contains a '#' character,
        it is expected that the part after the '#' should be the file's alias
        on the YARN worker.  If no '#' is found, the file alias will be the
        files's basename.

    :param as_dict:
        If true, the second value in the tuple will be a dict matching
        the skein.File spec.  Otherwise, it will be a
        skein.File instance.
        Use as_dict=True if you plan to pass the result to
        the a skein Spec .from_dict() factory.
        Defaults to True.

    :param file_properties:
        Other skein.model.File spec properties.
        See: https://jcristharif.com/skein/specification.html#files

    :return:
        A Tuple of (alias, skein.model.File)

    """
    if '#' in source:
        (uri, alias) = source.rsplit('#', 1)
    else:
        alias = os.path.basename(source)
        uri = source

    file = skein.model.File(source=uri, **file_properties)

    if as_dict:
        file = file.to_dict()

    return (alias, file)


def yarn_logs_commands(
    yarn_app_ids: List[str],
    app_owner: Optional[str] = None,
    sudo: bool = True
) -> List[str]:
    """
    Given YARN app ids, returns a list of yarn logs commands to run
    to view aggregated YARN logs for those YARN apps.

    :param yarn_app_ids:
        List of yarn app ids.

    :param app_owner:
        Add this as the -appOwner param

    :param sudo:
        If true and app_owner is given, sudo -u app_owner
        will be prefixed to the  yarn logs command.
    """

    sudo_cmd = f'sudo -u {app_owner} ' if app_owner and sudo else ''
    app_owner = f'-appOwner {app_owner} ' if app_owner else ''
    return [f'{sudo_cmd}yarn logs {app_owner}-applicationId {app_id}' for app_id in yarn_app_ids]


def find_yarn_app_ids(logs: str) -> List[str]:
    """
    Finds YARN app ids in logs.

    :param logs: Logs in which to search for YARN app ids.
    """
    app_ids = list(set(re.findall(YARN_APP_ID_REGEX, logs)))
    app_ids.sort()
    return app_ids
