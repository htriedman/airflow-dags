import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics', 'dags', 'aqs', 'aqs_hourly_dag.py']


def test_aqs_hourly_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="aqs_hourly")
    assert dag is not None
    assert len(dag.tasks) == 2
    assert dag.tasks[0].partition_names == [
        'wmf.webrequest/webrequest_source=text'
        '/year={{execution_date.year}}'
        '/month={{execution_date.month}}'
        '/day={{execution_date.day}}'
        '/hour={{execution_date.hour}}']

    # Tests that the defaults from default_args are here.
    assert dag.default_args['do_xcom_push'] is False

    # Tests that the defaults from dag_config are here.
    assert dag.default_args['metastore_conn_id'] == 'analytics-hive'
