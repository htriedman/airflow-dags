"""
### Compute the pageview_actor dataset every new hour

"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.url import URLTouchOperator
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, dataset, hql_directory, artifact, hadoop_name_node

dag_id = 'pageview_actor_hourly'
var_props = VariableProperties(f'{dag_id}_config')

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    # Same as webrequest_actor_label_hourly
    start_date=var_props.get_datetime('start_date', datetime(2023, 1, 2)),
    schedule_interval='@hourly',
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
) as dag:

    webrequest_sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)
    webrequest_actor_label_sensor = dataset("hive_wmf_webrequest_actor_label_hourly").get_sensor_for(dag)

    etl = SparkSqlOperator(
        task_id='compute_pageview_actor_hourly',
        sql=var_props.get('hql_path', f'{hql_directory}/pageview/actor/pageview_actor.hql'),
        query_parameters={
            'refinery_hive_jar_path': var_props.get('refinery_hive_jar_path', artifact('refinery-hive-0.2.1-shaded.jar')),
            'source_table': var_props.get('source_table', 'wmf.webrequest'),
            'actor_label_table': var_props.get('actor_label_table', 'wmf.webrequest_actor_label_hourly'),
            'destination_table': var_props.get('destination_table', 'wmf.pageview_actor'),
            'coalesce_partitions': var_props.get('coalesce_partitions', 32), # Number of output files
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
            'day': '{{ execution_date.day }}',
            'hour': '{{ execution_date.hour }}',
        },
        driver_memory='4G',
        executor_memory='8G',
        executor_cores=2,
        conf={
            'spark.dynamicAllocation.maxExecutors': 128,
        }
    )

    # TODO: Remove this step when the various dependent tables are migrated to Airflow
    add_success_file = URLTouchOperator(
        task_id="write_pageview_actor_success_file",
        url=f"{hadoop_name_node}/wmf/data/wmf/pageview/actor"
            "/year={{execution_date.year}}"
            "/month={{execution_date.month}}"
            "/day={{execution_date.day}}"
            "/hour={{execution_date.hour}}"
            "/_SUCCESS")

    [webrequest_sensor, webrequest_actor_label_sensor] >> etl >> add_success_file
