"""Generate daily head queries report for all wikis"""

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from datetime import datetime
from search.config.dag_config import discolytics_conda_env_tgz, get_default_args
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.config.variable_properties import VariableProperties


dag_id = 'fulltext_head_queries'
var_props = VariableProperties(f'{dag_id}_config')

default_args = var_props.get_merged('default_args', {
    **get_default_args(),
    'start_date': var_props.get_datetime('start_date', datetime(2021, 1, 1)),
})

with DAG(
    f'{dag_id}_daily',
    default_args=default_args,
    # min hour day month dow
    schedule_interval='38 0 * * *',
    max_active_runs=1,
    # single report covers all data, backfill/catchup wouldn't make sense
    catchup=False,
) as dag:
    table_search_satisfaction = var_props.get(
        'table_search_satisfaction', 'event.searchsatisfaction')
    output_table = var_props.get('output_table', 'discovery.fulltext_head_queries')
    output_partition_spec = output_table + '/date={{ ds_nodash }}'

    head_queries = SparkSubmitOperator.for_virtualenv(
        task_id='head_queries',
        name='airflow: fulltext_head_queries - {{ ds }}',
        # This task reads the fully history of searchsatisfaction,
        # hundreds of GB and thousands of partitions. Keep a cap
        # on how much parallelism spark will try to use.
        max_executors=100,
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/fulltext_head_queries.py',
        application_args=[
            '--search-satisfaction-partition', f'{table_search_satisfaction}/',
            '--num-queries', var_props.get('num_queries', 10000),
            '--output-partition', output_partition_spec,
        ])

    complete = DummyOperator(task_id='complete')
    head_queries >> complete
