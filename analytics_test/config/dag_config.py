from wmf_airflow_common.config import dag_default_args
from wmf_airflow_common.artifact import ArtifactRegistry
from wmf_airflow_common.util import is_wmf_airflow_instance

# General paths.
hadoop_name_node = 'hdfs://analytics-test-hadoop'
refinery_directory = f'{hadoop_name_node}/wmf/refinery'
hql_directory = f'{refinery_directory}/current/hql'
artifacts_directory = f'{refinery_directory}/current/artifacts'
hdfs_temp_directory = '/tmp/analytics'

# Emails.
alerts_email = 'data-engineering-alerts@lists.wikimedia.org'

# default_args for this Airflow Instance.
instance_default_args = {
    'owner': 'analytics',
    'email': alerts_email,
    'hadoop_name_node': hadoop_name_node,
    'metastore_conn_id': 'analytics-test-hive',
}

# For jobs running in production instances, set the production yarn queue.
if is_wmf_airflow_instance():
    instance_default_args['queue'] = 'production'

# Artifact registry for dependency paths.  By renaming
# this function to 'artifact', we get a nice little
# syntactic sugar to use when declaring artifacts in dags.
artifact_registry = ArtifactRegistry.for_wmf_airflow_instance('analytics_test')
artifact = artifact_registry.artifact_url

# Default arguments for all operators used by this airflow instance.
default_args = dag_default_args.get(instance_default_args)