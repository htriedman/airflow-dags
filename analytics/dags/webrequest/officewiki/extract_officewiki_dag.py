"""
### Extract webrequests that look like pageviews to Office wiki

Reasoning:
  office.wikimedia.org is an internal wiki for wmf staff.  As such, it's excluded
  from the pageview definition.  Some questions came up about some pages and how often
  they're accessed.  It's very hard to query the full webrequest for large periods of time
  (more than 1 day).  So this job will run as an experiment for March and April 2022,
  pulling one day at a time.  We'll then stop it and analyze the results, deciding
  whether we need a more permanent solution, like updating the pageview definition or
  instrumenting pageviews in some other way.

The resulting dataset is:
* partitioned by year, month, day
* located in /wmf/data/wmf/pageview/officewiki/daily/year=2022/month=4/day=13/
"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.hive import NamedHivePartitionSensor
from wmf_airflow_common.templates.time_filters import filters
from wmf_airflow_common.partitions_builder import daily_partitions
from analytics.config.dag_config import default_args, hql_directory, artifact

dag_id = 'officewiki_pageviews_daily'
var_props = VariableProperties(f'{dag_id}_config')
source_table = var_props.get('source_table', 'wmf.webrequest')
destination_table = var_props.get('destination_table', 'wmf.officewiki_webrequest_daily')
refinery_hive_jar_path = var_props.get('refinery_hive_jar_path', artifact('refinery-hive-0.2.1-shaded.jar'))
start = datetime(2022, 5, 15)

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', start),
    schedule_interval='@daily',
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
    user_defined_filters=filters,
) as dag:

    sensor = NamedHivePartitionSensor(
        task_id='wait_for_webrequest_day',
        partition_names=daily_partitions(
            table=source_table,
            granularity='@hourly',
            pre_partitions=['webrequest_source=text']
        )
    )

    etl = SparkSqlOperator(
        task_id='extract_officewiki_webrequest',
        sql=var_props.get('hql_path', f'{hql_directory}/webrequest/officewiki/extract_webrequest_daily.hql'),
        query_parameters={
            'source_table': source_table,
            'destination_table': destination_table,
            'refinery_hive_jar_path': refinery_hive_jar_path,
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
            'day': '{{ execution_date.day }}',
        }
    )

    sensor >> etl
