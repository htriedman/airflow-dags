from airflow import DAG
from airflow.operators.dummy import DummyOperator
from datetime import datetime
from search.config.dag_config import get_default_args, discolytics_conda_env_tgz
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator


dag_id = 'cirrus_namespace_map_daily'
var_props = VariableProperties(f'{dag_id}_config')

default_args = get_default_args()

with DAG(
    dag_id,
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': var_props.get_datetime('start_date', datetime(2023, 1, 30)),
    }),
    schedule_interval='0 1 * * *',
    # The dag overwrites, catchup would only add repeated work
    catchup=False,
    # The dag overwrites data, so running two at a time would be silly
    max_active_runs=1,
) as dag:
    fetch_namespace_map = SparkSubmitOperator.for_virtualenv(
        task_id='fetch_namespace_map',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/fetch_cirrussearch_namespace_map.py',
        application_args={
            # The trailing '/' indicates they are unpartitioned tables.
            '--canonical-wikis-partition': 'canonical_data.wikis/',
            '--output-partition': var_props.get('table_output', 'discovery.cirrus_namespace_index_map') + '/',
        }
    )

    complete = DummyOperator(task_id='complete')

    fetch_namespace_map >> complete
