from airflow.models import TaskInstance
from search.shared.hive_table_path import HiveTablePath
from search.shared.auto_size_spark import AutoSizeSparkSubmitOperator
from copy import deepcopy
from dataclasses import dataclass
import json
import os
import pytest
from typing import Any, Callable, TextIO
from unittest import mock


@dataclass
class FixtureSerDe:
    encode: Callable[[TextIO, Any], None]
    decode: Callable[[TextIO], Any]
    roundtrip: Callable[[Any], Any]


@pytest.fixture(scope='session', autouse=True)
def environment_override_fixture():
    override_env = {
        'AIRFLOW_ENVIRONMENT_NAME': 'wmf_search',
        'AIRFLOW_VAR_MJOLNIR_WEEKLY_CONFIG': json.dumps({
            # Limit the number of duplicate fixtures generated
            'wikis': ['arwiki', 'enwiki'],
        })
    }
    with mock.patch.dict(os.environ, override_env, clear=False):
        yield


@pytest.fixture(scope='session')
def fixture_dir():
    return os.path.join(os.path.dirname(__file__), 'fixtures')


@pytest.fixture(scope='session')
def compare_with_fixture(fixture_dir):
    """Returns a function that compares against a fixture"""
    def compare(group, fixture_id, content, serde='json'):
        path = os.path.join(fixture_dir, group, fixture_id + '.expected')
        serde = compare_with_fixture.serde[serde]

        if os.path.exists(path):
            with open(path, 'r') as f:
                expect = serde.decode(f)
            # Some encoders, like json, convert tuple->list, so we need
            # a roundtrip to normalize
            assert expect == serde.roundtrip(content)
        elif os.environ.get('REBUILD_FIXTURES') == 'yes':
            with open(path, 'w') as f:
                serde.encode(f, content)
        else:
            raise Exception(f'No fixture [{path}] and REBUILD_FIXTURES != yes')

    return compare

compare_with_fixture.serde = {
    'json': FixtureSerDe(
        encode=lambda f, val: json.dump(val, f, indent=4, sort_keys=True),
        decode=json.load,
        roundtrip=lambda val: json.loads(json.dumps(val))),
    'str': FixtureSerDe(
        encode=lambda f, val: f.write(val),
        decode=lambda f: f.read(),
        roundtrip=lambda val: val)
}


@pytest.fixture
def render_task(fixture_dir, mocker):
    """Returns a function that will render an airflow task"""
    def fn(task):
        # This will change the task, take a copy
        task = deepcopy(task)
        # Prevent from trying to talk with hive
        mocker.patch.object(HiveTablePath, '__call__').side_effect = lambda t: \
                t if t.startswith('hdfs://') else f'/path/to/{t}'
        # Some dags have expectations, such as it always runs on sunday. Some date manipulation
        # can depend on this. To ensure changes to date manipulation are properly tested for
        # consistency render the task against the first run date of the task.
        task_first_dt = task.dag.normalize_schedule(task.start_date)
        ti = TaskInstance(task, task_first_dt)
        context = ti.get_template_context()
        context['run_id'] = 'pytest_compare_against_fixtures'
        ti.render_templates(context=context)

        # This would apply itself at runtime, apply for fixtures as well.
        if isinstance(task, AutoSizeSparkSubmitOperator):
            if task._autosize_for_matrix:
                # While perhaps a bit tedious, developer will need to place appropriate
                # metadata files to have plausible fixtures.
                task._autosize_for_matrix.metadata_uri = \
                    f'file://{fixture_dir}/autosize_metadata/{task.dag_id}-{task.task_id}.json'
            task._get_auto_sizer().apply(task)

        return task
    return fn
