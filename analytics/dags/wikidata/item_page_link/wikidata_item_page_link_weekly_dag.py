"""
### Generate a wikidata_item_page_link snapshot from a wikidata entity table.

It generates a table linking entities to pages on different wikis.
https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Content/Wikidata_item_page_link
Data is saved in parquet format in a hive partition folder.

#### Sources:
* wmf.wikidata_entity
    - partitioned by wikidata_snapshot
    - 1 dump/week
* wmf.mediawiki_page_history
    - partitioned by history_snapshot
    - 1 dump/month
* wmf_raw.mediawiki_project_namespace_map
    - partitioned by history_snapshot
    - 1 dump/month
* event.mediawiki_page_move
    - partitioned hourly
    - To build an output partition, his job needs this data
      from the end of last month to the end of this week.

#### Note concerning the snapshotted tables:
    - a snapshot contains all the data of its source table.
    - the snapshot is labelled at the begining of the period (i.e. week),
      but contains the data till the end of the period.
    - ex: the weekly snapshot labelled 2022-03-07, contains a view of the
    source tables at 2022-03-14T00:00. So it contains all the updates of the
    table < 2022-03-14T00:00.

#### Steps:
0- Detects the partitions of the different tables.
1- Loads a list of wm pages with their id, db and namespace from a snapshot.
2- Loads the last data about pages from the events.
3- Updates the snapshot data with the event data.
4- Translates the page namespaces.
5- Loads the links form the entity table.
6- Provides the page information for each link.
7- Unloads data to a partition.

#### Output:
* wmf.wikidata_item_page_link
    - partitioned by wikidata_snapshot
    - 1 dump/week
    - located in /wmf/data/wmf/wikidata/item_page_link/

#### Output notes
* The Hive script runs in Spark.
* No _PARTITIONED or _SUCCESS files are generated.
"""

from datetime import datetime, timedelta
from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor
from wmf_airflow_common.templates.time_filters import filters
from analytics.config.dag_config import default_args, hql_directory

dag_id = 'wikidata_item_page_link_weekly'
var_props = VariableProperties(f'{dag_id}_config')
history_snapshot = '{{execution_date | start_of_next_week | start_of_previous_month | to_ds_month}}'
wikidata_snapshot = '{{execution_date | start_of_current_week | to_ds}}'

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 6)),
    schedule_interval='0 0 * * 1',  # Weekly starting on Monday.
    tags=['spark', 'hql', 'hive', 'wikidata'],
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'sla': timedelta(days=7),
    }),
    user_defined_filters=filters,
) as dag:

    s1 = NamedHivePartitionSensor(
        task_id='wait_for_wikidata_entity',
        partition_names=[f'wmf.wikidata_entity/snapshot={wikidata_snapshot}'],
    )

    s2 = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_page_history',
        partition_names=[f'wmf.mediawiki_page_history/snapshot={history_snapshot}'],
    )

    s3 = NamedHivePartitionSensor(
        task_id='wait_for_mediawiki_project_namespace_map',
        partition_names=[f'wmf_raw.mediawiki_project_namespace_map/snapshot={history_snapshot}'],
    )

    s4 = RangeHivePartitionSensor(
        task_id='wait_for_mediawiki_page_move',
        table_name='event.mediawiki_page_move',
        from_timestamp='{{execution_date | start_of_next_week | start_of_current_month}}',
        to_timestamp='{{execution_date | start_of_next_week}}',
        granularity='@hourly',
        # TODO Change this back to eqiad once the datacenter switchover test is finished.
        # pre_partitions=['datacenter=eqiad'],
        pre_partitions=['datacenter=codfw'],
    )

    etl = SparkSqlOperator(
        task_id='process_item_page_link_data',
        sql=var_props.get('query_path', f'{hql_directory}/wikidata/item_page_link/weekly.hql'),
        query_parameters={
            'history_snapshot': history_snapshot,
            'wikidata_snapshot': wikidata_snapshot,
            'destination_table': var_props.get('destination_table', 'wmf.wikidata_item_page_link'),
            'coalesce_partitions': 64,
        },
        # This job needs more memory than provided by default
        driver_memory='8G',
        executor_memory='8G',
        conf={'spark.dynamicAllocation.maxExecutors': 128},
    )

    [s1, s2, s3, s4] >> etl
