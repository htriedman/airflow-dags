# pylint: disable=protected-access

import pytest

from skein import (
    File as SkeinFile,
    FileType as SkeinFileType
)
from airflow.models import Connection
from wmf_airflow_common.hooks.spark import (
    SparkSubmitHook,
    SparkSkeinSubmitHook,
    kwargs_for_virtualenv,
)


@pytest.fixture
def fixture_base_hook_get_connection(mocker):
    """
    Populates the airflow db with a fake spark Connection.
    """
    mocker.patch(
        'airflow.hooks.base.BaseHook.get_connection',
        # We'll only be testing this in one test parameter,
        # we can just always return this no matter what the conn_id
        # passed to get_connection is.
        return_value=Connection(
            conn_id='spark-fake-yarn-conn',
            conn_type='spark',
            host='yarn',
            extra='{"queue": "default", "deploy-mode": "cluster"}',
        )
    )


@pytest.mark.usefixtures('fixture_base_hook_get_connection')
@pytest.mark.parametrize(['spark_submit_hook_kwargs', 'expected'], [
    pytest.param(
        {
            'conn_id': 'spark-fake-yarn-conn',
            'application': 'fake',
        },
        # These are returnred by Airflow SparkSubmitHook
        # if conn_id is not found in Airflow database connection table.
        {
            'master': 'yarn',
            'queue': 'default',
            'deploy_mode': 'cluster',
            'spark_home': None,
            'spark_binary': 'spark-submit',
            'namespace': None,
        },
        id="Should use parent Airflow SparkSubmitHook"
    ),

    pytest.param(
        {
            'application': 'fake',
            'master': 'yarn',
            'deploy_mode': 'client',
            'spark_binary': '/usr/lib/spark/bin/spark-submit'
        },
        {
            'master': 'yarn',
            'queue': None,
            'deploy_mode': 'client',
            'spark_home': None,
            'spark_binary': '/usr/lib/spark/bin/spark-submit',
            'namespace': None,
        },
        id="Should use our custom SparkSubmitHook"
    ),
])
def test_resolve_connection(spark_submit_hook_kwargs, expected):
    hook = SparkSubmitHook(**spark_submit_hook_kwargs)
    assert hook._connection == expected


@pytest.mark.usefixtures('fixture_base_hook_get_connection')
@pytest.mark.parametrize(['spark_submit_hook_kwargs', 'expected'], [
    pytest.param(
        {
            'conn_id': 'spark-fake-yarn-conn',
            'application': 'fake_app.jar',
        },
        [
            'spark-submit',
            '--master',
            'yarn',
            '--name',
            'default-name',
            '--queue',
            'default',
            '--deploy-mode',
            'cluster',
            'fake_app.jar',
        ],
        id="Should use parent Airflow SparkSubmitHook"
    ),

    pytest.param(
        {
            'driver_cores': 2,
            'driver_java_options': '-Dmy.prop=fake',
            'application': 'fake_app.jar',
            'master': 'yarn',
            'queue': 'fake-queue',
            'name': 'fake_name',
            'deploy_mode': 'client',
            'spark_binary': '/usr/lib/spark/bin/spark-submit',
            'application_args': ['--app-arg1', 1],  # 1 should be cast to a string.
            'env_vars': {'VAR1': 'val1'},
        },
        [
            '/usr/lib/spark/bin/spark-submit',
            '--driver-java-options',
            '-Dmy.prop=fake',
            '--driver-cores',
            '2',
            '--conf',
            'spark.executorEnv.VAR1=val1',
            '--master',
            'yarn',
            '--conf',
            'spark.yarn.appMasterEnv.VAR1=val1',
            '--name',
            'fake_name',
            '--queue',
            'fake-queue',
            '--deploy-mode',
            'client',
            'fake_app.jar',
            '--app-arg1',
            '1'
        ],
        id="Should use our custom SparkSubmitHook"
    ),
])
def test_build_spark_submit_command(spark_submit_hook_kwargs, expected):
    hook = SparkSubmitHook(**spark_submit_hook_kwargs)
    assert hook._build_spark_submit_command() == expected


@pytest.mark.parametrize(['spark_skein_submit_hook_kwargs', 'expected'], [

    pytest.param(
        {
            'driver_cores': 2,
            'driver_java_options': '-Dmy.prop=fake',
            'application': 'fake_app.jar',
            'master': 'yarn',
            'queue': 'fake-queue',
            'name': 'fake_name',
            'deploy_mode': 'client',
            'spark_binary': '/usr/lib/spark/bin/spark-submit'
        },
        [
            '/usr/lib/spark/bin/spark-submit',
            '--driver-java-options',
            '-Dmy.prop=fake',
            '--driver-cores',
            '2',
            '--master',
            'yarn',
            '--name',
            'fake_name',
            '--queue',
            'fake-queue',
            '--deploy-mode',
            'client',
            'fake_app.jar',
        ],
        id="no command modification needed"
    ),

    pytest.param(
        {
            'command_preamble': 'export VAR=VAL1; ',
            'command_postamble': ' || sleep 100',
            'application': 'fake_app.jar',
            'master': 'yarn',
            'name': 'fake_name',
            'deploy_mode': 'client',
            'files': '/path/to/file1.txt#alias1.txt,/path/to/file2.txt',
            'py_files': '/path/to/my.py',
            'keytab': '/path/to/keytab',
            'archives': 'hdfs:///path/to/archive.tgz#alias_archive.tgz',
        },
        [
            'export VAR=VAL1; ',
            'spark-submit',
            '--master',
            'yarn',
            '--files',
            'alias1.txt,file2.txt',
            '--py-files',
            'my.py',
            '--archives',
            'hdfs:///path/to/archive.tgz#alias_archive.tgz',
            '--keytab',
            'keytab',
            '--name',
            'fake_name',
            '--deploy-mode',
            'client',
            'fake_app.jar',
            ' || sleep 100',
        ],
        id="local files paths are modified but remote ones are not, with command_preamble",
    ),

])
def test_spark_skein_submit_skein_hook_script(spark_skein_submit_hook_kwargs, expected):
    hook = SparkSkeinSubmitHook(**spark_skein_submit_hook_kwargs)
    assert hook._skein_hook._application_spec.master.script == ' '.join(expected)


@pytest.mark.parametrize(['spark_submit_hook_kwargs', 'expected_skein_files'], [
    pytest.param(
        {
            'application': 'fake_app.jar',
            'master': 'yarn',
            'name': 'fake_name',
            'deploy_mode': 'client',
            'files': '/path/to/file1.txt#alias1.txt,/path/to/file2.txt',
            'py_files': '/path/to/my.py',
            'keytab': '/path/to/keytab',
            # local_archive.tgz will be added twice so that
            # it can be used in the spark-submit command as an archive,
            # but also unpacked for use by the Skein YarnApp master.
            'archives': 'hdfs:///path/to/archive.tgz#alias_archive.tgz,/path/to/local_archive.tgz',
        },
        {
            'alias1.txt': SkeinFile(source='/path/to/file1.txt'),
            'file2.txt': SkeinFile(source='file:///path/to/file2.txt'),
            'my.py': SkeinFile(source='file:///path/to/my.py'),
            'keytab': SkeinFile(source='file:///path/to/keytab'),
            'alias_archive.tgz': SkeinFile(source='hdfs:///path/to/archive.tgz'),
            # local_archive will be added twice, once as an ARCHIVE and once as a FILE.
            'local_archive.tgz': SkeinFile(
                source='file:///path/to/local_archive.tgz',
                type=SkeinFileType.ARCHIVE,
            ),
            '_skein_archive_orig.local_archive.tgz': SkeinFile(
                source='file:///path/to/local_archive.tgz',
                type=SkeinFileType.FILE,
            )
        },
        id="Files added to skein properly, with local archives added twice.",
    ),
])
def test_spark_skein_submit_hook_skein_files(spark_submit_hook_kwargs, expected_skein_files):
    hook = SparkSkeinSubmitHook(**spark_submit_hook_kwargs)
    skein_files = hook._skein_hook._application_spec.master.files
    assert skein_files == expected_skein_files


@pytest.mark.parametrize(['for_virtualenv_kwargs', 'expected'], [

    pytest.param(
        {
            'virtualenv_archive': 'hdfs:///path/to/conda_env.tgz',
            'entry_point': 'bin/my_pyspark_job.py',
            'use_virtualenv_spark': True,
            'archives': 'hdfs:///my_extra_archive.tgz#my_extra_archive',
            'driver_cores': '2',
            'driver_java_options': '-Dmy.prop=fake',
            'master': 'yarn',
            'queue': 'fake-queue',
            'name': 'fake_name',
            'deploy_mode': 'client',
            'keytab': '/path/to/me.keytab',
            'principal': 'me@ORG',
        },
        [
            'venv/bin/spark-submit',
            '--driver-java-options',
            '-Dmy.prop=fake',
            '--driver-cores',
            '2',
            '--conf',
            'spark.executorEnv.PYSPARK_DRIVER_PYTHON=venv/bin/python',
            '--conf',
            'spark.executorEnv.PYSPARK_PYTHON=venv/bin/python',
            '--master',
            'yarn',
            '--conf',
            'spark.yarn.appMasterEnv.PYSPARK_PYTHON=venv/bin/python',
            '--conf',
            'spark.yarn.appMasterEnv.PYSPARK_DRIVER_PYTHON=venv/bin/python',
            '--archives',
            'hdfs:///path/to/conda_env.tgz#venv,hdfs:///my_extra_archive.tgz#my_extra_archive',
            '--keytab',
            '/path/to/me.keytab',
            '--principal',
            'me@ORG',
            '--name',
            'fake_name',
            '--queue',
            'fake-queue',
            '--deploy-mode',
            'client',
            'venv/bin/my_pyspark_job.py',
        ],
        id="spark_submit_hook_using_kwargs_for_virtualenv"
    ),
])
def test_spark_submit_hook_kwargs_for_virtualenv(for_virtualenv_kwargs, expected):
    hook = SparkSubmitHook(
        **kwargs_for_virtualenv(
            **for_virtualenv_kwargs
        )
    )
    assert hook._build_spark_submit_command() == expected


def test_spark_submit_hook_kwargs_for_virtualenv_both_application_and_entry_point():
    with pytest.raises(ValueError):
        kwargs_for_virtualenv(
            virtualenv_archive='hdfs:///path/to/conda_env.tgz',
            entry_point='bin/my_pyspark_job.py',
            application='/my/application/py',
        )
