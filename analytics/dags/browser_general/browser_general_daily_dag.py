"""
# Browser usage

This job computes weekly browser usage stats from pageview_hourly.
The results serve as an intermediate table for various traffic reports,
i.e.: mobile web browser breakdown, desktop os breakdown, or
desktop+mobile web os+browser breakdown, etc.

#Note: The resulting output is;
* partitioned by year, month, day
* located in /wmf/data/wmf/browser/general
"""

from datetime import datetime, timedelta
from airflow import DAG
from wmf_airflow_common.operators.spark import SparkSqlOperator
from wmf_airflow_common.config.variable_properties import VariableProperties
from airflow.providers.apache.hive.sensors.named_hive_partition import (
    NamedHivePartitionSensor)
from wmf_airflow_common.partitions_builder import daily_partitions
from analytics.config.dag_config import default_args, hql_directory


# Define Constants
dag_id = 'browser_general_daily'
source_granularity = '@hourly'
projectview_source = 'wmf.projectview_hourly'
pageview_source = 'wmf.pageview_hourly'
var_props = VariableProperties(f'{dag_id}_config')

# Instantiate DAG
with DAG(
    dag_id=dag_id,
    description='write_traffic_stats_data_to_hive',
    doc_md=__doc__,
    start_date=var_props.get_datetime('start_date', datetime(2022, 6, 1)),
    schedule_interval='@daily',
    tags=['spark', 'hql', 'hive'],
    default_args=var_props.get_merged('default_args',
        { **default_args,
            'sla': timedelta(hours=6),
        }
    ),
) as dag:

    # Sensors to check that all hive partitions are present
    projectview_sensor = NamedHivePartitionSensor(
        task_id='wait_for_projectview_partitions',
        partition_names=daily_partitions(
            table=projectview_source, granularity=source_granularity
        ),
        poke_interval=timedelta(minutes=20).total_seconds(),
    )

    pageview_sensor = NamedHivePartitionSensor(
        task_id='wait_for_pageview_partitions',
        partition_names=daily_partitions(
            table=pageview_source, granularity=source_granularity
        ),
        poke_interval=timedelta(minutes=20).total_seconds(),
    )

    # ETL Task to execute hql and write to destination
    etl = SparkSqlOperator(
        task_id='summarize_traffic_stats',
        sql=var_props.get(
            'hql_path',
            f'{hql_directory}/browser/general/browser_general.hql',
        ),
        query_parameters={
            'projectview_source': projectview_source,
            'pageview_source': pageview_source,
            'destination_table': var_props.get(
                'destination_table', 'wmf.browser_general'
            ),
            'year': '{{ execution_date.year }}',
            'month': '{{ execution_date.month }}',
            'day': '{{ execution_date.day }}',
            'threshold': var_props.get('threshold', 0.05),
            'os_family_unknown': 'Other',
            'os_major_unknown': '-',
            'browser_family_unknown': 'Other',
            'browser_major_unknown': '-',
            'coalesce_partitions': 1,
        },
    )

    [projectview_sensor,pageview_sensor] >> etl
