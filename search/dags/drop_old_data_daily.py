from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.dummy import DummyOperator
from datetime import datetime
from search.config.dag_config import \
    data_path, get_default_args, discolytics_conda_env_tgz, \
    refinery_local_directory
import shlex
from typing import Any, List, Sequence
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.operators.skein import SimpleSkeinOperator


dag_id = 'drop_old_data_daily'
var_props = VariableProperties(f'{dag_id}_config')

default_args = get_default_args()


def refinery_execute_python(
    script_path: str,
    script_args: List[str],
    *args: Any, **kwargs: Any
) -> BashOperator:
    # the search airflow instance has refinery installed and available for
    # direct import. Would perhaps be more consistent if there was a conda env
    # for running these from Skein
    all_args = [
        f'PYTHONPATH={refinery_local_directory}/python',
        # Must use system python, not airflow's conda env, to have appropriate deps
        '/usr/bin/python3',
        script_path,
    ] + script_args

    safe_arguments = ' '.join(shlex.quote(x) for x in all_args)
    bash_command = '/usr/bin/env' + ' ' + safe_arguments

    return BashOperator(bash_command=bash_command, *args, **kwargs)


def refinery_drop_older_than(
    database: str,
    tables: Sequence[str],
    # This script requires a significant arguments checksum. A human must
    # execute the script without a checksum, verify the dry-run operation, and
    # update the checksum reported in any automated usage.
    checksum: str,
    older_than_days: int = 60,
    allowed_interval: int = 7,
    *args: Any, **kwargs: Any
) -> BashOperator:
    return refinery_execute_python(
        f'{refinery_local_directory}/bin/refinery-drop-older-than',
        [
            '--verbose',
            '--database=' + database,
            '--tables=^({})$'.format('|'.join(tables)),
            '--older-than=' + str(older_than_days),
            '--allowed-interval=' + str(allowed_interval),
            '--execute=' + checksum,
        ], *args, **kwargs)


with DAG(
    dag_id,
    default_args=var_props.get_merged('default_args', {
        **default_args,
        'start_date': var_props.get_datetime('start_date', datetime(2023, 3, 1)),
    }),
    schedule_interval='@daily',
    max_active_runs=2,
    # Always deletes items older than X days, rather than based on execution
    # date. catching up missed runs would do nothing.
    catchup=False,
    # We might end up with quite a few tasks, don't run them all at the
    # same time.
    concurrency=3,
) as dag:
    complete = DummyOperator(task_id='complete')

    refinery_drop_older_than(
        task_id='drop_glent_prep_partitions',
        database='glent',
        tables=['.*'],
        # Each glent partition contains a full dataset, rather than for slice
        # of the time range like some other dbs. It runs once a week and uses
        # last weeks output as this weeks input, as such we really want at
        # least 2 historical outputs at any given time to allow at least some
        # backfilling.
        # The limits here are not related to privacy policy, glent is approved
        # to maintain its de-identified datasets indefinitly, but practical.
        older_than_days=29,
        checksum='9b7209e548e5d11e25b5a2af3ed09d4b',
    ) >> complete

    refinery_drop_older_than(
        task_id='drop_mjolnir_partitions',
        database='mjolnir',
        # This data is derived from long term data
        older_than_days=15,
        tables=[
            # All contain private data that must not be retained.
            'feature_vectors',
            'labeled_query_page',
            'query_clicks_ltr',
            'query_clustering',
            # Intentionally excluded:
            # - model_parameters: Has no private data, useful for looking at
            #   training history.
        ],
        checksum='ee78459921ebf210b384e561258abaa2',
    ) >> complete

    refinery_drop_older_than(
        task_id='drop_discovery_partitions',
        older_than_days=84,  # 12 weeks
        database='discovery',
        tables=[
            # Contains private data that must not be retained
            'query_clicks_daily',
            'query_clicks_hourly',
            'search_satisfaction_daily',
            # Contains non-private data, but no real need to keep forever
            'mediawiki_revision_recommendation_create',
            'ores_articletopic',
            'ores_scores_export',
            'popularity_score',
            'wikibase_item',
            # Intentionally excluded:
            # - cirrus_namespace_index_map: Not private, not partitioned
            # - wikibase_rdf: Managed somewhere else
            # - fulltext_head_queries: Handled below with shorter allowed lifetime
        ],
        checksum='852a2075b5663ef22a383d145191c3b9',
    ) >> complete

    refinery_drop_older_than(
        task_id='drop_discovery_short_term_partitions',
        older_than_days=6,  # Data derived from 84 day tables
        database='discovery',
        tables=['fulltext_head_queries'],
        checksum='851be7cd8add879c762e50563a5de233',
    ) >> complete

    refinery_drop_older_than(
        task_id='drop_wikibase_rdf_partitions',
        database='discovery',
        tables=["wikibase_rdf"],
        # we want to keep 4 partition (generated weekly). But since the data takes
        # multiple days to arrive we allow a 6 days tolerance here
        older_than_days=29 + 6,
        checksum='deaff5e72f68130db18eec1ba64a3952',
    ) >> complete

    refinery_drop_older_than(
        task_id='drop_processed_external_sparql_query_partitions',
        database='discovery',
        tables=["processed_external_sparql_query"],
        older_than_days=90,
        checksum='f676bbf2aa0b04a22e226700b93a8bbe',
    ) >> complete

    refinery_drop_older_than(
        task_id='drop_subgraph_query_mapping_and_metrics_partitions',
        database='discovery',
        tables=[
            "subgraph_qitems_match",
            "subgraph_predicates_match",
            "subgraph_uri_match",
            "subgraph_queries"
        ],
        # kept 90 days following sparql queries, whose `id` is used here
        older_than_days=90,
        checksum='f1da9a0f127ec19a147a150ba1495f92',
    ) >> complete

    # Drops data that is *not* part of a hive table. data from hive tables
    # needs to be specially handled. These are directories that are written
    # to directly without any hive integration. Specified paths must
    # contains folders in the format `date=YYYYMMDD`.
    SparkSubmitOperator.for_virtualenv(
        task_id='drop_dated_directories',
        virtualenv_archive=discolytics_conda_env_tgz,
        entry_point='bin/drop_dated_directories.py',
        application_args=[
            '--verbose',
            '--delete-after-days', '60',
            f'{data_path}/glent/esbulk',
            f'{data_path}/mjolnir/trained_models',
            f'{data_path}/mjolnir/training_files',
            f'{data_path}/transfer_to_es',
            f'{data_path}/wdqs/entity_revision_map',
        ]
    ) >> complete

    # Config is hardcoded into the script. To make things work the script
    # was forked into discolytics and can be run from there. Drops partitions
    # from hive tables with a partition key of the format `snapshot=YYYYMMDD`.
    SimpleSkeinOperator(
        task_id='drop_snapshot_partitioned_partitions',
        resources={'memory': 2048, 'vcores': 1},
        files=f'{discolytics_conda_env_tgz}#venv',
        script='venv/bin/python venv/bin/refinery_drop_mediawiki_snapshots.py --verbose'
    )
