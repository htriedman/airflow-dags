#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Section topics data pipeline orchestration
Topics for section in wikipedia articles are generated using blue links in article sections, and written into Hive
See https://gitlab.wikimedia.org/repos/structured-data/section-topics
"""

from datetime import datetime, timedelta

from airflow import DAG
from airflow.providers.apache.hive.sensors.named_hive_partition import NamedHivePartitionSensor
from mergedeep import merge

from platform_eng.config.dag_config import alerts_email as default_email
from platform_eng.config.dag_config import artifact, default_args, hadoop_name_node
from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.url import URLSensor

dag_id = "section_topics"
alerts_email = "sd-alerts@lists.wikimedia.org"
var_props = VariableProperties(f"{dag_id}_config")
conda_env = var_props.get("conda_env", artifact("section-topics-0.5.0-v0.5.0.conda.tgz"))
work_dir = var_props.get("work_dir", "/user/analytics-platform-eng/structured-data/section_topics/")

modified_args = merge(
    {},
    default_args,
    {
        "email": [default_email, alerts_email],
        # Large job as per
        # https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Large_jobs,
        # https://github.com/wikimedia/wmfdata-python/blob/v2.0.0/wmfdata/spark.py#L50
        "driver_memory": "4G",
        "executor_cores": 4,
        "executor_memory": var_props.get("executor_memory", "8G"),
        "conf": {
            "spark.sql.shuffle.partitions": 2048,
            "spark.reducer.maxReqsInFlight": 1,
            "spark.shuffle.io.retryWait": "60s",
            "spark.executor.memoryOverhead": "3g",
            "spark.dynamicAllocation.enabled": "true",
            "spark.dynamicAllocation.maxExecutors": 128,
            "spark.shuffle.service.enabled": "true",
            # Uncomment in case of `FetchFailed` due to `NetworkTimeout`, see
            # https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Cluster/Spark#Scaling_the_driver,
            # https://towardsdatascience.com/fetch-failed-exception-in-apache-spark-decrypting-the-most-common-causes-b8dff21075c
            # "spark.stage.maxConsecutiveAttempts": 10,
            # "spark.shuffle.io.maxRetries": 10,
        },
        # Avoid `skein.exceptions.DriverError: Received message larger than max (21089612 vs. 4194304)`
        "skein_app_log_collection_enabled": False
    }
)

with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 9, 7)),
    schedule_interval=var_props.get("schedule_interval", "0 0 * * 1"),  # Every monday @ 12AM
    catchup=var_props.get("catchup", False),
    tags=["structured-data-team"],
    default_args=modified_args,
) as dag:
    # Pre-condition: wait for the latest input snapshots
    # NOTE We pass the snapshot values through Airflow"s built-in template variables and macros,
    #      see https://airflow.apache.org/docs/apache-airflow/2.2.0/templates-ref.html (2.1.4 returns HTTP 404)
    # TODO If we upgrade to Airflow version 2.2.0, use `data_interval_start` instead of `execution_date` (deprecated)
    weekly = "{{ execution_date.format('YYYY-MM-DD') }}"
    # Set the most recent monthly snapshot given the weekly one
    # NOTE A snapshot date is the *beginning* of the snapshot interval. For instance:
    #      - 2022-05-16 covers until 2022-05-22 (at 23:59:59).
    #        May is not over, so the May monthly snapshot is not available yet.
    #        Hence return end of *April*, i.e., 2022-04
    #      - 2022-05-30 covers until 2022-06-05.
    #        May is over, so the May monthly snapshot is available.
    #        Hence return end of *May*, i.e., 2022-05
    monthly = "{{ execution_date.end_of('week').subtract(months=1).start_of('month').format('YYYY-MM') }}"

    # DAG task 1: wait for the Wikidata weekly table
    wait_for_wikidata = NamedHivePartitionSensor(
        task_id="wait_for_wikidata",
        partition_names=[f"wmf.wikidata_item_page_link/snapshot={weekly}"],
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    # DAG task 2: wait for the Wikitext monthly table
    # NOTE the `_PARTITIONED` file tells us that partitions were added to:
    #      - HDFS by sqoop
    #      - the Hive Metastore by `analytics/dags/mediawiki/mediawiki_history_load_dag.py`
    wait_for_wikitext = URLSensor(
        task_id="wait_for_wikitext",
        # URL for `wmf.mediawiki_wikitext_current`
        url=f"{hadoop_name_node}/wmf/data/wmf/mediawiki/wikitext/current/snapshot={monthly}/_PARTITIONED",
        poke_interval=timedelta(hours=1).total_seconds(),
    )

    # DAG task 3: execute the pipeline with default optional arguments
    # Usage: pipeline.py [-w WORK_DIR] [-i INPUT_WIKIS] [-d DENYLIST] [-f FILTER] [--handle-media] [-m MEDIA_PREFIXES] snapshot
    pipeline = SparkSubmitOperator.for_virtualenv(
        task_id="pipeline",
        launcher="skein",
        virtualenv_archive=conda_env,
        entry_point="lib/python3.10/site-packages/section_topics/pipeline.py",
        application_args=["-w", work_dir, weekly],
    )

    # Build the actual DAG
    wait_for_wikidata >> wait_for_wikitext >> pipeline
