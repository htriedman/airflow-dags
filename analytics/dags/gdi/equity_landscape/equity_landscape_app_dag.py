"""
# GDI Equity Landscape DAG - APP

* This DAG is used to run the input metrics for all the input metrics
## Outputs HDFS
"""

from datetime import datetime
from typing import Dict

from airflow import DAG
from airflow.operators.dummy import DummyOperator

from analytics.config import dag_config

from wmf_airflow_common.config.variable_properties import VariableProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator

dag_id = "gdi_equity_landscape_app"
var_props = VariableProperties(f"{dag_id}_config")

schema = var_props.get("schema", "gdi")

archives = (
    var_props.get(
        "conda_env",
        "https://gitlab.wikimedia.org/repos/gdi/equity-landscape/gdi-source/-/package_files/1074/download",
    )
    + ".tgz#venv"
)



partition_year = var_props.get("partition_year", 2021)

jobs_config = var_props.get(
        "jobs_config",
        {
            "affiliate_input_metrics": {
                "application": "lib/python3.7/site-packages/gdi_source/equity_landscape/affiliate_input_metrics.py",
                "args": {
                    "--schema": f"{schema}",
                    "--hdfs-path": "hdfs://analytics-hadoop/wmf/data/raw/gdi/equity_landscape/csv",
                }
            },
            "input_metrics": {
                "application": "lib/python3.7/site-packages/gdi_source/equity_landscape/input_metrics.py",
                "args": {
                    "--schema": f"{schema}",
                    "--output-path": "hdfs://analytics-hadoop//wmf/data/gdi/equity_landscape/output/input_metrics",
                },
            },
            "regional_input_metrics": {
                "application": "lib/python3.7/site-packages/gdi_source/equity_landscape/regional_input_metrics.py",
                "args": {
                    "--input-path": "hdfs://analytics-hadoop//wmf/data/gdi/equity_landscape/output/input_metrics",
                    "--output-path": "hdfs://analytics-hadoop//wmf/data/gdi/equity_landscape/output/regional_input_metrics",
                },
            },
            "output_metrics": {
                "application": "lib/python3.7/site-packages/gdi_source/equity_landscape/output_metrics.py",
                "args": {
                    "--input-path": "hdfs://analytics-hadoop//wmf/data/gdi/equity_landscape/output/regional_input_metrics",
                    "--output-path": "hdfs://analytics-hadoop//wmf/data/gdi/equity_landscape/output/output_metrics",
                },
            },
        },
    )

def _create_spark_operator(task_id: str, application_path: str, args: Dict):
    """
    Create a SparkSubmitOperator for the given application.

    param task_id: The task_id for the operator
    param application_path: The path to the application
    param args: The arguments to pass to the application

    """
    args["--year"] = partition_year

    return SparkSubmitOperator.for_virtualenv(
        task_id=task_id,
        virtualenv_archive=archives,
        use_virtualenv_spark=True,
        entry_point=application_path,
        skein_app_log_collection_enabled=False,
        launcher="skein",
        application=None,
        application_args=args,
    )


with DAG(
    dag_id=dag_id,
    doc_md=__doc__,
    start_date=var_props.get_datetime("start_date", datetime(2022, 10, 1)),
    schedule_interval=None,
    default_args=var_props.get_merged("default_args", dag_config.default_args),
) as dag:

    start = DummyOperator(task_id="start")
    end = DummyOperator(task_id="end")

    last_task = start
    for job_name, job_config in jobs_config.items():
        operator = _create_spark_operator(
            task_id=job_name,
            application_path=job_config["application"],
            args=job_config["args"],
        )
        last_task = last_task >> operator
    last_task >> end
